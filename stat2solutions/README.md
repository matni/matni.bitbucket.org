Here I will put my solutions to the Practice problems of
Statistical Rethinking (2020, 2nd ed.) by R. McElreath

## Note on output format {-}

- For HTML: bookdown::render_book("index.Rmd")
- For pdf: bookdown::render_book("index.Rmd", output_format = "bookdown::pdf_book") 

All of the content of this repository is licensed 
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
