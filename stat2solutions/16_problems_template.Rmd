# Problems Individual assignment

Please solve the following problems that I made up to cover some key ideas in 
Bayesian estimation as described in the Book. Use McElreath's function `ulam()` 
(It is also OK to code directly in STAN using the `rstan` library). I recommend 
that you compile your solutions in a Rmarkdown (or Quarto) document (please 
send me the knitted *html file).

| Exercises to solve for Individual Assignment-Problems |
|:------------------------------------------------------:|
|                         [MN4a]                         |
|                         [MN4b]                         |
|                         [MN4c]                         |
|                         [MN4d]                         |
|                         [MN4e]                         |
|                         [MN8a]                         |
|                         [MN8b]                         |


Note: You got started with MN4a in one of the seminars. 

<br>
