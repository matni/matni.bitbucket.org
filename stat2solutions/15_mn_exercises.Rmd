---
editor_options: 
  markdown: 
    wrap: 72
---

# My exercises

## MN1 {.unnumbered}

```{block MN1, type = 'mce'}
This is an old exam question from Stat 1:  

For symptom-free women age 40 to 50 who participate in screening using mammography, 
the following information is available:  

+	1 % has breast cancer,
+	If a woman has breast cancer, the probability is 99 % that she will have a 
positive mammogram, 
+	If a woman does not have breast cancer, the probability is 97 % that she will 
have a negative mammogram.  

Imagine a woman (40-50 years with no symptoms) who has a positive mammogram. What 
is the probability that she actually has breast cancer?
```

<br>

**solution**

## MN2 {.unnumbered}

```{block MN2, type = 'mce'}
From Aubrey Clayton’s excellent book “Bernoulli’s Fallacy” 
(@clayton2021bernoulli, p. 36)  

“Your friend rolls a six-sided die and secretly records the outcome; this number 
becomes the target T. You then put on a blindfold and roll the same sixsided die 
over and over. You’re unable to see how it lands so, each time, your friend 
(under the watchful eye of a judge, to prevent any cheating) tells you only whether 
the number you just rolled was greater than, equal to, or less than T. After some 
number of rolls … you must guess what the target was.”  

Suppose in one round of the game we had this sequence:  

*G, L, L, G, E, E, G*,  

where G represents a greater roll, L a lesser roll, and E an equal roll.  
What would be your strategy for guessing the target T, and how confident would 
you be? Start with a uniform prior distribution of T assuming that the die is fair.
```

<br>

**solution**

## MN3 {.unnumbered}

```{block MN3, type = 'mce'}
You test an individual M's ability to solve a certain class 
of logical problems. M gave answers to nine versions of the problem and you coded 
each as correct (1) or incorrect (0). This is the data:  

1, 0, 0, 0, 0, 1, 0, 0, 0

You want to estimate M's true ability ($p$) using this model:

$y_i \sim Bernoulli(p)$  

$p \sim Uniform(0, 1)$

(a) List the critical assumptions of this model.
(b) Use grid approximation to derive point estimate and 95 % Compatibility 
Interval (CI, defined as a highest density interval). 
(c) Verify that you obtain the same result using the binomial distribution with 
$n=9$, that is, equal to the number of trials (this is McElreath's approach to
his globe tossing data). 
```

<br>

Here code for grid approximation

```{r}
# data
d <- c(1, 0, 0, 0, 0, 1, 0, 0, 0)

# grid
pgrid <- seq(0, 1, 0.001)

# prior
prior <- dbeta(pgrid, shape1 = 1, shape = 1)  # same as dunif(pgrid, 0, 1)

# Likelihood: probability of data as a function of theta = pgrid
# First a function to calculate log joint probability for given value of p
# log to avoid issues with very small values
loglikfunc <- function(p) {
  loglik <- sum(dbinom(d, size = 1, prob = p, log = TRUE))
  loglik
}

loglik <- sapply(pgrid, loglikfunc)
likelihood <- exp(loglik)  # Antilog to get back to probability scale

# Relative posterior
post_rel <- prior * likelihood

# Two plots: Grid-approx relative-posterior (left) next to true posterior (right)
par(mfrow = c(1, 2)) 

# Plot posterior
plot(pgrid, post_rel, type = "l", xlab = "Probability of success", 
     ylab = "Relative posterior")

# Plot posterior using conjugate beta-binomial, just as check shoud be the same
# shape
curve(dbeta(x, shape1 = sum(d) + 1, shape2 = length(d)-sum(d) + 1), 
      ylab = "Posterior density", xlab = "Probability of success")
```

Sample from the posterior to obtain 95 % HDI

```{r}
# Sample from the posterior
samples <- sample(pgrid, size = 1e6, prob = post_rel, replace = TRUE)

# Summarize: Point estimates
m <- mean(samples) # mean from samples
mdn <- median(samples) # median from samples

# Summarize: Dispersion
post_sd <- sd(samples)
post_mad <- mad(samples)  # Note: default adjustment constant = 1.4826

# 95 % HID
hdi95 <- rethinking::HPDI(samples, prob = 0.95)

# Print point estimate and 95 % HDI
round(c(mdn = mdn, hdi95), 2)

```

Plot

```{r}
# Plot posterior with 95 % HID using rethinking
rethinking::dens(samples, show.HPDI = 0.95,
                 xlab = "Probability success",
                 ylab = "Posterior density")  # Plot with interval
```

<br>

Here using grid approximation with binomial(n = 9, p) ála McElreath.

```{r}
set.seed(999)

# Data
n = 9  # Number of trials
s = 2 # Number of successes

# Grid
pgrid <- seq(0, 1,  length.out = 1e4)  

# Prior
prior <- rep(1, length(pgrid))

# Likelihood: probability of s as a function of theta
likelihood <- dbinom(s, n, prob = pgrid)

# Relative posterior
rel_posterior <- likelihood * prior
  
# Plot relative posterior
plot(pgrid, rel_posterior, type = 'l', xlab = "Probability of success", 
     ylab = "Relative posterior")

# Sample from the posterior
samples2 <- sample(pgrid, size = 1e6, prob = rel_posterior, replace = TRUE)
round(c(mdn = median(samples2), rethinking::HPDI(samples, prob = 0.95)), 2)
```

**solution**

a.  One is independence of trials, that is, no carry over effects (e.g.,
    no learning from trial to trial). Another assumption is constant $p$
    for all problems, implying both that all problems are equally hard
    for the test taker and that performance doesn't change with time (no
    fatigue etc.)
b.  See above
c.  See above: (1) exact solution using beta-binomial conjugate, and (2)
    plot of grid approximation using Binomial(N = 9, p) and derived
    point estimate and 95% CI.

<br>



## MN4a {.unnumbered}

```{block MN4a, type = 'mce'}
Use McElreath’s dataset `Howell2` restricted to adults (age > 18). Fit two linear 
models using quadratic approximation, `quap()`:  
1. Weight as outcome, with two predictors: sex as an indicator or index variable
and age as a continuous variable.   
2.	As above, but allow for a curvilinear effect of age, by adding age-squared 
to the model.  

Center the age variable around its mean.

(a) For the first model, motivate priors based on a prior predictive simulation 
(cf. Fig. 4.5 of the Book).
Then fit the model and report regression coefficients with compatibility 
intervals, and illustrate model predictions in a plot of the data (weight as a 
function of *non-centered* age) with separate prediction lines for women and men. 
Include compatibility regions around fitted lines (e.g., as in Fig. 5.1 of the 
book) or add samples of fitted lines (e.g., as in Fig. 4.7).  
(b) Do the same for the second model.
```

Load data and recode

```{r}
data(Howell2)
d <- Howell2
rm(Howell2)
d <- d[d$age > 18, c('age', 'male', 'weight')]  # Select subset of data
d <- d[complete.cases(d), ]  # Remove cases with any NA

# Fix data: Index variable of sex, and centered age
d$sex <- d$male + 1  # Make male an index variable, 1, 2
d$age_c <- d$age-mean(d$age)
```

<br>

**solution**

Models: 

- SEX -- Index variable: 1 = females, 2 = males
- Agec  -- Age (y) centered around mean age (about 40 y)



Model 4a:

$weight_i \sim Normal(\mu_i, \sigma)$  

$mu_i  = a_{SEX[i]} + bAgec_i$

$a_j  \sim Normal(70, 30),\ \ for \ j = 1, 2$

$b  \sim Normal(0, 0.5)$

$\sigma \sim Exponential(0,1)$

<br>

Model 4b:

$weight_i \sim Normal(\mu_i, \sigma)$  

$mu_i  = a_{SEX[i]} + bAgec_i + b_2Agec^2_i$

$a_j  \sim Normal(70, 30),\ \ for \ j = 1, 2$

$b  \sim Normal(0, 0.5)$

$b_2  \sim Normal(0, 0.02)$

$\sigma \sim Exponential(0,1)$

<br>

Prior predictive simulation, for model 4a and 4b (Note: male and female have same prior N(70, 30)).
```{r}
# Function that draws a line for linear predictor model 4b: y = a + b*x + b2*x^2 
# Set b2 <- 0 for model 4a
# argument: j = index of vector
drawpp <- function(j){
  yy <- a[j] + b[j]*xx + b2[j]*xx^2  
  lines(xx, yy, col = rgb(1, 0, 0, 0.2))
}

# Random parameter values from priors
a <- rnorm(1e4, 70, 30)
b <- rnorm(1e4, 0, 0.5)



par(mfrow = c(1, 2))

# Model 4a   

b2 <- rnorm(1e4, 0, 0)  # b2 = 0

# Empty plot
plot(c(-25, 50), c(0, 200), pch = "", xlab = "Age [centered at mean age]",
     ylab = "weight [kg]")
abline(h = 20, col = "grey")  # Low weight line as reference
abline(h = 150, col = "grey") # High weight line as reference
mtext("Prior, Model 4a", side = 3, cex = 1.2)

# Draw 200 lines from prior
xx <- seq(-25, 50, 1)  # x-values for lines
for (j in 1:200) drawpp(j)

# Model 4b  
b2 <- rnorm(1e4, 0, 0.02)  # Model 4b

# Empty plot
plot(c(-25, 50), c(0, 200), pch = "", xlab = "Age [centered at mean age]",
     ylab = "weight [kg]")
abline(h = 20, col = "grey")  # Low weight line as reference
abline(h = 150, col = "grey") # High weight line as reference
mtext("Prior, Model 4b", side = 3, cex = 1.2)

# Draw 200 lines from prior
xx <- seq(-25, 50, 1)  # x-values for lines
for (j in 1:200) drawpp(j)


```
Both seem OK: Wide priors that allow mean weights anywhere between 20 and 150 kg, 
and trends in both directions.

<br>

Plot data, with non-parametric line (LOWESS)
```{r}
# Empty plot
xlim <- c(15, 90) - mean(d$age)
plot(d$age_c, d$weight, pch = "", axes = FALSE, ylim = c(30, 65), xlim = xlim,
     xlab = "Age [y]", ylab = "Weight [kg]")

# Add axis, note the uncentering of teh x-axis
axis(1, at = seq(15, 90, 5) - mean(d$age), labels = seq(15, 90, 5), pos = 30) 
axis(2, at = seq(30, 65, 5), pos = 15 - mean(d$age)) 

# Add data, females
points(d$age_c[d$sex == 1], d$weight[d$sex == 1], pch = 21, 
       bg = "lightgrey")
lines(lowess(d$weight[d$sex == 1] ~ d$age_c[d$sex == 1]), col = "black")

# Add data, males
points(d$age_c[d$sex == 2], d$weight[d$sex == 2], pch = 21, 
       bg = "lightgreen")
lines(lowess(d$weight[d$sex == 2] ~ d$age_c[d$sex == 2]), 
      col = "darkgreen", lty = 2)


```
<br>

Fit models:

Model 4a
```{r}
m4a <- quap(alist(
                  weight ~ dnorm(mu, sigma),
                  mu <- a[sex] + b*age_c, 
                  a[sex] ~ dnorm(70, 30), 
                  b ~ dnorm(0, 0.5),
                  sigma ~ dexp(0.1)), data = d)

precis(m4a, depth = 2)
```

Plot model 4a
```{r}
s4a <- extract.samples(m4a)

newdat <- expand.grid(age_c = seq(15, 90) - mean(d$age), sex = c(1, 2))
mu <- link(m4a, data = newdat)
mu_mean <- apply(mu, 2 , mean )
mu_hdi <-  apply(mu, 2, HPDI , prob = 0.95)

# Empty plot
xlim <- c(15, 90) - mean(d$age)
plot(d$age_c, d$weight, pch = "", axes = FALSE, ylim = c(30, 65), xlim = xlim,
     xlab = "Age [y]", ylab = "Weight [kg]")

# Add axis, note the uncentering of teh x-axis
axis(1, at = seq(15, 90, 5) - mean(d$age), labels = seq(15, 90, 5), pos = 30) 
axis(2, at = seq(30, 65, 5), pos = 15 - mean(d$age)) 

# Add data, females
points(d$age_c[d$sex == 1], d$weight[d$sex == 1], pch = 21, 
       bg = "lightgrey")

# Add data, males
points(d$age_c[d$sex == 2], d$weight[d$sex == 2], pch = 21, 
       bg = "lightgreen")

# Add female predictions
lines(newdat$age_c[newdat$sex == 1], mu_mean[newdat$sex == 1], lwd = 2)
shade(mu_hdi[,newdat$sex == 1], newdat$age_c[newdat$sex == 1])

# Add male predictions
lines(newdat$age_c[newdat$sex == 2], mu_mean[newdat$sex == 2], lwd = 2, col = "green")
shade(mu_hdi[,newdat$sex == 2], newdat$age_c[newdat$sex == 2], col = rgb(0, 1, 0, 0.2))
```

<br>

Model 4b 
```{r}
d$age2_c <- d$age_c^2

m4b <- quap(alist(
                  weight ~ dnorm(mu, sigma),
                  mu <- a[sex] + b*age_c + b2*age2_c, 
                  a[sex] ~ dnorm(70, 30), 
                  b ~ dnorm(0, 0.5),
                  b2 ~ dnorm(0, 0.02),
                  sigma ~ dexp(0.1)), data = d)

precis(m4b, depth = 2, digits = 5)

```


Plot model 4b
```{r}
newdat$age2_c <- newdat$age_c^2  # Add squared age to newdat (defined above)
mu2 <- link(m4b, data = newdat)  
mu_mean <- apply(mu2, 2 , mean )
mu_hdi <-  apply(mu2, 2, HPDI , prob = 0.95)

# Empty plot
xlim <- c(15, 90) - mean(d$age)
plot(d$age_c, d$weight, pch = "", axes = FALSE, ylim = c(30, 65), xlim = xlim,
     xlab = "Age [y]", ylab = "Weight [kg]")

# Add axis, note the uncentering of teh x-axis
axis(1, at = seq(15, 90, 5) - mean(d$age), labels = seq(15, 90, 5), pos = 30) 
axis(2, at = seq(30, 65, 5), pos = 15 - mean(d$age)) 

# Add data, females
points(d$age_c[d$sex == 1], d$weight[d$sex == 1], pch = 21, 
       bg = "lightgrey")

# Add data, males
points(d$age_c[d$sex == 2], d$weight[d$sex == 2], pch = 21, 
       bg = "lightgreen")

# Add female predictions
lines(newdat$age_c[newdat$sex == 1], mu_mean[newdat$sex == 1], lwd = 2)
shade(mu_hdi[,newdat$sex == 1], newdat$age_c[newdat$sex == 1])

# Add male predictions
lines(newdat$age_c[newdat$sex == 2], mu_mean[newdat$sex == 2], lwd = 2, col = "green")
shade(mu_hdi[,newdat$sex == 2], newdat$age_c[newdat$sex == 2], col = rgb(0, 1, 0, 0.2))
```

Model comparison favors model m4b
```{r}
# Model comparison
compare(m4a, m4b, func=WAIC)

```

**Extra: Robust regression**

Similar result with T2-likelihood
```{r}
m4at <- quap(alist(
                  weight ~ dstudent(2, mu, sigma),
                  mu <- a[sex] + b*age_c, 
                  a[sex] ~ dnorm(70, 30), 
                  b ~ dnorm(0, 0.5),
                  sigma ~ dexp(0.1)), data = d)

precis(m4at, depth = 2)

m4bt <- quap(alist(
                  weight ~ dstudent(2, mu, sigma),
                  mu <- a[sex] + b*age_c + b2*age2_c, 
                  a[sex] ~ dnorm(70, 30), 
                  b ~ dnorm(0, 0.5),
                  b2 ~ dnorm(0, 0.02),
                  sigma ~ dexp(0.1)), data = d)

precis(m4bt, depth = 2, digits = 5)

coeftab(m4a, m4at, m4b, m4bt, digits = 3)
```



## MN4b {.unnumbered}

```{block MN4b, type = 'mce'}
Rerun the first model in MN4a, but now with age as a categorical variable in 10 
year bins (18-27, 28-37, ...). Make it an index variable (1, 2, 3, ...) and 
code the model in the same why as the Book's example R.code 5.48. Interpret the 
result.
```

<br>

**solution**

<!--

```{r}
# Make index variable of age
d$agecat <- as.integer(cut(d$age, breaks = c(-Inf, 27, 37, 47, 57, 67, 77, Inf)))
table(d$agecat)


m4b <- ulam(alist(
                  weight ~ dnorm(mu, sigma),
                  mu <- a[sex, agecat], 
                  matrix[sex, agecat]: a ~ dnorm(70, 20), 
                  sigma ~ dexp(0.1)), 
            data = d)

precis(m4b, depth = 3)
```

```{r}
dd <- expand.grid(agecat = 1:7, sex = 1:2)
mu <- link(m4b, data = dd)
dd$mu <- apply(mu, 2, mean)
dd$lo <- apply(mu, 2, HPDI, prob = 0.95)[1, ]
dd$hi <- apply(mu, 2, HPDI, prob = 0.95)[2, ]

plot(dd$agecat, dd$mu, ylim = c(35, 55), pch = "")
for (j in 1:14) {
  lines(c(dd$agecat[j], dd$agecat[j]), c(dd$lo[j], dd$hi[j]))
}
points(dd$agecat, dd$mu, pch = 21, bg = "grey")
```

-->

## MN4c {.unnumbered}

```{block MN4c, type = 'mce'}
Return to the two models in MN4a.  

1. Add an interaction term to the first model, allowing for separate linear 
relationships for men and women (i.e., separate intercepts and slopes).  

2. Expand the second model so it allows for different quadratic relationships 
for men and women (i.e., separate intercepts and slopes and coefficients for 
age-squared). 

Plot model predictions, including uncertainty for both models.
```

TBA

<br>

**solution**

## MN4d {.unnumbered}

```{block MN4d, type = 'mce'}
Compare the five models you fitted in 4a-c, using WAIC and PSIS for model 
comparison, and interpret the result. What model seem best 
in terms of out-of-sample prediction?
```

TBA

<br>

**solution**

## MN4e {.unnumbered}

```{block MN4e, type = 'mce'}
Refit the "best" model from MN4d, this time using Hamiltonian Monte Carlo, 
function `ulam()` from the rethinking package.  

(a) Describe how many sampling iterations, warmup iterations, and chains 
you used. Motivate your choice.
(b) Report `neff` and `R`, and comment.
(c) Make a trace-plot and comment. 
```

TBA

<br>

**solution**

## MN5 {.unnumbered}

```{block MN5, type = 'mce'}
Simulate observed and potential data from a randomized experiment following 
the potential outcome model of causality. Describe results in terms of the 
unobserved sample average treatment effect (SATE), population average treatment 
effect (PATE) and the observed between-group difference (sometimes called the 
naïve or the prima-facie effect). See @Gelman2020, Chapter 18, for definitions 
of SATE and PATE.
```

<br>

**solution**

Below a simulation of the scenario described in @Gelman2020, see tables in 
Chapter 18.

Simulation of a randomized experiment with two groups: Fish oil and 
Placebo treatment, where the outcome is systolic blood pressure after treatment. 
```{r}
set.seed(123)  # Set the random number generator

# Number of participants
n <- 60   # Make it an even number to have equal sized groups
id <- 1:n # Give each participant a number

# Simulation parameters
mu0 <- 130  # Population average systolic blood pressure (sbp) if untreated
sd0 <- 10  # Population sd, if untreated
pate <- -5  # Population average causal effect of fish oil
sdeffect <- 2  # sd of causal effect

# Potential outcome if untreated
y0 <- rnorm(n, mean = mu0, sd = sd0)

# Single unit causal effects
delta <- rnorm(n, mean = pate, sd = sdeffect)

# Potential outcome if treated
y1 <- y0 + delta

# Random assignment to placebo (= 0) or fish-oil (= 1), half n to each group
temp <- c(rep(0, n/2), rep(1, n/2))  # Vector with n/2 zeros and n/2 ones
treatment <- sample(temp, replace = FALSE)  # Random assignment using sample()

# Observed sbp
y_obs <- (1 - treatment) * y0 + treatment * y1

# Collect data in data frame. Observed variables capitalized (but not rounded)
d <- data.frame(ID = id, y0 = y0, y1 = y1, 
                         delta = delta, Treat = treatment, 
                         Observed = y_obs)
```

<br>

Data (observed and unobserved) for some participants
```{r}
head(round(d))
```
<br>

Distribution of single-unit causal effects
```{r}
hist(d$delta, breaks = seq(-11.25, 0.25, 0.5), 
     xlab = "Single-unit causal effect", main = "")
```
<br>

PATE, SATE (n = 60 for SATE), prima-facie.

```{r}
ce <- c(PATE = pate, SATE = mean(d$delta), 
        primafacie = mean(d$Observed[d$Treat == 1]) - mean(d$Observed[d$Treat == 0]))
round(ce, 1)
```


## MN6 {.unnumbered}

```{block MN6, type = 'mce'}
Do all tutorials on http://dagitty.net/learn/index.html
```

No solution here, just go to dagitty.net and start practicing!

<br>

## MN7 {.unnumbered}

```{block MN7, type = 'mce'}
Below a DAG with five variables: X (exposure), Y (outcome), Z1 
(measured covariate), Z2 (measured covariate), U (unmeasured variable).

a. Simulate data consistent with this DAG. Assume that all variables (nodes) are 
continuous and normally distributed and that all relationships (arrows) are linear. 
Specify the total causal effect and the direct causal effect of X on Y in your 
simulated scenario.  
b. Fit linear multiple regression models to your data to illustrate that a model 
with Z2 as the only covariate may give an unbiased estimate of the true total 
causal effect of X and Y, whereas a model with both Z1 and Z2 (or only Z1) may 
yield a biased estimate. Show also that including Z1 in a model will not 
suffice to estimate the direct causal effect of X on Y (because it is a collider 
on the path between X and U).  
c. Use cross-validation (SR: p. 217-) to compare the out-of-sample predictive 
performance of two models: one with Z2 as the only covariate, and the second with 
both Z1 and Z2 as covariates. Which model is best in terms of out-of-sample 
prediction?
```

```{r, fig.width = 4, fig.height = 4}

dag <- dagitty( "dag {
   X -> Y
   X -> Z1 -> Y
   X <- Z2 -> Y
   Z1 <- U -> Y
}")

coordinates(dag) <- list(
  x = c(X = 1, Z1 = 2, Z2 = 2, U = 3, Y = 3),
  y = c(X = 3, Z1 = 2, Z2 = 4, U = 1, Y = 3))

plot(dag)
```

<br>

Simulating linear relationships and random effects

```{r}
set.seed(222)
n <- 1e4  # Number of observations

# Simulate random causal effects (here modeled as linear regression coefficients), 
# as many as graph edges
xz1 <- rnorm(n, 0.8, sd = 0.1)
z1y <- rnorm(n, 0.5, sd = 0.1)
xy <-  rnorm(n, 0.3, sd = 0.1)
z2x <- rnorm(n, 0.8, sd = 0.1)
z2y <- rnorm(n, 0.8, sd = 0.1)
uz1 <- rnorm(n, 0.3, sd = 0.1)
uy <- rnorm(n, -0.4, sd = 0.1)

# Exogenous variables, explicitly shown in the DAG
z2 <- rnorm(n)
u <-  rnorm(n)

# Exogenous variables, typically omitted from the DAG
x_exo  <- rnorm(n)
z1_exo <- rnorm(n)
y_exo  <- rnorm(n)

# Endogenous variables
x <- x_exo + z2x*z2   
z1 <- z1_exo + xz1*x + uz1*u
y <- y_exo + z1y*z1 + z2y*z2 + xy*x + uy*u

# Put in data frame (not necessary, but makes it easy to calculate corr-matrix)
dr <- data.frame(x, y, z1, z2, u)
round(cor(dr), 2)
```

<br>

Total average causal effect

```{r}
hist(xy + xz1*z1y, breaks = 200, main = "Single-unit causal effects")
pate <- mean(xy + xz1*z1y)
lines(c(pate, pate), c(0, 4000), col = "blue", lwd = 3)
round(pate, 2)
```

<br>

Define models

```{r}
# Crude model, biased: omitted variable bias
m0 <- alist(
  y ~ dnorm(mu, sigma),
  mu <- b0 + bx*x,
  c(b0, bx) ~ dnorm(0, 1),
  sigma ~ dexp(1)
)

# Unbiased model, adjusting for confounder Z2
m1 <- alist(
  y ~ dnorm(mu, sigma),
  mu <- b0 + bx*x + b2*z2,
  c(b0, bx, b2) ~ dnorm(0, 1),
  sigma ~ dexp(1)
)

# Biased model, over-adjustment bias, Z1 is a mediator
m2 <- alist(
  y ~ dnorm(mu, sigma),
  mu <- b0 + bx*x + + b1*z1 + b2*z2,
  c(b0, bx, b1, b2) ~ dnorm(0, 1),
  sigma ~ dexp(1)
)

```

<br>

Fit models, using quap()

```{r}
m0r <- quap(m0, data = dr)
m1r <- quap(m1, data = dr)
m2r <- quap(m2, data = dr)
coeftab(m0r, m1r, m2r)
```

<br>

Visualize causal effect estimates

```{r}
s0 <- extract.samples(m0r)
s1 <- extract.samples(m1r)
s2 <- extract.samples(m2r)

dens(s0$bx, show.HPDI = 0.95, xlim = c(0.2, 1.2), xlab = "Causal effect estimate")
dens(s1$bx, show.HPDI = 0.95, xlim = c(0, 1.5), add = TRUE)
dens(s2$bx, show.HPDI = 0.95, xlim = c(0, 1.5), add = TRUE)
lines(c(pate, pate), c(0, 200), col = "blue", lwd = 2)

```

Model m1 get the causal effect right, m0 overestimates the effect
(omitted variable bias), m2 underestimate the total average causal
effect (over-adjustment bias).

<br>

Model comparison (PSIS and WAIC give very similar estimates)

```{r}
compare(m0r, m1r, m2r, func = PSIS)
compare(m0r, m1r, m2r, func = WAIC)
```

**solution**

-   Model 1 give an unbiased estimate of the total average casual effect
    of x on y\
-   Model 2 underestimates the causal effect but is the best model in
    terms of out-of-sample prediction.


## MN8a {.unnumbered}

```{block MN8a, type = 'mce'}
Look at Study 1 of Wilson and Rule, 2015, and try to reproduce their 
Table 1, using `ulam()`. 
![Wilson and Rule, 2015, Table 1](figs/wilson_table1.PNG). Please read the 
article for details.  

a. Motivate your choice of priors with prior predictive simulations.
b. List coefficients for both models and compare the the coefficients reported 
in Wilson & Rule's Table 1.
c. Explain in your own words the meaning of the odds-ratio for trustworthiness. 
What comparison does it refer to?
d. Plot posterior density with 90 % CI for the trustworthiness odds-ratio. 
Include estimates from the two models in the same plot.
```

<br>

[Follow this link to find
data](https://bitbucket.org/matni/matni.bitbucket.org/raw/4cd0ba4afd03be26670efa852134bc13eac71745/rmsnotes/datasets/wilson2015_data_study1.txt){target="blank"}
Save (Ctrl+S on a PC) to download as text file

[Articel
here](https://journals.sagepub.com/doi/pdf/10.1177/0956797615590992)

This code imports the data.

```{r}
# Read data file, link for download above
d <- read.table("wilson2015_data_study1.txt", header = TRUE, sep = ",")

# Only include variables used in Table 1 of the article
d <- d[ , c("sent", "trust", "zAfro", "attract", "maturity" , "zfWHR",
             "glasses", "tattoos" )]
```

TBA

<br>

**solution**

## MN8b {.unnumbered}

```{block MN8b, type = 'mce'}
a. Visualize the first model from MN8a in a plot with trustworthiness on the x-axis 
and probability of death sentence on the y-axis. Visual the model with a best 
fitting line and a band or separate lines illustrating uncertainty.
b. Do the same for the second model. Figure out a way to handle the covariates in 
such a plot. 
```

TBA

<br>

**solution**

<!--

## MN8c {.unnumbered}

```{block MN8c, type = 'mce'}
Express the model prediction of the first model (crude model) you fitted in MN8a 
as a (a) risk-difference, (b) risk-ratio and (c) odds-ratio for a low versus high 
value on trustworthiness. Defined low as the 5th percentile and high as the 95th 
percentile of the trustworthiness ratings, so the contrasts spans 90 % of the 
variation in trustworthiness. Make the contrasts low versus high (and not high 
versus low), as it is easier to interpret ratios greater than 1 than ratios 
less than 1. Report contrasts as point estimates with 90 % CI.
```

TBA

<br>

**solution**

-->

## MN9 {.unnumbered}

```{block MN9, type = 'mce'}
Load the UCBadmit data.
```

```{r, eval=FALSE}
library(rethinking)
data(UCBadmit)
d <- UCBadmit
```

```{block MN9b, type = 'mce'}
Fit four models using `ulam()`:

1. Binomial regression, `applicant.gender` as only predictor
2. Beta-binomial regression, `applicant.gender` as only predictor
3. Binomial regression, gender as predictor with `dept` as covariate
4. Beta-binomial regression, gender as predictor with `dept` as covariate.  

Comment on MCMC-diagnostics, visualize the model fits (e.g., as in Fig. 11.5 
of the book), and interpret each model fit as evidence for or against a gender 
bias in admissions. You may use code from the book (Chs. 11 and 12) for defining 
your models.
```

<br>

<!--

Define models

```{r}
# Model 1-3 from the book: those from Ch 12 with same variable names as those 
# in Ch 11.

# Binomial only gender (Book: R.code 11.29, model 11.7)
m1 <- alist(
  admit ~ dbinom(applications, p),
  logit(p) <- a[gid],
  a[gid] ~ dnorm(0, 1.5))

# Beta-Binomial only gender (Book: R.code 12.2, model 12.1)
m2 <- alist(
  admit ~ dbetabinom(applications, pbar, theta),
  logit(pbar) <- a[gid],
  a[gid] ~ dnorm( 0 , 1.5 ),
  transpars> theta <<- phi + 2.0,
  phi ~ dexp(1)
)

# Binomial gender + department (Book: R.code 11.32, model 11.7)
m3 <- alist(
  admit ~ dbinom(applications, p),
  logit(p) <- a[gid] + delta[dept_id],
  a[gid] ~ dnorm(0, 1.5) ,
  delta[dept_id] ~ dnorm(0, 1.5)
)

# Beta-Binomial gender + department
m4 <- alist(
  admit ~ dbetabinom(applications, pbar, theta),
  logit(pbar) <- a[gid] + delta[dept_id],
  a[gid] ~ dnorm( 0 , 1.5 ),
  delta[dept_id] ~ dnorm(0, 1.5),
  transpars> theta <<- phi + 2.0,
  phi ~ dexp(1)
)

```

<br>

Run models

```{r}
data(UCBadmit)
d <- UCBadmit

# Define data for ulam, m1 and m2
dat_list <- list(
  admit = d$admit,
  applications = d$applications,
  gid = ifelse(d$applicant.gender=="male", 1 , 2)
)

# Define data for ulam, m3 and m4 (add department ID)
dat_list2 <- dat_list
dat_list2$dept_id <- rep(1:6, each=2)

# Below pre-calculated
# m1f <- ulam(m1, data = dat_list, iter = 5e3, chains = 4, refresh = 0, log_lik = TRUE)
# m2f <- ulam(m2, data = dat_list, iter = 5e3, chains = 4, refresh = 0, log_lik = TRUE)
# m3f <- ulam(m3, data = dat_list2, iter = 5e3, chains = 4, refresh = 0, log_lik = TRUE)
# m4f <- ulam(m4, data = dat_list2, iter = 5e3, chains = 4, refresh = 0, log_lik = TRUE)
#  
# save(m1f, m2f, m3f, m4f, file = "MN09.RData")
```

<br>

```{r}
load("MN09.RData")
# Check MCMC diagnostics, using precis()
precis(m1f, depth = 2)
precis(m2f, depth = 2)
precis(m3f, depth = 2)
precis(m4f, depth = 2)

# Below posterior checks

# Function that draw lines connecting points from same dept, 
# Book: Rcode 11.31 (p. 342)
drawlines <- function(){ 
  for ( i in 1:6 ) {
    x <- 1 + 2*(i-1)
    y1 <- d$admit[x]/d$applications[x]
    y2 <- d$admit[x+1]/d$applications[x+1]
    lines( c(x,x+1) , c(y1,y2) , col=rangi2 , lwd=2 )
    text( x+0.5 , (y1+y2)/2 + 0.05 , d$dept[x] , cex=0.8 , col=rangi2 )
  }
}

# Models with gender as only predictor 
par(mfrow = c(1, 2))
postcheck(m1f)  # Binomial
drawlines()
postcheck(m2f)  # Beta-binomial
drawlines()

# Models with gender and department as predictor 
par(mfrow = c(1, 2))
postcheck(m3f)  # binomial
drawlines()
postcheck(m4f)  # beta-binomial
drawlines()

# Compare coefficients
# coeftab(m1f, m2f, m3f, m4f)
```

**solution**

-   MCMC diagnostics seems OK for all models, 10,000 iterations,
    excluding warmup. Model 3 has some issue, see discussion in book on
    over-parametrization of this model (p. 345).\
-   Little support for gender discrimination from models m2, m3, m4,
    that is, the Beta-binomial models that allow for varying p across
    departments (m2, m4), and the Binomial model that include department
    (m3). The Beta-binomial models have wider CIs, as they allow
    variation in p (thus allow more uncertainty to start with), the one
    with department as predictor (m4) has CIs including the observed
    data.

<br>

Extra stuff, model 3 with one fewer parameter

```{r}
# Define data for ulam, m1 and m2
dat_list2z <- list(
  admit = d$admit,
  applications = d$applications,
  male = 1 * (d$applicant.gender=="male")
)

dat_list2z$dept_id <- rep(1:6, each=2)

# Binomial gender + department (Book: R.code 11.32, model 11.7)
m3z <- alist(
  admit ~ dbinom(applications, p),
  logit(p) <- a*male + delta[dept_id],
  a ~ dnorm(0, 1.5) ,
  delta[dept_id] ~ dnorm(0, 1.5)
)

#m3fz <- ulam(m3z, data = dat_list2z, iter = 5e3, chains = 4, refresh = 0, #log_lik = TRUE)

#save(m3fz, file = "MN09b.RData")

load("MN09b.RData")

# Precis to see fit measures
precis(m3fz, depth = 2)
precis(m3f, depth = 2)

# compare parameters
# coeftab(m3fz, m3f)

# Plot posterior checks for the two parametrizations
par(mfrow = c(1, 2))
postcheck(m3fz)
drawlines()
postcheck(m3f)
drawlines()
```

<br>

-->

## MN10 {.unnumbered}

```{block MN10, type = 'mce'}
Here from exercise 8H4:   

The values in data(nettle) are data on language diversity in 74 nations. The 
meaning of each column is given below.    

(1) `country`: Name of the country,
(2) `num.lang`: Number of recognized languages spoken,
(3) `area`: Area in square kilometers,
(4) `k.pop`: Population, in thousands,
(5) `num.stations`: Number of weather stations that provided data for the next 
two columns,
(6) `mean.growing.season`: Average length of growing season, in months,
(7) `sd.growing.season`: Standard deviation of length of growing season, in months.  


In 8H4, linear (Gaussian) regression was used to model `num.lang` (measure of 
language diversity) as a function of `mean.growing.season` and 
`sd.growing.season`. The idea being that language diversity is related to 
food security. With plenty of food, cultural groups can be smaller and more 
self-sufficient, leading to more languages per capita (see the book, p. 261, 
exercise 8H4 for more details). McElreath points out that:  

"A count model would be better here, but you'll learn that in Chapter 11" (p. 262).  
Now's the time! 

(a) Fit a Poisson model with `mean.growing.season` and 
`sd.growing.season` and their interaction as predictors (cf. 8H4c). Visualize 
model predictions in plots with regression lines and compatibility intervals.

(b) Repeat, but now using a Gamma-Poisson model. Compare regression 
coefficients from the Poisson and the Gamma-Poisson model.
```

<br>

**solution**

<!--

In 8H4, the outcome was number of languages per capita. Here I first repeat the 
interaction model from 8H4 (Normal regression).


```{r}
# Get data
data(nettle)
d <- nettle

# Define outcome
d$lang.per.cap <- log(d$num.lang / d$k.pop)  # Outcome, on log scale

# Standardized predictors
d$zgrow_season <- (d$mean.growing.season - mean(d$mean.growing.season))/
                   sd(d$mean.growing.season)
d$log_area <- log(d$area)   
d$zlogarea <- (d$log_area - mean(d$log_area))/sd(d$log_area) 
d$zsd_grow_season <- (d$sd.growing.season - mean(d$sd.growing.season))/
                   sd(d$sd.growing.season)

# Fit model, uisng quap() (becasue its faster!)
s3 <- quap(flist = alist(
  lang.per.cap ~ dnorm(mu, sigma),
  mu <- b0 + b1 * zgrow_season + b2 * zsd_grow_season + 
        b3 * zgrow_season * zsd_grow_season + b4 * zlogarea,
  b0 ~ dnorm(0, 1), 
  b1 ~ dnorm(0, 0.5), 
  b2 ~ dnorm(0, 0.5), 
  b3 ~ dnorm(0, 0.5), 
  b4 ~ dnorm(0, 0.5), 
  sigma ~ dexp(1)
), data = d)

precis(s3)

# Triptych plot ala McElreath.
set.seed(123)

triptych <- function(zsd = -1) {
  plot(d$zgrow_season, d$lang.per.cap, xlab = "Mean growing season (z-score)",
     ylab = "Log language diversity per 1k", xlim = c(-2.5, 2.5), pch = "")

  # Add line for zsd_grow_season = zsd
  mu <- link(s3, n = 1e4, data = list(zgrow_season = seq(-3, 3, 0.1), 
                           zsd_grow_season = zsd, zlogarea = mean(d$zlogarea)))
  lines(seq(-3, 3, 0.1), apply(mu, 2, mean), lty = 2, col = "black")
  shade(apply(mu, 2, HPDI, prob = 0.95), seq(-3, 3, 0.1))
  mtext(side = 3, text = sprintf("SD growing season = %0.f", zsd), cex = 0.7)
}

par(mfrow = c(1, 3))
triptych(zsd = -1)
triptych(zsd = 0)
triptych(zsd = 1)
```

<br>

Below same model but using Poisson regression. I used `num.lang` as outcome, and 
population as exposure (see Section 11.2.3, p. 357). The model use 
`mean.growing.season` and `sd.growing.season` and their interaction 
as predictors, and log(`area`) as covariate, all standardized. 

<br>

Select data for ulam() model.
```{r}
dat <- list(numlang = d$num.lang,  # Outcome
            logpop = log(d$k.pop), # Log population as exposure
            grow = d$zgrow_season, # Main predictor 1, z-score
            sdseason = d$zsd_grow_season,     # Main predictor 2, , z-score
            area = d$zlogarea,    # Covariate, z-score
            gxs = d$zgrow_season *  d$zsd_grow_season  # Interaction term
            ) 
```

<br>

```{r}
mn10.1code <- alist(
           numlang ~ dpois(lambda),
           log(lambda) <- logpop + b0 + b1*grow + b2 * sdseason + b3 * gxs + b4*area,
           b0 ~ dnorm(0, 1), 
           c(b1, b2, b3, b4) ~ dnorm(0, 1)  # Check priors later on!
          )

# Precalcualted to save time
#mn10.1 <- ulam(flist = mn10.1code, data = dat, chains = 4, iter = 4e3, refresh = 0)
#save(mn10.1, file = "MN10.RData")

load("MN10.RData")
precis(mn10.1)
```

<br>

Plot data

```{r}
# Triptych plot ala McElreath.
set.seed(123)

# Link by hand
mn10post <- data.frame(extract.samples(mn10.1))

mulink <- function(grow, zsd) {
  # Model predictions for grow at a specific value of zsd, 
  # assuming area = 0 (i.e. log-area at its mean)
  out <- mn10post$b0 + mn10post$b1*grow + mn10post$b2*zsd + mn10post$b3*zsd*grow
  out  
}

grow <- seq(-3, 3, 0.1)  # New data for grow, z-scores

triptych2 <- function(zsd = -1) {
  mu <- sapply(grow, mulink, zsd = zsd)
  ypred <- apply(mu, 2, median)
  y_ci <- apply(mu, 2, PI, prob = 0.95)

  plot(d$zgrow_season, d$lang.per.cap, xlab = "Mean growing season (z-score)",
     ylab = "Log language diversity per 1k", xlim = c(-2.5, 2.5), pch = "")
  lines(grow, ypred)
  shade(y_ci, grow)
  mtext(side = 3, text = sprintf("SD growing season = %0.f", zsd), cex = 0.7)
}

par(mfrow = c(1, 3))
triptych2(zsd = -1)
triptych2(zsd = 0)
triptych2(zsd = 1)
```


**solution**

Similar trend as the Gaussian model, but much tighter compatibility intervals. 
Probably data is overdispersed relative to the assumptions of the Poisson model, 
see next exercise MN11.


```{r}
mn11.1code <- alist(     
           numlang ~ dgampois(lambda, phi),
           log(lambda) <- logpop + b0 + b1*grow + b2 * sdseason + b3 * gxs + b4*area,
           b0 ~ dnorm(0, 1), 
           c(b1, b2, b3, b4) ~ dnorm(0, 1),  # Check priors later on!
           phi ~ dexp(1)
          )

# Precalculated to save time
# mn11.1 <- ulam(flist = mn11.1code, data = dat, chains = 4, iter = 4e3, refresh = 0)
# save(mn11.1, file = "MN11.RData")
load("MN11.RData")
precis(mn11.1)
```
<br>


```{r}
# Triptych plot ala McElreath.
set.seed(123)

# Link by hand
mn11post <- data.frame(extract.samples(mn11.1))

mulink <- function(grow, zsd) {
  # Model predictions for grow at a specific value of zsd, 
  # assuming area = 0 (i.e. log-area at its mean)
  out <- mn11post$b0 + mn11post$b1*grow + mn11post$b2*zsd + mn11post$b3*zsd*grow
  out  
}

grow <- seq(-3, 3, 0.1)  # New data for grow, z-scores

triptych2 <- function(zsd = -1) {
  mu <- sapply(grow, mulink, zsd = zsd)
  ypred <- apply(mu, 2, median)
  y_ci <- apply(mu, 2, PI, prob = 0.95)

  plot(d$zgrow_season, d$lang.per.cap, xlab = "Mean growing season (z-score)",
     ylab = "Log language diversity per 1k", xlim = c(-2.5, 2.5), pch = "")
  lines(grow, ypred)
  shade(y_ci, grow)
  mtext(side = 3, text = sprintf("SD growing season = %0.f", zsd), cex = 0.7)
}

par(mfrow = c(1, 3))
triptych2(zsd = -1)
triptych2(zsd = 0)
triptych2(zsd = 1)
```


<br>

**solution**  

Wider compatibility bands compared to the Poisson model (MN10). Make sense, as the 
Poisson model comes with the strong assumption of variance equal to 
expected value (lambda). The Gamma-Poisson model allows for larger standard 
deviations than assumed by the Poisson model.


<br>

-->