---
title: "RMS notes"
author: "Mats E. Nilsson"
date: "2025-01-16"
site: bookdown::bookdown_site
documentclass: book
bibliography: ["references.bib"]
link-citations: true
github-repo: 
url: 
description: ""
editor_options: 
  markdown: 
    wrap: 72
---


```{=html}
<script>
  addKlippy('left', 'top', 'auto', '1', 'Copy code', 'Copied!');
</script>
```

# Preface {.unnumbered}

## Welcome to Research Methods 1 and Stat 1, fall 2024 {.unnumbered}

In this book I collect my notes to the courses Research Methods 1 and
Statistics 1, both part of the Master program in psychology, at
Department of Psychology, Stockholm University. Lecture notes for Method
1 are in odd numbered chapters, notes for Stat 1 are in even numbered
chapters. I will update the notes as we go along.

<!--

### My Goals as a Teacher {.unnumbered}

1. Open Up New Ways of Thinking
2. Surpass Anyone’s Expectations of What You Can Achieve

-->

### Preliminaries {.unnumbered}

1.  **Athena**, you find course material on our learning platform
    Athena, see athena.su.se (or download the mobile app)
2.  **Schedule**, follow link at Athena
3.  **Literature**, see Athena
4.  **Examination**, see syllabus, available on Athena
5.  **R and R-studio**, For an introduction to R, please join the
    satellite course learn R by EXample (REX), where we'll practice our
    R skills


### Things you will learn and practice {.unnumbered}

Apart from general skills in Research methods and Statistics, the courses will
give you training in

- **Critical Thinking in Research Ethics**: Develop the ability to analyze and address 
ethical issues in research (Method1).
- **Open science skills**: Good data management practices, including reproducible 
analysis code (Stat1).
- **Data Simulation**: Learn to conduct data simulations for various purposes, 
including approximate problem-solving, power analysis, and verifying that your 
regression models function as intended (Method1 & Stat1).
- **R Programming**: Get started with coding in R, the leading language for statistical 
analysis (Method1 & Stat1). 
- **Effective Presentation Design**: Designing and delivering engaging slide 
presentations as part of group assignments (Method1 & Stat1).
- **Following Instructions**: Develop the ability to follow written instructions 
accurately; in this course crucial for successful group assignments (Method1 & Stat1).


### Why R? {.unnumbered}

Google "why learn R" to find out...

In Methods 1, we'll use R for data simulation to help us think clearly
about causality. And we will use R as our statistical software in
Stat 1. For those new to R, there is a satellite course **learn R by
EXample (REX)** that will run before lunch on some lecture days, see
here for material: [REX
webpage](https://matni.bitbucket.io/rex/docs/){target="blank"}

<br>

<!-- Code below add link to download file in folder download
<a href="download/rms_concepts24.xlsx" download="rms_concepts24.xlsx" class="btn btn-primary">Download Excel File</a>
-->

## Datasets {.unnumbered}

Here links to data sets that may be used in some lectures. Follow link,
then press Ctrl+S (or corresponding on Mac) to save as .txt file.

Data sets from ROS: Regression and Other Stories, @gelman2020regression.

-   [kidiq.txt.txt](https://bitbucket.org/matni/matni.bitbucket.org/raw/2764bd7e58ab2b24b69cdd82dee3102a8e42283b/rmsnotes/datasets/kidiq.txt){target="blank"}
-   [wells.txt](https://bitbucket.org/matni/matni.bitbucket.org/raw/f2ab8616653393dd021d2ff2941deb908e2b905c/rmsnotes/datasets/wells.txt){target="blank"} 
-   [electric_wide.txt](https://bitbucket.org/matni/matni.bitbucket.org/raw/545eea120a8ab57d7349eade1eb6a4cd30fe826d/rmsnotes/datasets/electric_wide.txt){target="blank"} 

Two other data sets:

-   [rt_howell.txt](https://bitbucket.org/matni/matni.bitbucket.org/raw/2764bd7e58ab2b24b69cdd82dee3102a8e42283b/rmsnotes/datasets/rt_howell.txt){target="blank"}
-   [sat_howell.txt](https://bitbucket.org/matni/matni.bitbucket.org/raw/2764bd7e58ab2b24b69cdd82dee3102a8e42283b/rmsnotes/datasets/sat_howell.txt){target="blank"}


<!-- 
-   [nes_selected.txt](https://bitbucket.org/matni/matni.bitbucket.org/raw/932fcc62ac6a6a9cd12cc8b9bbfb008e01899f6b/rmsnotes/datasets/nes_selected.txt){target="blank"}

-   [earnings.txt](https://bitbucket.org/matni/matni.bitbucket.org/raw/2764bd7e58ab2b24b69cdd82dee3102a8e42283b/rmsnotes/datasets/earnings.txt){target="blank"}
-->
