---
editor_options: 
  markdown: 
    wrap: 72
---

# M: Research Questions and Claims

## Topics {.unnumbered}

<div class="concepts">
<ul>
<li>Science and everyday thinking
<ul>
<li>Truth: Dichotomous (true or false)</li>
<li>Justification: A matter of degree</li>
<li>Resist dichotomous thinking, embrace uncertainty</li>
</ul></li>
<li>Research question
<ul>
<li>Give context (research area)</li>
<li>Motivate why important (research gap)</li>
<li>Formulate precisely (exposure and outcome variables(s), study
population)</li>
<li>Chose research design</li>
</ul></li>
<li>Error
<ul>
<li>Random error</li>
<li>Systematic error = bias</li>
</ul></li>
<li>Validity of research claims
<ul>
<li>Campbell’s validity typology:
<ul>
<li>Statistical conclusion validity</li>
<li>Internal validity</li>
<li>Construct validity</li>
<li>External validity</li>
</ul></li>
<li>Threats to validity</li>
<li>Design elements to counteract threats to validity</li>
</ul></li>
</ul>
<p>Theoretical articles to read:</p>
<ul>
<li><p><span class="citation">@steiner2023frameworks</span> on
frameworks for causal inference, the sections on Campbell’s validity
typology/threats framework</p></li>
<li><p><span class="citation">@gelman2020regression</span> mentions
internal and external validity as several places, see for example
chapter 18, pp. 354-355, where they use the term “threats to validity”
and discusses issues related to validity, such as the “Hawthorne effect”
and experimenter effects.</p></li>
</ul>
</div>

<br>

## Science and everyday thinking

<div class="citations">
<p>Science is nothing more than a refinement of everyday thinking.<br />
<font size="2"> Albert Einstein <span class="citation">[cited in
@haack2011defending]</span></font></p>
</div>

<br>

Science is nothing more than a refinement of everyday thinking. It's the
application of *common sense*, understood as "good sense" or "sound
reason" or "logical thinking". There is only one logic, and it applies
to all rational activities, be it research, criminal investigations,
medical diagnosis, or political decisions.

Unfortunately, our intuitions often lead us wrong, and what may appear a
sensible conclusion may turn out otherwise. The "refinement of everyday
thinking" is the wisdom that researcher has gained from thinking about
problems. Thinking a bit harder and in terms of alternative explanations
may help. Let's practice on this statement:

*Industrial workers are healthier than the general population. Thus,
work in industry is good for your health.*

Please find alternative explanations.

### Justification {.unnumbered}

Research aims at increasing our *knowledge*, typically factual knowledge
about the world. Philosophers may debate how to best define "knowledge",
but most would agree that *truth* and *justification* are the two key
elements. To know something requires that it is true. You may believe
that the earth is flat as a pancake, but you cannot know it because your
belief is false. But truth is not enough. I may claim that there are
living organisms on the planet Jupiter. If such organisms indeed are
discovered a hundred years from now, I was right, but still I didn't
know, because my claim was unjustified, I lacked good reasons. Maybe in
a dream I saw small germs crawling the surface of Jupiter. Dreams are
not good reasons for believing things and therefor may claim would be
dismissed as unjustified.

What is more important, being true or being justified? For a researcher,
the answer is straight forward, *research is all about justification*. A
scientific claim is only taken seriously if justified. It happens that
justified claims turn out to be false, because the agreement between
truth and the presence of good reasons is not perfect. But, luckily, it
is typically much easier to find good reasons for true than for false
claims. As every defense attorney knows, it is typically easier to
defend an innocent client than one guilty as charged. Simply because the
prosecutor will have a harder time finding good reasons for guilt of
innocent suspects.

The primacy of justification over truth is old news:

<div class="citations">
<p>Herodotus, in about 500 BC, discusses the policy decisions of the
Persian kings. He notes that a decision was wise, even though it led to
disastrous consequences, if the evidence at hand indicated it as the
best one to make; and that a decision was foolish, even though it led to
the happiest possible consequences, if it was unreasonable to expect
those consequences.</p>
<p><font size="2"> Cited in <span
class="citation">@jaynes1985bayesian</span> </font></p>
</div>

<br>

Justification is a matter of degree, so research claims are more or less
justified. That is why researchers typically speak of their results as
providing some degree of support of their hypothesis, not as conclusive
evidence for or against. Here examples of phrases, from weak to strong
support:

-   Our results are consistent with the notion that ...
-   Our results may suggest that ...
-   Our results weakly support our hypothesis that ...
-   Our results provide clear evidence for our hypothesis that ...
-   Our results strongly supports our hypothesis that ...
-   Our results, together with previous research, leave little doubt
    that ...

There will always be room for uncertainty in empirical research, and
that is why researchers think and talk about there results as more or
less in support of their hypothesis.

<br>

## Research question

As essential as it is to justify our research claims, it is equally
critical to ask the right question. The questions we pose drive the
research design, guiding the collection and analysis of data from which
we draw and justify our conclusions. However, formulating the right
question is no small task; it demands both creativity and insight. This
process involves identifying gaps in existing knowledge and intuitively
exploring new, promising directions. If successful, these new paths can
significantly advance our research field; if not, they may lead to
another dead end, a common outcome in research.

Once a general research idea is in place, considerable effort must be
invested in precisely articulating the research question. In research
papers, this question is typically presented in the Introduction,
following a structured approach:

1.  The opening paragraph(s) establish the broader context for the
    research problem.

2.  A literature review identifies the specific knowledge gap that
    necessitates the question.

3.  The research question is then concisely and precisely formulated,
    usually specifying the independent variable(s), dependent variable,
    and unit of analysis.

4.  A brief overview of the research design chosen to address the
    question is provided at the end of the Introduction.

Note that the research question can also be introduced as a hypothesis.
In such cases, the goal of the research is to evaluate this hypothesis.

Below is a practice question (1E1) that involves analyzing how the
research problem was described and justified in the Introduction of a
recent paper.

<br>

## Random and Systematic error

Two types of error:

-   **Random error**. The sum of many unknown and independent errors
    decrease with study size (the number of observations) or reliability
    of measurements. We will talk about three types of random error:
    -   **Random measurement error**: Measurements may underestimate or
        overestimate true scores, despite high test validity.
    -   **Sampling error**: Random sampling of participants from a
        target population may still lead to unrepresentative samples.
    -   **Randomization error**: Random assignment of participants to
        treatment to groups may still lead to unbalanced groups.
-   **Systematic error**, also known as **bias**. Stays constant no
    matter the study size or reliability of tests. Examples of bias:
    -   Confounding bias
    -   Selection bias
    -   Measurement bias

<br>

The Figure below is redrawn from Fig. 7.1 in @rothman2012epidemiology.


```r
x <- 1:1000

y_random <- 1/sqrt(x)
y_systematic <- rep(0.6, length(x))

par(mgp = c(0.2, 0.5, 0)) # First argument, distance label to axis

plot(log(x), y_random, pch = '', axes = FALSE, 
     xlab = "Study size", ylab = "Error", cex.lab = 0.9)
lines(log(x), y_random, lty = 2, col = "blue")
lines(log(x), y_systematic, col = "red")
axis(1, at = c(0, 7), labels = c("", ""), pos = 0, tck = 0)
axis(2, at = c(0, 1), labels = c("0", ""), pos = 0, tck = 0, las = 2,
     cex.axis = 0.8)

text(x = 1, y = 1, "Random error", cex = 0.8, col = "blue", font = 3)
text(x = 2.1, y = 0.65, "Systematic error (bias)", cex = 0.8, col = "red", font = 3)
```

<img src="01-method24_files/figure-html/code01.1-1.png" width="576" />

<br>

"Study size" may refer to number of tested individuals, as well as,
number of items or stimulus repetitions per individual. Thus, the figure
applies equally well to *reliability* and *validity* of individual
scores. Averaging over repeated measures (e.g., many items in a
questionnaire or may repetitions of the same stimulus in a psychophysial
experiment) reduces random error but not systematic error (see also
Chapter 5).

<br>

## Validity: Types, Threats, and Tricks

Campbell and co-workers developed a qualitative approach to causal
inference that has been very influential in psychology and related
fields. It is a description of how researchers thinks (or should think)
when designing a study or when evaluating evidence from a published
study. It encourage us to think in terms of alternative explanations
(threats to validity) and to counteract these threats with clever design
elements (or design "tricks").

**Three steps**

1.  Specify **validity type**.
2.  Identify **threats to validity**.
3.  Apply **design elements** (tricks) to counteract threats to
    validity. i.e., to rule out plausible alternative explanations.

<br>

Here is @steiner2023frameworks definitions of the validity types

-   **Internal validity**. The validity of inferences about whether the
    observed association between treatment status T and outcome Y
    reflects a causal impact of T on Y. Thus, internal validity is about
    the assumptions required for *causal inference*.\
-   **Statistical conclusion validity**. The validity of inferences
    about the association (covariation) between the presumed treatment T
    and outcome Y. Thus, this validity type is about the assumptions
    required for making *statistical inference* from the realized
    randomization or sampling outcomes to the underlying target
    populations of possible treatment assignments and participants.\
-   **Construct validity**. The validity with which inferences are made
    from the operations and settings in a study to the theoretical
    constructs those operations and settings are intended to represent.
    It is about the correct labeling of variables and accurate *language
    use*.\
-   **External validity**. The validity of inferences about whether the
    observed cause-effect relationship holds over variations in
    participants, settings, treatments, outcomes, and times. External
    validity directly relates to the assumptions required for
    *generalizing* effects

Campbell's typology was developed in a time when psychology research
methods and statistics largely focused on whether associations were
significant or non-significant. This kind of dichotomous thinking has
many problems, see for example, @Amrhein2019.

This table is my attempt to apply the typology to research aiming at
estimating effect sizes, rather than categorizing results as significant
versus non-significant.

<br>

| Validity type                   | Question                            | Issue                                |
|------------------|-------------------------|-----------------------------|
| Statistical conclusion validity | Estimates too uncertain?            | Statistical inference                |
| Internal validity               | Are causal estimates biased?        | Causal inference                     |
| Construct validity              | What was measured, really?          | Language use (labeling of variables) |
| External validity               | Relevant outside the study context? | Generalization                       |

<br>

### Example study {.unnumbered}

We conducted a between-subjects experiment where participants performed
a simple task (proof-reading) either in quiet (control group) or while
exposed to loud noise (treatment group) from loudspeakers in our
sound-proof laboratory. We hypothesized that the treatment group would,
on average, perform substantially worse than the control group on our
outcome measure: the number of errors made on the simple task. This
error count served as our measure of the construct 'noise-induced
stress'.

**Statistical conclusion validity**

1.  *Question*: Are effect-size estimates precise enough to say anything
    about direction and practical relevance of the results?
2.  *Threat*: Large compatibility intervals due to small sample sizes.
    Results inconclusive, as the observed data would be compatible with
    large as well as negligible or even reversed effects.
3.  *Design element*: Increased sample size to improve the precision of
    the estimated group difference (i.e., to obtain narrower
    compatibility intervals).

<br>

**Internal validity**

1.  *Question*: Would we be justified in claiming that the observed
    group difference is a valid estimate of the average *causal* effect
    of the treatment on performance in our sample?
2.  *Threat*. Group-threat: Maybe the groups were unequal to start with
    on factors related to performance. This systematic error may lead to
    underestimation or overestimation of the causal effect in the
    sample.
3.  *Design element*: Random assignment of participants to treatment
    groups, to obtain groups balanced on potentially confounding
    variables (measured and unmeasured).

<br>

**Construct validity**

1.  *Question*: Would we be justified in claiming that the observed
    effect is relevant to the construct of interest?
2.  *Threat*. Maybe performance errors in our laboratory is not a valid
    measure of our target construct (noise-induced stress).
3.  *Design element*: Multiple outcome measures, for example,
    self-reported stress and physical stress indicators (e.g., heart
    rate, stress hormones, etc.). If they all point in the same
    direction, this would make us more justified in generalizing from
    measurements to construct.

<br>

**External validity**

1.  *Question*: Would we be justified in claiming that the size of the
    causal effect applies in a realistic environment (and not only to
    the participants in our experiment)?
2.  *Threat*. People may behave different in our unrealistic setting
    (sound laboratory) than in real life, such as in a work place.
3.  *Design element*: Conduct the experiment in a real workplace setting
    using concealed loudspeakers, ensuring that the purpose of the study
    remains masked from the workers, as much as ethical guidelines
    permit.

<br>

## Practice {.unnumbered}

The practice problems are categorized as Easy (E), Medium (M), and Hard
(H). Credit goes to Richard McElreath and his excellent book,
"Statistical Rethinking" (@McElreath2020), from which I adapted this
layout of practice problems.

### Easy {.unnumbered}

<div class="practice">
<p><strong>1E1.</strong> Read the Introduction section of <span
class="citation">@michal2024practical</span> and identify the
sentence(s) where they:</p>
<ol style="list-style-type: lower-alpha">
<li>Introduce the research area.<br />
</li>
<li>Identify the knowledge gap that justifies the research.<br />
</li>
<li>Formulate the research question or hypothesis.<br />
</li>
<li>Describe and justify their design strategy.<br />
</li>
<li>Rephrase (c) into a single question that incorporates the key
independent and dependent variables, along with the study units.</li>
</ol>
<p><font size="2"> <a href="https://doi.org/10.1177/09567976241231506"
target="_blank">Find article here</a> </font></p>
</div>

<br>

<div class="practice">
<p><strong>1E2.</strong> Explain the difference between random and
systematic error, with reference to:</p>
<ol style="list-style-type: lower-alpha">
<li><p>Scores on an exam in Research Methods.</p></li>
<li><p>Difference in average grades between two schools in a specific
year.</p></li>
<li><p>Average scores of control and treatment group in a randomized
experiment.</p></li>
</ol>
</div>

<br>

<div class="practice">
<p><strong>1E3.</strong> Please find alternative explanations:</p>
<ol style="list-style-type: lower-alpha">
<li><p>“Industrial workers are healthier than the general population.
Thus, work in industry is good for your health.”</p></li>
<li><p>“Children in schools exposed to aircraft noise have higher grades
than children in non-exposed schools. Thus, aircraft noise cannot have
adverse effects on children’s learning.”</p></li>
<li><p>“Productivity increased after we improved the lighting. This
proves that good lighting conditions is important for
productivity.”</p></li>
<li><p>“With increasing traffic volumes, exposure to residential
road-traffic noise has increased substantially over the last decades. In
the same period, we have seen a remarkable reduction in the number of
heart attacks. Thus, road-traffic noise cannot cause heart attacks, as
some noise researchers seem to suggest.”</p></li>
</ol>
</div>

<br>

<div class="practice">
<p><strong>1E4.</strong> The design elements <em>random selection of
participants from a population</em> and <em>random assignment of
participants to treatment conditions</em> serve distinct purposes.</p>
<p>Explain these purposes in the context of Campbell’s validity
typology.</p>
<p><font size="2"> This is an old exam question: </font></p>
</div>

<br>

### Medium {.unnumbered}

<div class="practice">
<p><strong>1M1.</strong></p>
<ol style="list-style-type: lower-alpha">
<li><p>Explain with an example how the “placebo effect” may pose a
threat to the validity of a research claim.</p></li>
<li><p>The placebo effect is often thought of as a threat to internal
validity, but may be better viewed as a threat to construct validity.
Explain.</p></li>
</ol>
<p><font size="2"> Note. And remember: Understanding why a threat to
validity, like the placebo effect, might mislead us is crucial;
accurately classifying it within Campbell’s typology is less important.
</font></p>
</div>

<br>

<div class="practice">
<p><strong>1M2.</strong> Campbell’s validity typology can be simplified
into two main categories: internal and external validity. In which of
these categories would you place statistical conclusion validity and
construct validity? Explain your reasoning.</p>
</div>

<br>

<div class="practice">
<p><strong>1M3.</strong> Please find alternative explanations:</p>
<ol style="list-style-type: lower-alpha">
<li><p>“The top 10 schools in terms of average performance on the
national standardized tests were all small schools with fewer than 500
pupils. Reducing school size would increase school
performance.”</p></li>
<li><p>“A flurry of deaths by natural causes in a village led to
speculation about some new and unusual threat. A group of priests
attributed the problem to the sacrilege of allowing women to attend
funerals, formerly a forbidden practice. The remedy was a decree that
barred women from funerals in the area. The decree was quickly enforced,
and the rash of unusual deaths subsided. This proves that the priests
were correct.</p></li>
</ol>
<p><font size="2"> b. taken from the excellent book “How We Know What
Isn’t So” <span class="citation">@gilovich2008how</span>. </font></p>
</div>

### Hard {.unnumbered}

<div class="practice">
<p><strong>1H1.</strong> When randomly assigning participants to either
the control or treatment condition, you might end up with groups that
are imbalanced on important covariates purely by chance (known as
“randomization error”).</p>
<p>Is this imbalance a random error, systematic error (bias), or both?
Discuss.</p>
</div>

<br>

<div class="practice">
<p><strong>1H2.</strong> “The reviewer raised concerns about the small
sample size, but this should not be an issue, given that the difference
I observed was highly statistically significant.”</p>
<p>Explain why the reviewer had a point despite the statistical
significance of the findings.</p>
<p><font size="2"> Note. You may consult <span
class="citation">@gelman2020regression</span>, Ch. 16.1: <em>The
winner’s curse in low-power studies</em>.<br />
</font></p>
</div>

<br>

<div class="practice">
<p><strong>1H3.</strong></p>
<p>“Babies to smoking mothers have an increased risk of mortality within
one year, compared to babies to non-smoking mothers. However, among
babies born with low birth weight (LBW), the opposite seems to be true,
so a smoking mother is protective for LBW babies”.</p>
<p>Please find an alternative explanation.</p>
<p><font size="2"> Note. This one is hard, we will discuss it later on,
but if you want to jump ahead, please check out <span
class="citation">@banack2014obesity</span>.<br />
</font></p>
</div>

<br>

## (Session Info) {.unnumbered}


```r
sessionInfo()
```

```
## R version 4.1.3 (2022-03-10)
## Platform: x86_64-w64-mingw32/x64 (64-bit)
## Running under: Windows 10 x64 (build 26100)
## 
## Matrix products: default
## 
## locale:
## [1] LC_COLLATE=Swedish_Sweden.1252  LC_CTYPE=Swedish_Sweden.1252   
## [3] LC_MONETARY=Swedish_Sweden.1252 LC_NUMERIC=C                   
## [5] LC_TIME=Swedish_Sweden.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## loaded via a namespace (and not attached):
##  [1] bookdown_0.27   digest_0.6.29   R6_2.5.1        jsonlite_1.8.9 
##  [5] magrittr_2.0.3  evaluate_0.15   highr_0.9       stringi_1.7.6  
##  [9] rlang_1.1.3     cli_3.6.2       rstudioapi_0.13 jquerylib_0.1.4
## [13] bslib_0.3.1     rmarkdown_2.14  tools_4.1.3     stringr_1.4.0  
## [17] xfun_0.31       yaml_2.3.5      fastmap_1.1.0   compiler_4.1.3 
## [21] htmltools_0.5.2 knitr_1.39      sass_0.4.1
```
