# Notes REX07 {-}


```{r setup, cache = FALSE, echo = FALSE}
# This will allow error messages to be converted to HTML
knitr::opts_chunk$set(error = TRUE)
```

## Control statements {-}

Sometimes you may want the code to be executed only under specific conditions 
(`if, else`), or you may want to repeat a procedure several times on different 
objects each time (`for loops`). Here we will look at how to do this using the 
control statements, `if, else` and `for loops`. There are several others, such as 
`switch` statements and `while loops` that you may want to explore on your own.

<br>

### if, if else, else, ifelse() {-}

Here a simple example using the `if` statement in a function.

```{r}
# A function that center the input its mean, and divide by sd if z = 1. 

center <- function(x, z = 0){
  out <- x - mean(x)
  
  if (z == 1) {  # code in curly brackets executed only if z == 1
    out <- out/sd(x)
  }
  
  out
}

# Random data
set.seed(123)
x <- rnorm(30, 100, 15) 

# Check function
xcenter <- center(x)
xz <- center(x, z = 1)

stats <- c(meanx = mean(x), sdx = sd(x), 
           meanxcenter = mean(xcenter), sdxcenter = sd(xcenter),
           meanxz = mean(xz), sdxz = sd(xz))
round(stats, 2)
```

<br>

Here developed to include more than two alternative, using `if else` statements


```{r}
# A function that recode the input variable using one of three methods:
# 1. centering: mean = 0, 
# 2. z-transform: mean = 0 & sd = 1, 
# 3. Set range to min = 0, max = 1

recode <- function(x, method){
  
  if (method == 1) {  # code in curly brackets executed only if method == 1
    out <- x - mean(x)
  } else if (method == 2){ # code in curly brackets executed only if method == 2
    out <- (x - mean(x)) / sd(x)
  } else { # code in curly brackets executed only if method is neither 1 nor 2
    out <- (x - min(x)) / (max(x) - min(x))
  }
  
  out
}

# Random data
set.seed(123)
x <- rnorm(30, 100, 15) 

# Check function
xcenter <- recode(x, method = 1)
xz <- recode(x, method = 2)
x01 <- recode(x, method = 3)

summary(data.frame(x = x, xcenter = xcenter, xz = xz, x01 = x01))
```

<br>

Note that method 3 was automatically implemented if method was not set to 1 
or 2. I usually prefer to spell out all logical conditions, as in this revised 
version of the function.

```{r}
# A function that recode the input variable using one of three methods:
# 1. centering: mean = 0, 
# 2. z-transform: mean = 0 & sd = 1, 
# 3. Set range to min = 0, max = 1

recode <- function(x, method){
  
  if (method == 1) {  
    out <- x - mean(x)
  } else if (method == 2){ 
    out <- (x - mean(x)) / sd(x)
  } else if (method == 3) { 
    out <- (x - min(x)) / (max(x) - min(x))
  } else {  # Give an error message and stop R if method is none of 1, 2 or 3
    stop ("ERROR: the method argument can only be given values 1, 2 or 3")
    # If you just want to give a warning and not stop R, use: warning("message") 
  }
  
  out
}

recode(x, method = 4)
```
<br>

For quick if-else operations, you may use the function `ifelse(test, yes, no)`.

```{r}
# median split of x (defined above), using ifelse(test, yes, no). 
# test: is x lower than its median? If yes, set to zero, 
# if no set to 1. 
xmdn_split <- ifelse(x < median(x), 0, 1)
table(xmdn_split)
```

<br>

### for loop {-}

Here is a small data set with responses, `y` from nine individuals, `id`, on five 
experimental conditions `cond`, stored in long format (see below first 10 rows of 
the data frame). 
```{r}
set.seed(123)
n <- 9
cond <- rep(c(0, 1, 2, 3, 4) , n)
id <- rep(1:n, each = length(unique(cond)))
ind0 <- rep(runif(n, min = 10, max = 200), each = length(unique(cond)))  # Baseline mean for n individuals
y <- ind0 + 5 * cond + rnorm(n, 0, sd = 0.15*ind0)
d <- data.frame(id, cond, y)
head(round(d), 10)
```

<br> 

We would like to express the outcome scale in relation to the participants mean 
response, by recoding the outcome  

$y′ = y_i - mean(y_i) + mean(y_.)$,  

where $mean(y_i)$ is the mean response of participant $i$ and $mean(y_.)$ is the 
grand mean over all participants. This will set all individual responses to the 
same mean (the grand mean).

Here is one way to achieve this, using a for loop.

```{r}
d$y_recoded <- rep(NA, nrow(d)) # Create new empty column, to be filled as we 
                                # loop along.

y_grand_mean <- mean(d$y) # Grand mean y
                   
# Add recoded y-variable using a for loop
for (j in 1:nrow(d)) {  # Loop over each row in the data frame
  idrow <- d$id[j]  # ID at row j
  idmean <- mean(d$y[d$id == idrow])  # mean value for this id
  
  # Deduct individual mean value from y and add grand mean
  d$y_recoded[j] <- d$y[j] - idmean + y_grand_mean 
 }

summary(d) # Look at data frame after loop
```
<br>

Plot individual data, again using a for loop! (There are other ways to do this!)

```{r}
par(mfrow = c(1, 2))  # Set up for two plots next to each other
ids <- unique(d$id)  # Unique id numbers

##  Untransformed profiles -----------------------------------------------------
# Empty plot
plot(d$cond, d$y, pch = "", ylim = c(0, 300),
       xlab = "Experimental condition", ylab = "Response")  # Empty plot

# Add line for each participant
for (j in ids) {
  lines(0:4, d$y[d$id == j], col = "grey")
  points(0:4, d$y[d$id == j], pch = as.character(j))
}


##  Transformed profiles -----------------------------------------------------
# Empty plot
plot(d$cond, d$y_recoded, pch = "", ylim = c(0, 300),
       xlab = "Experimental condition", ylab = "Grand-mean transformed response")  

# Add line for each participant
for (j in ids) {
  lines(0:4, d$y_recoded[d$id == j], col = "grey")
  points(0:4, d$y_recoded[d$id == j], pch = as.character(j))
}

# Note: The code above repeats the same idea twice, would be better (and safer) 
# to create a function that does it in a general way, and then just do two 
# function calls.
```

<br>

## Script: VVG and aggregssion, continued {-}

Last time we simulated data according to the DAG below, and fitted some models.

Today we will look closer at how the simulation was constructed.


```{r}
library(dagitty) 

vvg_dag <- dagitty( "dag {
   VVG -> AGG
   VVG -> PA -> AGG
   U -> PA -> BMI
   VVG <- Support -> SProb -> AGG
   SProb <- U -> AGG
}")

coordinates(vvg_dag) <- list(
  x = c(VVG = 1, Support = 1, BMI = 2, PA = 3, SProb = 3, U = 4, AGG = 4),
  y = c(VVG = 5, Support = 1, BMI = 3, PA = 4, SProb = 2, U = 1, AGG = 5))

plot(vvg_dag)

```
  
  
Exposure: Violent video games (VVG)   

Outcome: Adult aggression (AGG)  

Observed covariates: Physical activity (PA), Parental support (Support), 
School problems (SProb), BMI (BMI)

Unobserved covariate: U (e.g., genetic predisposition to get into trouble)

<br>


The simulation do not care about units, so you may consider all variables 
as z-transformed with mean (about) zero and standard deviation roughly around one.

```{r, eval = FALSE}
# Simulating data consistent with a Directed Acyclical Graph(DAG):
# Violent video games (VVG) as exposure and Adult aggression (AGG) as outcome.
# Covariates: Physical activity (PA), Parental support (Support), 
# School problems (SProb), BMI (BMI).
# Unobserved covariate: U (e.g., genetic predisposition to get into trouble).
# 
# DAG:
# VVG -> AGG
# VVG -> PA -> AGG
# U -> PA -> BMI
# VVG <- Support -> SProb -> AGG
# SProb <- U -> AGG


## Clear things and set random seed --------------------------------------------
rm(list = ls())
graphics.off()
cat("\014")
set.seed(123)
## .............................................................................


## Simulate data ---------------------------------------------------------------
n = 1e3  # Sample size

# Define total causal effect of vvg on agg
dir_ce <- 0.3  # Direct causal effect of vvg on agg
bvvg_pa <- -1  # Causal effect of vvg on pa
bpa_agg <- -0.3 # Causal effect of pa on agg
ind_ce <- bvvg_pa * bpa_agg  # Indirect causal effect
total_ce <- dir_ce + ind_ce # This is what we want to estimate! (= PATE)

# Exogenous variables
support <- rnorm(n)  # Standardized scores, drawn from the Standard normal
u <- rnorm(n)

# Endogenous Variable
vvg <-   rnorm(n) - 0.5 * support
pa <-    rnorm(n) + bvvg_pa * vvg + 1 * u  # Check with non-zero coefficient!
bmi <-   rnorm(n) + -1.2 * pa
sprob <- rnorm(n) + -0.8 * support  + 0.5 * u
agg <-   rnorm(n) + 0.3 * vvg      + bpa_agg * pa   + 1.5 * sprob + 1 * u

# Combine in data frame (not needed, but makes it look like a real data set)
d <- data.frame(vvg, agg, support, pa, bmi, sprob, u)
## .............................................................................


## Estimate causal effects ------------------------------------------------------
# I am using lm() here, please try stan_glm(), same thing. lm() is much faster 
# for large n.

# To be estimated = Population average causal effect (PATE)
total_ce  

adj0 <- lm(agg ~ vvg, data = d)  # Bias: Omitted variable bias (= Confounding bias)

adj1 <- lm(agg ~ vvg + support, data = d) # No bias

adj2 <- lm(agg ~ vvg + sprob, data = d) # Bias: Collider bias

adj3 <- lm(agg ~ vvg + support + sprob, data = d) # No bias

adj4 <- lm(agg ~ vvg + pa + support, data = d) # Bias: Over-adjustment bias 

adj5 <- lm(agg ~ vvg + bmi + support, data = d) # Bias: Over-adjustment bias

# Note that model adj4 estimates correctly the direct causal effect (dir_ce), as 
# long as the path u -> pa is set to zero. If not, adj4 yields a biased estimate  
# of the direct effect (as well as of the total average causal effect). 
## .............................................................................


## Illustrate causal estimate --------------------------------------------------
# This function add a point with confidence interval to an existing plot
# Arguments: Model -- Regression model including the predictor vvg
#            xpos -- Position along x-axis of the point and CI
add_estimate <- function(model, xpos) {
  # Model estimate of causal effect
  estimate <- model$coefficients['vvg']
  # It's 95 % CI
  ci <- confint(model)['vvg',]
  
  # Draw CI in plot
  arrows(x0 = xpos, x1 = xpos, y0 = ci[1], y1 = ci[2], 
         length = 0.05, angle = 90, code = 3)
  # Draw point
  points(xpos, estimate, pch = 21, bg = 'grey')
}

# Make plot
plot(0, xlim = c(0, 5), ylim = c(-0.1, 1.5), pch = '', xlab = 'Model', 
     ylab = 'Causal estimate')
# Add line at true total average causal effect
lines(c(-1, 6), c(total_ce, total_ce), lty = 2)
# Add line at true direct average causal effect
lines(c(-1, 6), c(dir_ce, dir_ce), lty = 3, col = "grey")
# Add line at zero causal effect
lines(c(-1, 6), c(0, 0), lty = 1, col = 'grey')

# Use function to add data points and error bars (95 % CIs)
add_estimate(adj0, 0)
add_estimate(adj1, 1)
add_estimate(adj2, 2)
add_estimate(adj3, 3)
add_estimate(adj4, 4)
add_estimate(adj5, 5)
## .............................................................................

```

<br>

