# Notes REX10 {-}





## And more practice {-}

This is our last REX, and we will do some final practicing of R-skills with the 
data set `kidiq.txt` (from @gelman2020regression).

To repeat: The data is from a survey of adult American women and their 
children. @gelman2020regression describe the data set and use it at several places, 
e.g., pp. 130-136, 156-158, 161, 185-187, and 197. 

[Follow this link to find data](https://bitbucket.org/matni/matni.bitbucket.org/raw/3dee78fc9ace615a849b5ce8cb5abd381ebb1da6/rex/kidiq.txt){target="blank"} 
Save (Ctrl+S on a PC) to download as text file

<br>

**Code book**  

+ **kid_score** \ Test score children, on IQ-like measure (in the sample with 
mean and standard deviation of 87 and 20 points, respectively).
+ **mom_hs** \ Indicator variable: mother did complete high-school (1) or not (0)
+ **mom_iq** \ Test score mothers, transformed to IQ score: mean = 100, sd = 15. do   
+ **mom_work** \ Working status of mother, coded 1-4:
    - 1: mother did not work in first three years of child's life,
    - 2: mother worked in second or third year of child's life, 
    - 3: mother worked part-time in first year of child's life,
    - 4: mother worked full-time in first year of child's life. 
+ **mom_age** Age of mothers, years

The data file is comma-delimited and include variable names.


```r
d <- read.table("kidiq.txt", sep = ",", header = TRUE)
```
<br>

Recoding (from REX09)


```r
# Center mom_iq to be used as continuous variable in regression analysis
d$mom_iq_c <- d$mom_iq - mean(d$mom_iq)

# Center mom_age to mean age 
d$mom_age_c <- d$mom_age - mean(d$mom_age)

# Factor variable of mom_work
d$mom_work_f <- factor(d$mom_work, levels = c(1, 2, 3, 4),
                       labels = c("1_nowork", "2_worky2y3", 
                                  "3_parttime", "4_fulltime"))

# Linear transform kid_score to iq-scale: mean = 100, sd = 15
# Can be done in one step, I prefer to do it in two steps, via z-scores
zkid <- (d$kid_score - mean(d$kid_score)) / sd(d$kid_score)
d$kid_iq <- zkid * 15 + 100

str(d)
```

```
## 'data.frame':	434 obs. of  9 variables:
##  $ kid_score : int  65 98 85 83 115 98 69 106 102 95 ...
##  $ mom_hs    : int  1 1 1 1 1 0 1 1 1 1 ...
##  $ mom_iq    : num  121.1 89.4 115.4 99.4 92.7 ...
##  $ mom_work  : int  4 4 4 3 4 1 4 3 1 1 ...
##  $ mom_age   : int  27 25 27 25 27 18 20 23 24 19 ...
##  $ mom_iq_c  : num  21.12 -10.64 15.44 -0.55 -7.25 ...
##  $ mom_age_c : num  4.21 2.21 4.21 2.21 4.21 ...
##  $ mom_work_f: Factor w/ 4 levels "1_nowork","2_worky2y3",..: 4 4 4 3 4 1 4 3 1 1 ...
##  $ kid_iq    : num  84 108.2 98.7 97.2 120.7 ...
```

<br>

Let's look closer at one of the models fitted last time (REX09): 
adj2: `kid_iq ~ mom_hs + mom_iq_c + mom_age_c`. Below model fit using `lm()`,
displayed using `summary()` and `confint()`.


```r
# adj2: Adjust for mom_iq and mom_age
adj2 <- lm(kid_iq ~ mom_hs + mom_iq_c + mom_age_c, data = d)  
summary(adj2)
```

```
## 
## Call:
## lm(formula = kid_iq ~ mom_hs + mom_iq_c + mom_age_c, data = d)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -39.163  -9.128   1.763   8.248  36.869 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 96.73917    1.45234  66.609   <2e-16 ***
## mom_hs       4.15014    1.65917   2.501   0.0127 *  
## mom_iq_c     0.41342    0.04457   9.276   <2e-16 ***
## mom_age_c    0.16517    0.24307   0.680   0.4972    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 13.34 on 430 degrees of freedom
## Multiple R-squared:  0.215,	Adjusted R-squared:  0.2095 
## F-statistic: 39.25 on 3 and 430 DF,  p-value: < 2.2e-16
```

```r
confint(adj2)
```

```
##                  2.5 %     97.5 %
## (Intercept) 93.8846134 99.5937340
## mom_hs       0.8890467  7.4112384
## mom_iq_c     0.3258194  0.5010183
## mom_age_c   -0.3125774  0.6429197
```

<br>

Below some plots to inspect the model. These are not intended for publication, 
just for our understanding of model implications and model fit (diagnostics).

<br>

First, just for fun, a "Bubble plot" of `kid_iq` against `mom_iq`, with different 
symbols for `mom_hs` and symbol size proportional to `mom_age`.


```r
# Empty Plot, no symbols
plot(d$mom_iq, d$kid_iq, pch = "")

# Add data points. 
cex <- 2.5 * (d$mom_age/max(d$mom_age)) # Symbol size proportional to mom_age
points(d$mom_iq[d$mom_hs == 0], d$kid_iq[d$mom_hs == 0], pch = 16,
       col = rgb(0, 0, 1, 0.2), cex = cex)
points(d$mom_iq[d$mom_hs == 1], d$kid_iq[d$mom_hs == 1], pch = 16, 
       col = rgb(1, 0, 0, 0.2), cex = cex)
```

<img src="rex10_files/figure-html/unnamed-chunk-4-1.png" width="672" />

<br>

Here a plot of model predictions, keeping `mom_age` constant at 0 (mean age). 
Note that I use the centered version of mothers IQ `mom_iq_c`, and note that the 
pattern of data is the same as above, despite x-scale values are different.


```r
# Empty Plot, no symbols
plot(d$mom_iq_c, d$kid_iq, pch = "")

# Add data points. 
points(d$mom_iq_c[d$mom_hs == 0], d$kid_iq[d$mom_hs == 0], pch = 16,
       col = rgb(0, 0, 1, 0.5))
points(d$mom_iq_c[d$mom_hs == 1], d$kid_iq[d$mom_hs == 1], pch = 16, 
       col = rgb(1, 0, 0, 0.5))

# Add regression lines, separately for mom_hs = 0 and mom_hs = 1, 
# with mom_age set to 0
x <- seq(from = min(d$mom_iq_c), to = max(d$mom_iq_c), by = 1)
b <- coef(adj2)
y_hs0 <- b[1] + b[2] * 0 + b[3] * x + b[4] * 0
y_hs1 <- b[1] + b[2] * 1 + b[3] * x + b[4] * 0

lines(x, y_hs0, col = "darkred", lty = 1)
lines(x, y_hs1, col = "darkblue", lty = 2)
```

<img src="rex10_files/figure-html/unnamed-chunk-5-1.png" width="672" />

<br>

Here two diagnostic plots of the model fit, first residuals versus fitted (predicted) 
values and then observed versus fitted values.



```r
# Residuals versus predicted values, with lowess-line. A flat line around 0 
# is what we want to see.
plot(adj2$fitted.values, adj2$residuals, lty = 3)
abline(0, 0, col = "blue", lty = 3)
lines(lowess(adj2$fitted.values, adj2$residuals), col = "red", lty = 2)
```

<img src="rex10_files/figure-html/unnamed-chunk-6-1.png" width="672" />

```r
# Observed versus predicted values
plot(adj2$fitted.values, d$kid_iq, lty = 3, xlim = c(70, 130), ylim = c(70, 130))
abline(0, 1, col = "blue", lty = 3)
```

<img src="rex10_files/figure-html/unnamed-chunk-6-2.png" width="672" />
<br>

### expand.grid() {-}

A useful function for deriving model predictions is `expand.grid()` that creates 
a data-frame of all combinations of levels of the specified input variables. 
Here I use it to create a data-frame (I call it `nd`) with combinations of 
hypothetical values for `mom_hs`, `mom_iq`, and `mom_age`. Then I use this data 
frame as input to the `predict()` function, to obtain predicted values with 
confidence intervals.


```r
# Hypothetical values of hs, iq and age
hs <- c(0, 1)  # Mom high-school values
iq <- seq(from = -30, to = 30, by = 1)  # Centered IQ values
age <- seq(from = -6, to = 6, by = 1) # Centered age values

# All combinations of the above in a data frame
nd <- expand.grid(mom_hs = hs, mom_iq_c = iq, mom_age_c = age)
head(nd)
```

```
##   mom_hs mom_iq_c mom_age_c
## 1      0      -30        -6
## 2      1      -30        -6
## 3      0      -29        -6
## 4      1      -29        -6
## 5      0      -28        -6
## 6      1      -28        -6
```

```r
tail(nd)
```

```
##      mom_hs mom_iq_c mom_age_c
## 1581      0       28         6
## 1582      1       28         6
## 1583      0       29         6
## 1584      1       29         6
## 1585      0       30         6
## 1586      1       30         6
```

```r
# Predictions for each combination of predictor values according to model adj2
# This is a matrix with nrow(newdata) rows and 3 columns (prediction, lower ci, 
# upper ci) 
preds <- predict(adj2, newdata = nd, interval = "confidence", level = 0.95)

# Add to data frame newdata, just to keep all in one place
nd$m <-  preds[, 1]  # predicted value (m for mean)
nd$lo <- preds[, 2]  # CI 95 low
nd$hi <- preds[, 3]  # CI 95 hi
head(nd)
```

```
##   mom_hs mom_iq_c mom_age_c        m       lo       hi
## 1      0      -30        -6 83.34558 79.31689 87.37427
## 2      1      -30        -6 87.49572 83.19674 91.79471
## 3      0      -29        -6 83.75900 79.76956 87.74844
## 4      1      -29        -6 87.90914 83.66509 92.15320
## 5      0      -28        -6 84.17242 80.22068 88.12416
## 6      1      -28        -6 88.32256 84.13232 92.51280
```

```r
tail(nd)
```

```
##      mom_hs mom_iq_c mom_age_c        m       lo       hi
## 1581      0       28         6 109.3059 103.9939 114.6179
## 1582      1       28         6 113.4561 109.6982 117.2139
## 1583      0       29         6 109.7193 104.3566 115.0821
## 1584      1       29         6 113.8695 110.0604 117.6786
## 1585      0       30         6 110.1328 104.7182 115.5473
## 1586      1       30         6 114.2829 110.4213 118.1445
```

<br>

Here model predictions for mom_age = 0


```r
# Empty Plot, no symbols
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))

# Add regression lines, separately for mom_hs = 0 and mom_hs = 1, 
# with mom_age set to 0. This time using predictions in newdata

x <- nd$mom_iq_c[nd$mom_hs == 0 & nd$mom_age == 0]  # Same for mom_hs == 1

# Mom-hs = 0
y_hs0 <- nd$m[nd$mom_hs == 0 & nd$mom_age == 0]
lines(x, y_hs0, col = "red", lty = 1)

# Mom-hs = 1
y_hs1 <- nd$m[nd$mom_hs == 1 & nd$mom_age == 0]
lines(x, y_hs1, col = "blue", lty = 1)
```

<img src="rex10_files/figure-html/unnamed-chunk-8-1.png" width="672" />
<br>

Here separate plots for mom_age = -6, 0, and 6 year re mean age.


```r
# Add regression lines kid_iq versus mom_iq, with specific values for
# mom_hs and mom_age
add_line <- function(mom_hs, mom_age, col = "blue") {
  x <- nd$mom_iq_c[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  y <- nd$m[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  lines(x, y, col = col, lty = 1)
  
}

# Three plots next to each other
par(mfrow = c(1, 3))

# Age = -6
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at -6 y re mean age", cex = 0.7)
add_line(mom_hs = 0, mom_age = -6, col = "red")
add_line(mom_hs = 1, mom_age = -6, col = "blue")

# Age = 0
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at mean age", cex = 0.7)
add_line(mom_hs = 0, mom_age = 0, col = "red")
add_line(mom_hs = 1, mom_age = 0, col = "blue")

# Age = +6
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at +6 y re mean age", cex = 0.7)
add_line(mom_hs = 0, mom_age = 6, col = "red")
add_line(mom_hs = 1, mom_age = 6, col = "blue")
```

<img src="rex10_files/figure-html/unnamed-chunk-9-1.png" width="672" />

<br>

Not to exciting since age doesn't do much, but at least this is shown here.

<br>

Here I repeat the above, but add shaded regions for CIs.


```r
# Add regression lines kid_iq versus mom_iq, with specific values for
# mom_hs and mom_age
add_line2 <- function(mom_hs, mom_age, col = "blue", 
                      bandcol = rgb(0, 0, 1, 0.2)) {
  # Regression line
  x <- nd$mom_iq_c[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  y <- nd$m[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  lines(x, y, col = col, lty = 1)
  
  # CI bands
  ylo <- nd$lo[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  yhi <- nd$hi[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  polygon(c(x, rev(x)), c(ylo, rev(yhi)), border = NA, col = bandcol)
  
}

# Three plots next to each other
par(mfrow = c(1, 3))

# Age = -6
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at -6 y re mean age", cex = 0.7)
add_line2(mom_hs = 0, mom_age = -6, col = "red", bandcol = rgb(1, 0, 0, 0.2))
add_line2(mom_hs = 1, mom_age = -6, col = "blue", bandcol = rgb(0, 0, 1, 0.2))

# Age = 0
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at -6 y re mean age", cex = 0.7)
add_line2(mom_hs = 0, mom_age = 0, col = "red", bandcol = rgb(1, 0, 0, 0.2))
add_line2(mom_hs = 1, mom_age = 0, col = "blue", bandcol = rgb(0, 0, 1, 0.2))

# Age = +6
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at -6 y re mean age", cex = 0.7)
add_line2(mom_hs = 0, mom_age = 6, col = "red", bandcol = rgb(1, 0, 0, 0.2))
add_line2(mom_hs = 1, mom_age =6, col = "blue", bandcol = rgb(0, 0, 1, 0.2))
```

<img src="rex10_files/figure-html/unnamed-chunk-10-1.png" width="672" />


<br>

### All the above code, please copy to script {-}

Here is all the code above, please copy and paste into a script file. 


```r
## Analyses of data in kidiq.txt, see REX09 for code book

## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Load data -------------------------------------------------------------------
d <- read.table("kidiq.txt", sep = ",", header = TRUE)
## .............................................................................


## Recode variables ------------------------------------------------------------
# Center mom_iq to be used as continuous variable in regression analysis
d$mom_iq_c <- d$mom_iq - mean(d$mom_iq)

# Center mom_age to mean age 
d$mom_age_c <- d$mom_age - mean(d$mom_age)

# Factor variable of mom_work
d$mom_work_f <- factor(d$mom_work, levels = c(1, 2, 3, 4),
                       labels = c("1_nowork", "2_worky2y3", 
                                  "3_parttime", "4_fulltime"))

# Linear transform kid_score to iq-scale: mean = 100, sd = 15
# Can be done in one step, I prefer to do it in two steps, via z-scores
zkid <- (d$kid_score - mean(d$kid_score)) / sd(d$kid_score)
d$kid_iq <- zkid * 15 + 100
## .............................................................................


## Model adjusting for mom_iq and mom_hs ---------------------------------------
adj2 <- lm(kid_iq ~ mom_hs + mom_iq_c + mom_age_c, data = d)  
summary(adj2)
confint(adj2)
## .............................................................................


## Bubble plot! ----------------------------------------------------------------
# Empty Plot, no symbols
plot(d$mom_iq, d$kid_iq, pch = "")

# Add data points. 
cex <- 2.5 * (d$mom_age/max(d$mom_age)) # Symbol size proportional to mom_age
points(d$mom_iq[d$mom_hs == 0], d$kid_iq[d$mom_hs == 0], pch = 16,
       col = rgb(0, 0, 1, 0.2), cex = cex)
points(d$mom_iq[d$mom_hs == 1], d$kid_iq[d$mom_hs == 1], pch = 16, 
       col = rgb(1, 0, 0, 0.2), cex = cex)
##..............................................................................


## Plot with model predictions at age = 0 re mean  ----------------------------
# Empty Plot, no symbols
plot(d$mom_iq_c, d$kid_iq, pch = "")

# Add data points. 
points(d$mom_iq_c[d$mom_hs == 0], d$kid_iq[d$mom_hs == 0], pch = 16,
       col = rgb(0, 0, 1, 0.5))
points(d$mom_iq_c[d$mom_hs == 1], d$kid_iq[d$mom_hs == 1], pch = 16, 
       col = rgb(1, 0, 0, 0.5))

# Add regression lines, separately for mom_hs = 0 and mom_hs = 1, 
# with mom_age set to 0
x <- seq(from = min(d$mom_iq_c), to = max(d$mom_iq_c), by = 1)
b <- coef(adj2)
y_hs0 <- b[1] + b[2] * 0 + b[3] * x + b[4] * 0
y_hs1 <- b[1] + b[2] * 1 + b[3] * x + b[4] * 0

lines(x, y_hs0, col = "darkred", lty = 1)
lines(x, y_hs1, col = "darkblue", lty = 2)
##..............................................................................


## Diagnostic plots -----------------------------------------------------------
# Residuals versus predicted values, with lowess-line. A flat line around 0 
# is what we want to see.
plot(adj2$fitted.values, adj2$residuals, lty = 3)
abline(0, 0, col = "blue", lty = 3)
lines(lowess(adj2$fitted.values, adj2$residuals), col = "red", lty = 2)

# Observed versus predicted values
plot(adj2$fitted.values, d$kid_iq, lty = 3, xlim = c(70, 130), ylim = c(70, 130))
abline(0, 1, col = "blue", lty = 3)
##..............................................................................


## Data frame with predicted values --------------------------------------------
# Hypothetical values of hs, iq and age
hs <- c(0, 1)  # Mom high-school values
iq <- seq(from = -30, to = 30, by = 1)  # Centered IQ values
age <- seq(from = -6, to = 6, by = 1) # Centered age values

# All combinations of the above in a data frame
nd <- expand.grid(mom_hs = hs, mom_iq_c = iq, mom_age_c = age)
head(nd)
tail(nd)

# Predictions for each combination of predictor values according to model adj2
# This is a matrix with nrow(newdata) rows and 3 columns (prediction, lower ci, 
# upper ci) 
preds <- predict(adj2, newdata = nd, interval = "confidence", level = 0.95)

# Add to data frame newdata, just to keep all in one place
nd$m <-  preds[, 1]  # predicted value (m for mean)
nd$lo <- preds[, 2]  # CI 95 low
nd$hi <- preds[, 3]  # CI 95 hi
head(nd)
tail(nd)
##..............................................................................


## Model predictions, age = 0 --------------------------------------------------
# Same as above, but now from data frame nd

# Empty Plot, no symbols
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))

# Add regression lines, separately for mom_hs = 0 and mom_hs = 1, 
# with mom_age set to 0. This time using predictions in newdata

x <- nd$mom_iq_c[nd$mom_hs == 0 & nd$mom_age == 0]  # Same for mom_hs == 1

# Mom-hs = 0
y_hs0 <- nd$m[nd$mom_hs == 0 & nd$mom_age == 0]
lines(x, y_hs0, col = "red", lty = 1)

# Mom-hs = 1
y_hs1 <- nd$m[nd$mom_hs == 1 & nd$mom_age == 0]
lines(x, y_hs1, col = "blue", lty = 1)
##..............................................................................


## Model predictions, age = -6, 0, and +6  -------------------------------------
# Add regression lines kid_iq versus mom_iq, with specific values for
# mom_hs and mom_age
add_line2 <- function(mom_hs, mom_age, col = "blue", 
                      bandcol = rgb(0, 0, 1, 0.2)) {
  # Regression line
  x <- nd$mom_iq_c[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  y <- nd$m[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  lines(x, y, col = col, lty = 1)
  
  # CI bands
  ylo <- nd$lo[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  yhi <- nd$hi[nd$mom_hs == mom_hs & nd$mom_age == mom_age]
  polygon(c(x, rev(x)), c(ylo, rev(yhi)), border = NA, col = bandcol)
  
}

# Three plots next to each other
par(mfrow = c(1, 3))

# Age = -6
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at -6 y re mean age", cex = 0.7)
add_line2(mom_hs = 0, mom_age = -6, col = "red", bandcol = rgb(1, 0, 0, 0.2))
add_line2(mom_hs = 1, mom_age = -6, col = "blue", bandcol = rgb(0, 0, 1, 0.2))

# Age = 0
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at -6 y re mean age", cex = 0.7)
add_line2(mom_hs = 0, mom_age = 0, col = "red", bandcol = rgb(1, 0, 0, 0.2))
add_line2(mom_hs = 1, mom_age = 0, col = "blue", bandcol = rgb(0, 0, 1, 0.2))

# Age = +6
plot(d$mom_iq_c, d$kid_iq, pch = "", xlim = c(-30, 30), ylim = c(80, 120))
mtext(side = 3, text = "mom_age at -6 y re mean age", cex = 0.7)
add_line2(mom_hs = 0, mom_age = 6, col = "red", bandcol = rgb(1, 0, 0, 0.2))
add_line2(mom_hs = 1, mom_age =6, col = "blue", bandcol = rgb(0, 0, 1, 0.2))
##..............................................................................
```


<br>
