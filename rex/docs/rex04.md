# Notes REX04 {-}




You have already used several R functions. Today we'll take a closer look at 
how they work. Then we will take a closer look at assumptions behind our simulated 
experiment and what will happen to the results as we make changes to scenario we 
have been simulating.
  
<br>  

## Functions {-}
Functions are small programs that do things. Most functions require that you
specify what you want to be done by providing arguments, but some functions can
be run without arguments: 

```r
date()
```

```
## [1] "Thu Oct 17 19:26:07 2024"
```

A function's arguments is listed in its help page. Many functions have 
arguments with default values. For example, the `mean()`
function has as default not to excluded missing data: `na.rm = FALSE`. This
is a bit annoying, because R will give you an NA where you expected a number. But 
it is in fact helpful since it reminds you that your data contain missing values. 

```r
my_data <- rnorm(1000)
my_data[my_data > 2 ] <- NA  # Turn values > 2 into missing data (NA)
mean(my_data)  # The mean is NA if NAs are included
```

```
## [1] NA
```

```r
mean(my_data, na.rm = TRUE)  # This gives the mean after removing NAs
```

```
## [1] -0.08489021
```

If you don't specify what argument your are providing, R will assume 
that you're entering them in the same order as specified by the function. For
example, our favorite `rnorm()` has this order of arguments: 
`rnorm(n, mean = 0, sd = 1)`. 

So if you call `rnorm(3, 100, 15)`, R will assume that you mean n = 3,
mean = 100 and sd = 15 and give you three random numbers from N(100, 15). 

```r
set.seed(1)
rnorm(3, 100, 15)
```

```
## [1]  90.60319 102.75465  87.46557
```

Note that you can input them in another order if you specify each argument: 

```r
set.seed(1)
rnorm(sd = 15, n = 3, mean = 100)
```

```
## [1]  90.60319 102.75465  87.46557
```
When writing a script, it's generally a good idea to specify arguments also 
when they are provided in the right order. This reminds you and others reading 
your code what the arguments stand for.
  
<br>   


### Writing your own functions {-}
You can easily create your own functions. Here is an example of a function that
takes a numeric vector as input and returns a z-transformed version of the data. 

```r
z_transform <- function(x) {
  zvalue <- (x - mean(x)) / sd(x)
  zvalue  # This returns the output 
}
```
When you run this code, it is stored as an object in R's memory (you should see it 
in R-Studio's Global Environment panel) and will be available until you
close R or clear its workspace with `rm(list = ls())` .

```r
my_data <- c(1, 4, 9, 9, 14, 17)
my_z <- z_transform(my_data) 
my_z 
```

```
## [1] -1.3408030 -0.8380019  0.0000000  0.0000000  0.8380019  1.3408030
```

Note that the objects created inside a function (e.g., `zvalue` in 
`z_transform()`), 
are not available outside the function, so this will generate an error:

```r
zvalue
```

```
## Error in eval(expr, envir, enclos): object 'zvalue' not found
```

Here is a user-made function that calculates sample statistics (three digits as
default) and outputs these values in a named vector

```r
calculate_stats <- function(x, digits = 3) {
  n <- length(x)
  m <- mean(x)
  md <- median(x)
  s <- sd(x)
  minimum <- min(x)
  maximum <- max(x)
  out <- round(c(n, m, s, md, minimum, maximum), digits)
  names(out) <- c('n', 'mean', 'sd', 'median', 'min', 'max')
  out
}
```

Here is another user-made function:

```r
create_rsample <- function(n = 100, m = 100, s = 15) {
  # Creates a random sample of n observations with a specified mean (m) and 
  # standard deviation (s). The data is drawn from the normal distribution and
  # then linearly transformed to the specified mean and standard deviation.
  x <- rnorm(n)
  x <- (x - mean(x)) / sd(x)  # Sets mean(x) = 0 and sd(x) = 1
  x <- (x * s) + m  # Sets mean(x) = m and sd(x) = s
  x  # Return x
}
```

And here are both functions in action:

```r
sample1 <- create_rsample()
calculate_stats(sample1)
```

```
##       n    mean      sd  median     min     max 
## 100.000 100.000  15.000  99.461  61.264 138.291
```

```r
sample2 <- create_rsample(10, 50, 10)
calculate_stats(sample2)
```

```
##      n   mean     sd median    min    max 
## 10.000 50.000 10.000 50.221 37.242 63.329
```
If these were your first functions: Congratulations, you just wrote your first
computer programs!  

Note that it's a very good idea to comment your code carefully, including your 
functions (as I did above for `create_rsample()`). The time spend on commenting
your code is usually much less then the time you will need to understand your 
uncommented code when revisited on another day. 
  
<br>   


## Script: Simulation randomized experiment {-}

Here is another and simpler version of a randomized experiment than the one we 
worked on in previous REX. We will use this one to show how functions can be used 
to do a kind or "power" analysis.  

The experiment is about the performance on a standardized test. The experiment 
has a control (treat0) and experimental treatment (treat1): they 
differ in their instructions for the test, the standard instruction (treat0) and 
our new instruction (treat1) that we believe will improve most test-takers 
performance, with on average 5 units.  


```r
## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Simulate potential outcomes from a sample -----------------------------------
set.seed(123)  # Random seed for rnorm() function below

# Number of participants
n <- 60   # Make it an even number to have equal sized groups
id <- 1:n # Give each a number

# Population average causal effect (PATE) of treat1 versus treat0 
# The population mean would be pate units larger if all had been
# given treat1 versus treat0.
pate <- 5  # y1 - y0

# Potential outcome if treat0
y0 <- rnorm(n, mean = 100, sd = 15)

# Potential outcome if on treat1 (sd = 3, please change if you like)
y1 <- y0 + rnorm(n, mean = pate, sd = 3)

# Single-unit causal effects
delta <- y1 - y0

# Add potential outcomes to data frame g
g <- data.frame(id = id,y0 = y0, y1 = y1, delta = delta)
## .............................................................................


## Randomize the sample to treat0 and treat 1 and get observed data ------------
set.seed(123)  # Random seed for sample() function below

# Sample average treatment effect (SATE)
sate <- mean(g$delta)

# Random assignment to treat0 or treat1, half n to each group
temp <- c(rep(0, n/2), rep(1, n/2))  # Vector with n/2 zeros and n/2 ones
g$treat <- sample(temp, replace = FALSE)  # Random assignment using sample()

# Observed outcome
g$y_obs <- (1 - g$treat) * y0 + g$treat * y1
head(g)  # Visual sanity check 

#Prima facie causal effect: Mean of treat1 - Mean of treat0
pfacie <- mean(g$y_obs[g$treat == 1]) - mean(g$y_obs[g$treat == 0])

# Compare sate (true value) and pfacie (our estimate of the true value)
sate_pf <- c(sate = sate, pfacie = pfacie)
round(sate_pf, 1)
## .............................................................................
```

<br>

Here is how we may do this repeatedly, to estimate how randomization errors will
bias our estimate (pfacie) away from the true sample average treatment effect (sate)


```r
# This function assume that there is a data frame called g in the global 
# environment that contains y0 and y1 for our sample. We created it above! 

randomize <- function() {
  n <- nrow(g)  # Sample size = number of rows in g
  # Random assignment to treat0 or treat1, half n to each group
  temp <- c(rep(0, n/2), rep(1, n/2))  # Vector with n/2 zeros and n/2 ones
  g$treat <- sample(temp, replace = FALSE)  # New random assignment
  
  # Observed outcome
  g$y_obs <- (1 - g$treat) * y0 + g$treat * y1  # new observed outcome


  #Prima facie causal effect: Mean of treat1 - Mean of treat0
  pfacie <- mean(g$y_obs[g$treat == 1]) - mean(g$y_obs[g$treat == 0])

  pfacie # output pfacie estimate
}

# Run function once
randomize()

# Run function 10,000 times using replicate()
set.seed(222)
manypf <- replicate(1e4, randomize())

# Inspect
hist(manypf, breaks = 100)
lines(c(sate, sate), c(0, 1e4), col = "red")

# Proportion type-S error (wrong sign)
mean(manypf < 0)

# Proportion type-M error (here defined as more than 3 points off)
mean(abs(manypf - sate) > 3)
```


<br>

Last time we did some serious statistical analyses of our simulated 
diet-experiment . Below 
we will have a look at the simulation itself, to check how it works and what 
happens when we change things. This is an theoretical exercise to increase our 
understanding of key concepts in the potential outcome model of causality and how 
they relates to causal effect estimates from randomized experiments.

Below code, modified from REX01-03.


```r
# Simulation randomized experiment. 
# Here we will simulate a simple randomized experiment, in which 
# participants were randomized to one of two diets (diet0 or diet1), and the 
# outcome is weight (kg) after a number of weeks on diet.


## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Simulate data ---------------------------------------------------------------
set.seed(123)  # Any number may be used

# Number of participants
n <- 60   # Make it an even number to have equal sized groups
id <- 1:n # Give each a number

# Population average causal effect (PATE) of diet1 versus diet0 
# means that on average individuals would change pate kg more on diet1 
# compared to diet0.
pate <- -1.5   

# Population average change over time if on diet0
pc0 <- -1  

# Baseline weight, before diet
wpre <- rnorm(n, mean = 85, sd = 9)

# Potential outcome if on diet0 (sd = 3, please change if you like)
diff0 <- rnorm(n, mean = pc0, sd = 3)  # diff from pre to post if on diet0
y0 <- wpre + diff0

# Potential outcome if on diet1 (sd = 3, please change if you like)
diff1 <- rnorm(n, mean = pc0 + pate, sd = 3) # diff from pre to post if on diet1
y1 <- wpre + diff1

# Random assignment to diet0 or diet1, half n to each group
temp <- c(rep(0, n/2), rep(1, n/2))  # Vector with n/2 zeros and n/2 ones
treatment <- sample(temp, replace = FALSE)  # Random assignment using sample()

# Observed weight loss
y_obs <- (1 - treatment) * y0 + treatment * y1

# Add selected variables to data frame d, not needed but simplifies overview
d <- data.frame(id = id, wpre = wpre, treat = treatment, 
                y0 = y0, y1 = y1, wpost = y_obs)

# Post-pre difference
d$wchange <- d$wpost - d$wpre

# Inspect data frame 
str(d)  
summary(d) 
## .............................................................................


## -----------------------------------------------------------------------------

# Selected and slightly modified code from REX01-03 above, new code below: -----


## True causal effects ---------------------------------------------------------
# Single unit causal effects, unobserved (true)
d$delta <- d$y1 - d$y0
hist(d$delta)

# Sample average causal effect, unobserved (true)
sate <- mean(d$delta)  # 
print(sate)  # Show in console

# Population average causal effect (from above), unobserved (true)
print(pate)  # Show in console
## .............................................................................


## Estimated causal effect -----------------------------------------------------

# Prima-facie effect, observed (potentially biased)
pf <- mean(d$wpost[d$treat == 1] - d$wpost[d$treat == 0])

# Prima-facie effect, estimated using regression, lm()
pflm <- lm(wpost ~ treat, data = d)
pf_point_est <- pflm$coefficients[2]
pf_ci <- confint(pflm)
summary(pflm)
confint(pflm)

# Prima-facie adjusted effect, observed (potentially biased)
# Using difference scores, to adjust for baseline differences
pf_preadj <- mean(d$wchange[d$treat == 1] - d$wchange[d$treat == 0])

# Prima-facie adjusted effect, estimated using regression, lm()
# using difference scores, to adjust for baseline differences
pflm_adj <- lm(wchange ~ treat, data = d)
pfadj_point_est <- pflm_adj$coefficients[2]
pfadj_ci <- confint(pflm_adj)
summary(pflm_adj)
confint(pflm_adj)
## -----------------------------------------------------------------------------


## Collect true and estimated effects in vector --------------------------------
ceffects <- c(pate = pate, sate = sate, prima_facie = pf, pf_preadj = pf_preadj)
print(round(ceffects, 2))
## .............................................................................


## Illustrate true (unobserved) and estimated effects in graph -----------------
# Empty plot
plot(d$treat, d$wchange, 
     xlim = c(-0.2, 1.2), ylim = c(-6, 6),
     axes = FALSE,
     xlab = '', ylab = '',
     pch = '')

# Add line at zero weight change
lines(c(-0.2, 1.2), c(0, 0), lty = 1, col = "grey")

# Add lines at PATE and SATE
lines(c(-0.2, 1.2), c(pate, pate), lty = 2, col = "darkgreen")
lines(c(-0.2, 1.2), c(sate, sate), lty = 1, col = "green")

# Add estimated effect with 95 % CI for pf
lines(c(0, 0), c(pf_ci[2, ]), col = "blue")
points(0, pf_point_est, pch = 21, bg = "lightblue")

# Add estimated effect with 95 % CI for pf baseline adjusted
lines(c(1, 1), c(pfadj_ci[2, ]), col = "blue")
points(1, pfadj_point_est, pch = 21, bg = "lightblue")

# Add x-axis
xticks <- c(-0.2, 0, 1, 1.2)
axis(side = 1, pos = -6, at = xticks, 
     labels = c("", "prima-facie", "prima-facie\nbaseline adjusted", ""),
     tck = 0.01, cex.axis = 0.8)
axis(side = 3, pos = 6,
     at = xticks, labels = c("", "", "", ""),
     tck = 0.01)

# Add y-axis
yticks <- seq(-6, 6, by = 1)
axis(side = 2, pos = -0.2,
     at = yticks,
     tck = 0.01, las = 2, cex.axis = 0.8)
axis(side = 4, pos = 1.2,
     at = yticks, labels = rep("", length(yticks)),
     tck = 0.01, las = 2)

# Add axis labels
mtext("Type of estimate", side = 1, line = 1)
mtext("Causal effect estimate [kg]", side = 2, line = 1.4)

# Add legend
points(0.3, -4, pch = 21, bg = "blue", cex = 0.8)
lines(c(0.3, 0.3), c(-3.5, -4.5), col = "blue")
text(0.3, -4, labels = "Point estimate, 95 % CI", cex = 0.7, pos = 4)

lines(c(0.26, 0.34), c(-5, -5), col = "darkgreen", lty = 2)
text(0.35, -5, labels = "PATE", cex = 0.7, pos = 4)

lines(c(0.26, 0.34), c(-5.5, -5.5), col = "green", lty = 1)
text(0.35, -5.5, labels = "SATE", cex = 0.7, pos = 4)
## .............................................................................
```


<br>


### Exercises {-}

- Check what happens to PATE, SATE, and prima-facie and baseline-adjusted 
prima-facie as you increase the samples size (try a few values of `n`)
- Check what happens to SATE, and prima-facie and baseline-adjusted prima-facie 
as you increase the PATE (try a few values of `pate`)
- ADVANCED: Simulate and plot (histogram) SATE from 1000 randomizations of a 
given sample. First draw n = 60 from the population, then do repeated 
randomizations 1000 times, each time recording the SATE. You will find the function 
`replicate()` helpful! (see example above for the test-instruction example)

<br>
