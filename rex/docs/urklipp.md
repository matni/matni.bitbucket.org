---
title: "urklipp"
output: html_document
date: '2022-11-11'
---




<!-- This was previously under REX05: random numbers --> 
### Model simulation using random numbers {-}

This is a causal model of the relationship between three variables, 
intelligence (iq), social skills (ss) and income: 

$iq\rightarrow{income}\leftarrow{ss}$.  

In words: intelligence and social skills
are independent and both influence income. To be more specific, we may assume
that the causal effects leads to a linear relationship between social skills on 
the one hand and iq and ss on the other:  
$income = \beta_0 + \beta_1iq + \beta_2ss +\epsilon$  

Below a simulation of this linear model, using random numbers drawn 
from the standard normal distribution (mean = 0, sd = 1, default values of 
`rnorm()`).

```r
set.seed(123)
# One possible set of beta weights
betaw <- c(0, 0.5, 0.5) 
# Sample size
n = 10^4
# Random numbers representing iq
iq <- rnorm(n)  
# Random numbers representing ss
ss <- rnorm(n) 
# Income influenced by both iq and ss (added to 'baseline' income)
income <- betaw[1] + betaw[2] * iq + betaw[3] * ss + rnorm(n)  
simul <- data.frame(iq, ss, income)  # Put all in a data frame (not necessary)
```

A scatter plot matrix and a correlation matrix suggest that our simulation is 
consistent with the model: iq and ss are independent, and both are positively
correlated with income.

```r
pairs(~simul$iq + simul$ss + simul$income)  # Scatter plot matrix
```

<img src="urklipp_files/figure-html/unnamed-chunk-2-1.png" width="672" />

```r
round(cor(simul), 3)  # Pearson coefficients of correlation (to 3 decimals)
```

```
##           iq    ss income
## iq     1.000 0.006  0.424
## ss     0.006 1.000  0.406
## income 0.424 0.406  1.000
```

As expected, a multiple regression model will recover the beta coefficients, 
plus minus some random error.

```r
lm(simul$income ~ simul$iq + simul$ss)
```

```
## 
## Call:
## lm(formula = simul$income ~ simul$iq + simul$ss)
## 
## Coefficients:
## (Intercept)     simul$iq     simul$ss  
##   -0.007081     0.520061     0.496815
```
  
We may use this simulated data to illustrate *Collider stratification bias*
(e.g., Elwert & Winship, 2014. Endogenous selection bias: the problem of 
conditioning on a collider variable. *Annual Review of Sociology*, 40, 31-53).  

A researcher is interested in the relationship between intelligence and social
skills. His hypothesis is that intelligent individuals tend to spend time on
reading books and other solitary tasks and therefore have less time to develop their
social skills. He therefore predicts a negative correlation between intelligence 
(iq) and social skills (ss), and test this on a sample of people with similar income. 
He considers this to be a strength of his designs as it controls for income by restriction. 
The code generates a plot that shows the relationship between iq and ss in the 
full sample (open symbols, black line) and restricted to high-income people 
(blue symbols, blue line).  

```r
plot(simul$iq, simul$ss, xlab = 'Intelligence (iq)', ylab = 'Social skills (ss)')

# Regression based on all data
full_model <- lm(simul$ss ~ simul$iq)
abline(full_model)

# Regression based on restricted data
points(simul$iq[simul$income > 2], simul$ss[simul$income > 2],
       col = 'blue', pch = 16, cex = 0.5)
restricted_model <- lm(simul$ss[simul$income > 2] ~ simul$iq[simul$income > 2])
abline(restricted_model, col = 'blue')
```

<img src="urklipp_files/figure-html/unnamed-chunk-4-1.png" width="480" />

The restricted data (blue symbols and line) support the researcher's hypothesis. 
But we know he's wrong, because iq and ss are independent (by assumption). This
is an example where controlling or adjusting for a potentially confounding 
variable introduces bias. Note that it is true that there is a negative
correlation between iq and ss in high-income individuals (in our simulated 
world); it's the researcher's *causal* hypothesis that is wrong, not his 
prediction of a negative correlation.

<br><br>

<!-- This was previously under REX05: random numbers --> 
## Script: Simulating data consistent with a DAG {-}

... under construction ...  

A new simulation exercise, this time of data consistent with this 
Directed Acyclical Graph (DAG):


```r
library(dagitty) 

vvg_dag <- dagitty( "dag {
   VVG -> AGG
   VVG -> PA -> AGG
   PA -> BMI
   VVG <- Support -> SProb -> AGG
   SProb <- Genetic -> AGG
}")

coordinates(vvg_dag) <- list(
  x = c(VVG = 1, Support = 1, BMI = 2, PA = 3, SProb = 3, Genetic = 4, AGG = 4),
  y = c(VVG = 5, Support = 1, BMI = 3, PA = 4, SProb = 2, Genetic = 1, AGG = 5))

plot(vvg_dag)
```

<img src="urklipp_files/figure-html/unnamed-chunk-5-1.png" width="672" />
  
  
Exposure: Violent video games (VVG)   

Outcome: Adult aggression (AGG)  

Covariates: Physical activity (PA), Parental support (Support), 
School problems (SProb), BMI (BMI), Genetic factor (Genetic).
