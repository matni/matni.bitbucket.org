# Notes REX03 {-}




Today we'll practice 'subsetting', i.e., extracting elements from data structures.
R will make a lot more sense, once you have understood the basics of
subsetting. After that we'll continue working on the script: it's finally 
time to do some serious statistical analyses of the result of our imaginary 
experiment.

<br> 


## Subsetting {-}
Please spend time on the examples below; modify and extend them to get a grip of
the various ways to extract parts of R objects. I've heard of people who gave up 
on R because they couldn't find out how to calculate means for separate groups. 
It's not totally intuitive, but once you know how, it's easy. And more
important, subsetting in R is extremely powerful, allowing you to analyze any 
subsets of your data. 

<br>  

### Vectors {-}


```r
z <- seq(1, 100, 7)
z
```

```
##  [1]  1  8 15 22 29 36 43 50 57 64 71 78 85 92 99
```

Use square brackets to subset parts of a vector (elements of vectors are 
indexed 1, 2, ..., n).

```r
z[2]  # Second element
```

```
## [1] 8
```

```r
z[-2]  # All but the second element
```

```
##  [1]  1 15 22 29 36 43 50 57 64 71 78 85 92 99
```

```r
z[c(1,7,9)]  # Some selected elements
```

```
## [1]  1 43 57
```

```r
z[z > 50]  # All elements greater than 50
```

```
## [1] 57 64 71 78 85 92 99
```

```r
z[length(z)]  # Last element
```

```
## [1] 99
```

```r
z[z < 1]  # Empty vector returned
```

```
## numeric(0)
```

You have to use square brackets, ordinary brackets are for functions.

```r
z(3)
```

```
## Error in z(3): could not find function "z"
```

Here is an example of the power of R: Subsetting one vector based on 
values in another variable.

```r
set.seed(99)
# IQs of a lot of people 
iq <- rnorm(10^5, mean = 100, sd = 15) 
# Their gender
gender <- sample(c('male', 'female', 'other'), length(iq),
                 replace = TRUE, prob = c(.49, .49, .02))
# Gender of the smartest!
gender_smartest <- gender[iq > 145]  
table(gender_smartest)
```

```
## gender_smartest
## female   male  other 
##     64     62      3
```

<br>  

### Data frames {-}
A data frame is a collection of vectors of the same length organized in rows
(observations) and columns (variables). Use the dollar sign to extract a
column (i.e., vector) from a data frame. You may then use square brackets to find 
subsets of data within the vector. Below an example 
using one of R's preloaded data sets (use data() to list all of preloaded data sets). 

```r
# Assign R's preloaded data set 'sleep' to data frame d. It contains data 
# on the effect of two sleep drugs on the amount of sleep re control nights
# in two groups of 10 patients each. 
d <- sleep   
str(d)  # Check structure of d
```

```
## 'data.frame':	20 obs. of  3 variables:
##  $ extra: num  0.7 -1.6 -0.2 -1.2 -0.1 3.4 3.7 0.8 0 2 ...
##  $ group: Factor w/ 2 levels "1","2": 1 1 1 1 1 1 1 1 1 1 ...
##  $ ID   : Factor w/ 10 levels "1","2","3","4",..: 1 2 3 4 5 6 7 8 9 10 ...
```

```r
# Subset outcome data: extra sleep on drug
d$extra  
```

```
##  [1]  0.7 -1.6 -0.2 -1.2 -0.1  3.4  3.7  0.8  0.0  2.0  1.9  0.8  1.1  0.1 -0.1
## [16]  4.4  5.5  1.6  4.6  3.4
```

```r
# Subset outcome for group 1
d$extra[d$group == 1]  
```

```
##  [1]  0.7 -1.6 -0.2 -1.2 -0.1  3.4  3.7  0.8  0.0  2.0
```

```r
# Calculate difference between group medians 
median(d$group == 1) - median(d$group == 2)
```

```
## [1] 0
```

```r
# Group membership of those with positive effect
d$group[d$extra > 0]
```

```
##  [1] 1 1 1 1 1 2 2 2 2 2 2 2 2 2
## Levels: 1 2
```

You may also use square brackets to subset a data frame, but then
you need two indices: one for rows, and the other for columns, separated by a 
comma. If you leave one empty, R assumes that you mean all rows or all columns.

```r
d[2, 1]  # Value of element in the second row of column 1
```

```
## [1] -1.6
```

```r
# Subset all the data for group 1; the output is a new data frame 
g1 <- d[d$group == 1,]  
# Here's the same without dollar sign (as verified below)
g1b <- d[d[,2] == 1,]
identical(g1, g1b)
```

```
## [1] TRUE
```

<br> 

## Script: Simulation randomized experiment {-}

Last time we had a look at the data from our simulated experiment to make sure 
everything looked OK and to get a general feel for the results. Today we will 
analyze the data to obtain results and figures that may go into a paper describing 
our experiment.

Here is code that imports the simulated data from the text file `weight.txt` 
created in REX01. The code below starts as most of my analysis scripts: 

a. Information on what the script does. May include a code book.
b. Remove everything to start fresh (1st code block)
c. If needed, load things, e.g., libraries (2nd code block). My advice: Use 
as few libraries as possible (ideal = 0 libraries), to make life easier for other 
users of your code.
d. Import data from a text-file (3rd code block)

**Note**: The analytic strategy below is to work with gain-scores, that is, 
weight loss (wpost-wpre). This is the most straight forward analysis of a randomized 
pre-post experiment. But several other approaches are also well justified. 
In general: Do all reasonable analysis to make sure that your results are robust 
to choice of analysis method (we will do one alternative below as a robustness 
check).


```r
# Simulation randomized experiment. 
# Import simulated data stored in text-file named "weight.txt". Data is 
# a simulation of a simple randomized experiment, in which 
# participants were randomized to one of two diets (diet0 or diet1), and the 
# outcome weight (kg) after a number of weeks on diet.
#
# Code book:
# id -- ID number, unique to each participant, id = 1, 2, ..., 60
# wpre -- Baseline weight, measured before experiment [kg]
# treat -- Experimental group: 1 = diet1 (treatment), 0 = diet0 (control)
# wpost -- Weight a number of weeks after the experiment [kg]


## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Load stuff ------------------------------------------------------------------
library(beeswarm)  # A small library for nice dot-plots
## .............................................................................


## Import data -----------------------------------------------------------------
d <- read.table("weight.txt", header = TRUE, sep = ",")
## .............................................................................


## Recode variables and add to data frame --------------------------------------
# Factor for treatment
d$treat_f <- factor(d$treat, labels = c("diet0", "diet1"))
table(d$treat, d$treat_f)  # Check that it worked, groups should be even

# Create weight change variable
d$wchange <- d$wpost - d$wpre

# Inspect data frame with new variables
str(d)  
summary(d)  
## .............................................................................


## Descriptive stats per experimental group ------------------------------------
# Maybe intended for a table. Note: Smarter ways to do this in coming REX's

m0_pre <-   mean(d$wpre[d$treat == 0])
sd0_pre <-  sd(d$wpre[d$treat == 0])
m0_post <-  mean(d$wpost[d$treat == 0])
sd0_post <- sd(d$wpost[d$treat == 0])
m0_diff <-  mean(d$wchange[d$treat == 0])
sd0_diff <- sd(d$wchange[d$treat == 0])

m1_pre <-   mean(d$wpre[d$treat == 1])
sd1_pre <-  sd(d$wpre[d$treat == 1])
m1_post <-  mean(d$wpost[d$treat == 1])
sd1_post <- sd(d$wpost[d$treat == 1])
m1_diff <-  mean(d$wchange[d$treat == 1])
sd1_diff <- sd(d$wchange[d$treat == 1])

my_stat <- c(m0_pre = m0_pre,   sd0_pre = sd0_pre, 
             m1_pre = m1_pre,   sd1_pre = sd1_pre, 
             m0_post = m0_post, sd0_post = sd0_post,
             m1_post = m1_post, sd1_post = sd1_post,
             m0_diff = m0_diff, sd0_diff = sd0_diff,
             m1_diff = m1_diff, sd1_diff = sd1_diff)

round(my_stat, 1) # Print to console

# Here a smarter way (even smarter way in next REX!)
m <- aggregate(list(pre = d$wpre, post = d$wpost, diff = d$wchange), # variables
               list(group = d$treat_f),                           # split by
               mean)                                            # stat function
m

s <- aggregate(list(pre = d$wpre, post = d$wpost, diff = d$wchange), # variables
               list(group = d$treat_f),                           # split by
               sd)                                            # stat function
s
## .............................................................................


## Figure change-scores --------------------------------------------------------
# (See also last code chunk for an advanced example of plotting in R)

# Just to show how the raw plot would look
plot(d$treat, d$wchange)  

# A bit nicer using beeswarm form library(beeswarm) and a few other tricks
beeswarm(d$wchange ~ d$treat,
         xlab = "Group",
         ylab = "Weight loss [kg]")  
abline(h = 0, lty = 2)  # Add line at zero change
mdn0 <- median(d$wchange[d$treat == 0]) # Median group 0
mdn1 <- median(d$wchange[d$treat == 1]) # Median group 1
# Add median values; took me some time to find out beeswarm's x-axis values
points(1, mdn0, pch = 22, bg = "blue")  
points(2, mdn1, pch = 22, bg = "blue")
## .............................................................................


## Stats on change scores ------------------------------------------------------
# Using lm(): Intercept is mean of diet0, coefficient of treat is our 
# estimated treatment effect = the difference between diet1 and diet0
# Note: We will use rstanarm::stan_glm() later on, would give very similar ests.
mdiff <- lm(wchange ~ treat, data = d)
# please compare this to a regular t.test: Same thing
t.test(wchange ~ treat, data = d, var.equal = TRUE)

# Extract analysis results from analysis object mdiff
point_est <- mdiff$coefficients  # Store point estimates
point_est  # Check in console
ci <- confint(mdiff)      # Store CI
ci # Check in console

# Main result, our estimated treatment effect with CI combined in vector
treat_est <- c(point_est[2], ci[2, 1], ci[2, 2])
names(treat_est) <- c("treat_point_est", "ci95lo", "ci95hi")
round(treat_est, 1)

# Alternative analysis as robustness check, should give similar estimates 
mcov <- lm(wpost ~ 1 + treat + wpre, data = d)  # baseline as covariate
mcov$coefficients
confint(mcov)
## .............................................................................


## Here an advanced example of plotting in base R ------------------------------

# Start with an empty plot
plot(d$treat, d$wchange, 
     xlim = c(-0.2, 1.2), ylim = c(-10, 10),
     axes = FALSE,
     xlab = '', ylab = '',
     pch = '')

# Add line at zero weight change
lines(c(-0.2, 1.2), c(0, 0), lty = 3)

# Add points
jitter <- rnorm(length(d$wchange), mean = 0, sd = 0.01)
points(d$treat+ jitter, d$wchange, pch = 21, bg = "grey")

# Add x-axis
xticks <- c(-0.2, 0, 1, 1.2)
axis(side = 1, pos = -10,
     at = xticks, labels = c("", "Diet 0", "Diet 1", ""),
     tck = 0.01, cex.axis = 0.8)
axis(side = 3, pos = 10,
     at = xticks, labels = c("", "", "", ""),
     tck = 0.01)

# Add y-axis
yticks <- seq(-10, 10, by = 2)
axis(side = 2, pos = -0.2,
     at = yticks,
     tck = 0.01, las = 2, cex.axis = 0.8)
axis(side = 4, pos = 1.2,
     at = yticks, labels = rep("", length(yticks)),
     tck = 0.01, las = 2)

# Add axis labels
mtext("Experimental group", side = 1, line = 1)
mtext("Weight change [kg]", side = 2, line = 1.4)

# Add mean and 95% CI
offset <- 0.1 
ci95_0 <- confint(lm(d$wchange[d$treat == 0] ~ 1))  # Messy code to get 95%CI
lines(c(0 + offset, 0 + offset), c(ci95_0[1], ci95_0[2]), col = "blue")
points(0.1, m0_diff, pch = 22, bg = "lightblue")

ci95_1 <- confint(lm(d$wchange[d$treat == 1] ~ 1))  # Messy code to get 95%CI
lines(c(1 - offset, 1 - offset), c(ci95_1[1], ci95_1[2]), col = "blue")
points(0.9, m1_diff, pch = 22, bg = "lightblue")

# Add legend
points(-0.1, -7, pch = 21, bg = "grey", cex = 0.8)
text(-0.1, -7, labels = "Individual data", cex = 0.7, pos = 4)

points(-0.1, -8, pch = 22, bg = "lightblue", cex = 0.8)
text(-0.1, -8, labels = "Group mean", cex = 0.7, pos = 4)

lines(c(-0.1, -0.1), c(-8.6, -9.4), col = "blue")
text(-0.1, -9, labels = "95 % Confidence interval", cex = 0.7, pos = 4)
## .............................................................................
```

<br><br>

Here the same script again, ut including the simulation code, for those who would
like to test other scenarios (more on this in next REX).


```r
# Simulation randomized experiment. 
# Simulate data from a simple randomized experiment, in which 
# participants were randomized to one of two diets (diet0 or diet1), and the 
# outcome weight (kg) after a number of weeks on diet.
#
# Code book:
# id -- ID number, unique to each participant, id = 1, 2, ..., 60
# wpre -- Baseline weight, measured before experiment [kg]
# treat -- Experimental group: 1 = diet1 (treatment), 0 = diet0 (control)
# wpost -- Weight a number of weeks after the experiment [kg]


## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Load stuff ------------------------------------------------------------------
library(beeswarm)  # A small library for nice dot-plots
## .............................................................................


## Simulate data ---------------------------------------------------------------
set.seed(123)  # Any number may be used

# Number of participants
n <- 60   # Make it an even number to have equal sized groups
id <- 1:n # Give each a number

# Baseline weight, before diet
wpre <- rnorm(n, mean = 85, sd = 9)

# Potential outcome if on diet0
diff0 <- rnorm(n, mean = -1, sd = 3)  # diff from pre to post if on diet0
y0 <- wpre + diff0

# Potential outcome if on diet1
diff1 <- rnorm(n, mean = -2.5, sd = 3) # diff from pre to post if on diet1
y1 <- wpre + diff1

# Random assignment to diet0 or diet1, half n to each group
temp <- c(rep(0, n/2), rep(1, n/2))  # Vector with n/2 zeros and n/2 ones
treatment <- sample(temp, replace = FALSE)  # Random assignment using sample()

# Observed weight loss
y_obs <- (1 - treatment) * y0 + treatment * y1

# Add observed variables to data frame d, rounded to 1 decimal
d <- data.frame(id = id, wpre = wpre, treat = treatment, wpost = y_obs)
d <- round(d, 1)

# Clear the global environment, keep d. Not necessary, just to keep it clean
rm(list = ls()[ls() != "d"])  
## .............................................................................


## Recode variables and add to data frame --------------------------------------
# Factor for treatment
d$treat_f <- factor(d$treat, labels = c("diet0", "diet1"))
table(d$treat, d$treat_f)  # Check that it worked, groups should be even

# Create weight change variable
d$wchange <- d$wpost - d$wpre

# Inspect data frame with new variables
str(d)  
summary(d)  
## .............................................................................


## Descriptive stats per experimental group ------------------------------------
# Maybe intended for a table. Note: Smarter ways to do this in coming REX's

m0_pre <-   mean(d$wpre[d$treat == 0])
sd0_pre <-  sd(d$wpre[d$treat == 0])
m0_post <-  mean(d$wpost[d$treat == 0])
sd0_post <- sd(d$wpost[d$treat == 0])
m0_diff <-  mean(d$wchange[d$treat == 0])
sd0_diff <- sd(d$wchange[d$treat == 0])

m1_pre <-   mean(d$wpre[d$treat == 1])
sd1_pre <-  sd(d$wpre[d$treat == 1])
m1_post <-  mean(d$wpost[d$treat == 1])
sd1_post <- sd(d$wpost[d$treat == 1])
m1_diff <-  mean(d$wchange[d$treat == 1])
sd1_diff <- sd(d$wchange[d$treat == 1])

my_stat <- c(m0_pre = m0_pre,   sd0_pre = sd0_pre, 
             m1_pre = m1_pre,   sd1_pre = sd1_pre, 
             m0_post = m0_post, sd0_post = sd0_post,
             m1_post = m1_post, sd1_post = sd1_post,
             m0_diff = m0_diff, sd0_diff = sd0_diff,
             m1_diff = m1_diff, sd1_diff = sd1_diff)

round(my_stat, 1) # Print to console

# Here a smarter way (even smarter way in next REX!)
m <- aggregate(list(pre = d$wpre, post = d$wpost, diff = d$wchange), # variables
               list(group = d$treat_f),                           # split by
               mean)                                            # stat function
m

s <- aggregate(list(pre = d$wpre, post = d$wpost, diff = d$wchange), # variables
               list(group = d$treat_f),                           # split by
               sd)                                            # stat function
s
## .............................................................................


## Figure change-scores --------------------------------------------------------
# (See also last code chunk for an advanced example of plotting in R)

# Just to show how the raw plot would look
plot(d$treat, d$wchange)  

# A bit nicer using beeswarm form library(beeswarm) and a few other tricks
beeswarm(d$wchange ~ d$treat,
         xlab = "Group",
         ylab = "Weight loss [kg]")  
abline(h = 0, lty = 2)  # Add line at zero change
mdn0 <- median(d$wchange[d$treat == 0]) # Median group 0
mdn1 <- median(d$wchange[d$treat == 1]) # Median group 1
# Add median values; took me some time to find out beeswarm's x-axis values
points(1, mdn0, pch = 22, bg = "blue")  
points(2, mdn1, pch = 22, bg = "blue")
## .............................................................................


## Stats on change scores ------------------------------------------------------
# Using lm(): Intercept is mean of diet0, coefficient of treat is our 
# estimated treatment effect = the difference between diet1 and diet0
# Note: We will use rstanarm::stan_glm() later on, would give very similar ests.
mdiff <- lm(wchange ~ treat, data = d)
# please compare this to a regular t.test: Same thing
t.test(wchange ~ treat, data = d, var.equal = TRUE)

# Extract analysis results from analysis object mdiff
point_est <- mdiff$coefficients  # Store point estimates
point_est  # Check in console
ci <- confint(mdiff)      # Store CI
ci # Check in console

# Main result, our estimated treatment effect with CI combined in vector
treat_est <- c(point_est[2], ci[2, 1], ci[2, 2])
names(treat_est) <- c("treat_point_est", "ci95lo", "ci95hi")
round(treat_est, 1)

# Alternative analysis as robustness check, should give similar estimates 
mcov <- lm(wpost ~ 1 + treat + wpre, data = d)  # baseline as covariate
mcov$coefficients
confint(mcov)
## .............................................................................


## Here an advanced example of plotting in base R ------------------------------

# Start with an empty plot
plot(d$treat, d$wchange, 
     xlim = c(-0.2, 1.2), ylim = c(-10, 10),
     axes = FALSE,
     xlab = '', ylab = '',
     pch = '')

# Add line at zero weight change
lines(c(-0.2, 1.2), c(0, 0), lty = 3)

# Add points
jitter <- rnorm(length(d$wchange), mean = 0, sd = 0.01)
points(d$treat+ jitter, d$wchange, pch = 21, bg = "grey")

# Add x-axis
xticks <- c(-0.2, 0, 1, 1.2)
axis(side = 1, pos = -10,
     at = xticks, labels = c("", "Diet 0", "Diet 1", ""),
     tck = 0.01, cex.axis = 0.8)
axis(side = 3, pos = 10,
     at = xticks, labels = c("", "", "", ""),
     tck = 0.01)

# Add y-axis
yticks <- seq(-10, 10, by = 2)
axis(side = 2, pos = -0.2,
     at = yticks,
     tck = 0.01, las = 2, cex.axis = 0.8)
axis(side = 4, pos = 1.2,
     at = yticks, labels = rep("", length(yticks)),
     tck = 0.01, las = 2)

# Add axis labels
mtext("Experimental group", side = 1, line = 1)
mtext("Weight change [kg]", side = 2, line = 1.4)

# Add mean and 95% CI
offset <- 0.1 
ci95_0 <- confint(lm(d$wchange[d$treat == 0] ~ 1))  # Messy code to get 95%CI
lines(c(0 + offset, 0 + offset), c(ci95_0[1], ci95_0[2]), col = "blue")
points(0.1, m0_diff, pch = 22, bg = "lightblue")

ci95_1 <- confint(lm(d$wchange[d$treat == 1] ~ 1))  # Messy code to get 95%CI
lines(c(1 - offset, 1 - offset), c(ci95_1[1], ci95_1[2]), col = "blue")
points(0.9, m1_diff, pch = 22, bg = "lightblue")

# Add legend
points(-0.1, -7, pch = 21, bg = "grey", cex = 0.8)
text(-0.1, -7, labels = "Individual data", cex = 0.7, pos = 4)

points(-0.1, -8, pch = 22, bg = "lightblue", cex = 0.8)
text(-0.1, -8, labels = "Group mean", cex = 0.7, pos = 4)

lines(c(-0.1, -0.1), c(-8.6, -9.4), col = "blue")
text(-0.1, -9, labels = "95 % Confidence interval", cex = 0.7, pos = 4)
## .............................................................................
```


<br>

### Exercises  {-}

Next time we will dissect the logic behind the simulation above. You may go ahead 
and answer these questions:

1. What was the Population average causal effect (PATE) in the simulated scenario?
2. What was the Sample average causal effect (SATE) in the simulated scenario 
(given n = 60, and `set.seed(123)`)? 
3. Make a boxplot of the single-unit causal effects in the simulation 
(given `n = 60`, and `set.seed(123)`).
4. Check what happens to the boxplot as you change:
    + seed (number in `set.seed()`)
    + sample size (variable `n`)

Read more about PATE and SATE in @gelman2020regression (chapter 18).

