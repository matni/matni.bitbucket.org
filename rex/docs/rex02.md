# Notes REX02 {-}




This seminar first reviews the fundamentals of how R deals with data.
After that, we will continue our work on our simulated randomized experiment.  

The treatment of data structures (below), subsetting (REX03) and 
functions (REX04) is inspired by Hadley Wickham's book 
[Advanced R (2nd ed.)](http://adv-r.had.co.nz/Introduction.html), where you will
find a much deeper treatment of how R works. Note that we will adhere to Wickham's
guide for [good coding style](http://adv-r.had.co.nz/Style.html) when writing our
scripts.

<br>  

## How R deals with data {-}
Please spend some time on this topic. It may seem abstract, even boring, but 
it is worthwhile studying. You don't have to memorize all this, just play around 
with the examples below to get a feel of data storage in R.  

One lesson to learn is that R sometimes 'help' you to make sense. This can indeed
be helpful, if you know what you are doing, but may cause strange errors if you
don't; errors that may be hard to fix if you lack basic understanding of
how R deals with data.  

In R, data are contained in data structures. We will mainly work with two types
of data structures: Atomic vectors and data frames, so let's concentrate on 
them (and save lists, matrices and arrays for later).

## Vectors {-}
Atomic vectors, or vectors for short, are 1-dimensional collections of elements
of the same type. The most important vector types are

1. Integer vectors
2. Numeric (or double) vectors
3. Character vectors
4. Logical vectors

Note that R treats a single data element as a vector with one element (i.e., R
has no scalars).  
When you work with numbers in R, they will most often be be defined as
numeric vectors. If you input a non-fractional number, R will treat it
as a numeric vector unless you tell it otherwise.

```r
a <- 4
str(a)  
```

```
##  num 4
```

```r
b <- 4L  # you need to add an L to make it an integer
str(b)
```

```
##  int 4
```

<br> 

Character vectors are defined using single (or double) quotes:

```r
x <- 'four'
str(x)
```

```
##  chr "four"
```

```r
question <- "Did you hear a tone?"
str(question)
```

```
##  chr "Did you hear a tone?"
```

Logicals have two values, TRUE or FALSE. Typically, you get a logical
data element by making a statement:

```r
x <- 4 > 5  # I say: "4 is greater than 5". R answers: "FALSE"
x
```

```
## [1] FALSE
```

```r
str(x)
```

```
##  logi FALSE
```

<br>  

Sometimes you can and sometimes you can't do operations on elements of different
types. Always be careful when you can, to make sure you get what you expect.  

```r
# Adding numeric and integer works fine, output is numeric
x <- 4 + 4L  
x
```

```
## [1] 8
```

```r
str(x)
```

```
##  num 8
```

```r
# Adding numeric and character doesn't work
y <- 4 + 'four'
```

```
## Error in 4 + "four": non-numeric argument to binary operator
```

```r
# Adding a numeric and logical element works (surprisingly); useful for recording of 
# variables, more on this later on ...
z <- 4 + TRUE
z
```

```
## [1] 5
```

```r
str(z)  # output is numeric
```

```
##  num 5
```

<br>

You may combine several data elements (i.e., one-element vectors) in a vector 
using the concatenation function `c()`.

```r
x <- c(1, 2, 3, 5, 8, 13, 21)
x
```

```
## [1]  1  2  3  5  8 13 21
```

```r
y <- c('1', '2', '3')
y
```

```
## [1] "1" "2" "3"
```

```r
z <- c(TRUE, 2 != 5, FALSE) 
z
```

```
## [1]  TRUE  TRUE FALSE
```

```r
w <- c(1, 'two', 3)  # R simply assumes that you want a character vector
w
```

```
## [1] "1"   "two" "3"
```

<br>


### Factors {-}
Factors stores categorical data, and can contain only predefined values. For example, 
you may have a numeric vector `conditions`  with three values, 1, 2, 3, 
representing treatment group, placebo group, and no-placebo-control group.


```r
conditions <- rep(c(1, 2, 3), 3) 
str(conditions)
```

```
##  num [1:9] 1 2 3 1 2 3 1 2 3
```
You may use this vector to define a new vector that is a factor:

```r
conds_factor <- factor(conditions, levels = c(1, 2, 3), 
                       labels = c('treatment', 'placebo', 'no-placebo'))
str(conds_factor)
```

```
##  Factor w/ 3 levels "treatment","placebo",..: 1 2 3 1 2 3 1 2 3
```

```r
conds_factor
```

```
## [1] treatment  placebo    no-placebo treatment  placebo    no-placebo treatment 
## [8] placebo    no-placebo
## Levels: treatment placebo no-placebo
```
A factor is in fact an integer vector with specific attributes that (i) defines
the set of allowed values (levels) and (ii) changes its behavior compared to an
ordinary integer vector. For example, you cannot do arithmetic on a factor. If
you try, missing data, `NA`, is generated. 

```r
conds_factor - 1  # No error message, but a warning that NA's has been generated
```

```
## Warning in Ops.factor(conds_factor, 1): '-' not meaningful for factors
```

```
## [1] NA NA NA NA NA NA NA NA NA
```

```r
conditions - 1  # Arithmetic on the numeric vector of course works fine
```

```
## [1] 0 1 2 0 1 2 0 1 2
```
It may therefore be a good idea to keep the original numeric (or integer) vector
and define the factor as a new variable (as done in the example above)

<br>  

### Names of vector elements {-}
You may name elements of a vector to help you remember what they stand for.
Names are attributes (meta-data) attached to vectors:

```r
set.seed(123)
iq <- rnorm(500, 100, 15)
iq_stats <- c(mean = mean(iq), sd = sd(iq), maximum = max(iq))
iq_stats
```

```
##      mean        sd   maximum 
## 100.51886  14.59154 148.61560
```

```r
# You can also do it after a vector has been created
iq_stats2 <- c(median(iq), mad(iq))
names(iq_stats2) <- c('median', 'mad')
iq_stats2 
```

```
##   median      mad 
## 100.3108  14.0368
```


<br>  


## Data frames {-}

A data frame is a 2-dimensional data structure, that combines vectors of equal
length in rows and columns. Each vector (column) must contain the same type of
elements, but different columns may contain different types of elements. The code
below creates three vectors and then combines them into a data frame using the
`data.frame()` function.

```r
id <- seq(1, 200)
gender <- sample(c('male', 'female'), 200, replace = TRUE)  # character vector
performance <- rnorm(200, 100, 20)
my_data <- data.frame(id, gender, performance)
str(my_data)
```

```
## 'data.frame':	200 obs. of  3 variables:
##  $ id         : int  1 2 3 4 5 6 7 8 9 10 ...
##  $ gender     : chr  "male" "female" "female" "male" ...
##  $ performance: num  121.5 99.5 99.3 69.7 115.8 ...
```

`my_data` is a data frame with 200 observations in 3 columns.  

Here I add a factor to my data frame `my_data`, the factor has three levels, 
although only two have cases.


```r
my_data$group_f <- factor(my_data$gender, levels = c('male', 'female', 'other'))
table(my_data$group_f)
```

```
## 
##   male female  other 
##    101     99      0
```

<br><br>

## Script: Simulation randomized experiment {-} 

Let's start analyzing the data from our simulated experiment. Here's the code 
from last REX, followed by new code that we will work through today. 


```r
# Simulation randomized experiment. 
# Here we will simulate a simple randomized experiment, in which 
# participants were randomized to one of two diets (diet0 or diet1), and the 
# outcome is weight (kg) after a number of weeks on diet.


## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Simulate data ---------------------------------------------------------------
set.seed(123)  # Any number may be used

# Number of participants
n <- 60   # Make it an even number to have equal sized groups
id <- 1:n # Give each a number

# Baseline weight, before diet
wpre <- rnorm(n, mean = 85, sd = 9)

# Potential outcome if on diet0
diff0 <- rnorm(n, mean = -1, sd = 3)  # diff from pre to post if on diet0
y0 <- wpre + diff0

# Potential outcome if on diet1
diff1 <- rnorm(n, mean = -2.5, sd = 3) # diff from pre to post if on diet1
y1 <- wpre + diff1

# Random assignment to diet0 or diet1, half n to each group
temp <- c(rep(0, n/2), rep(1, n/2))  # Vector with n/2 zeros and n/2 ones
treatment <- sample(temp, replace = FALSE)  # Random assignment using sample()

# Observed weight loss
y_obs <- (1 - treatment) * y0 + treatment * y1

# Add observed variables to data frame d, rounded to 1 decimal
d <- data.frame(id = id, wpre = wpre, treat = treatment, wpost = y_obs)
d <- round(d, 1)

# Clear the global environment, keep d. Not necessary, just to keep it clean
rm(list = ls()[ls() != "d"])  
## .............................................................................

# From REX01 above, new code below: --------------------------------------------

## Data screening --------------------------------------------------------------
str(d)  
summary(d)  
table(d$treat) 
## .............................................................................


## Plots to inspect the data ---------------------------------------------------
# Boxplots
boxplot(d$wpre ~ d$treat,  ylim = c(60, 120))  # Pre-weight
boxplot(d$wpost ~ d$treat, ylim = c(60, 120))  # Post-weight

# Scatter plot pre - post 
plot(d$wpre, d$wpost, xlim = c(60, 120), ylim = c(60, 120))
lines(c(60, 120), c(60, 120), lty = 3)  # dotted "no change" line 
## .............................................................................


## Recode variables and save to data frame -------------------------------------
# Factor for treatment
d$treat_f <- factor(d$treat, labels = c("diet0", "diet1"))
table(d$treat, d$treat_f)  # Check that it worked

# Create weight change variable
d$wchange <- d$wpost - d$wpre
boxplot(d$wchange ~ d$treat)  # Compare groups, adjusted for pre weight

# Lost weight between pre and post
d$wloss <- 1 * (d$wpost < d$wpre)  # 1 if lost weight, 0 if not
table(d$treat_f, d$wloss)  # Check weight loss between treatments

# Inspect data frame with new variables
str(d)  
summary(d)  
## .............................................................................
```


<br>

### Exercises {-}

Next time we will do some serous analysis of the data. Please start already now, 
for instance:

1. Calculate descriptive statistics, e.g., mean, median, sd, and mad, for 
weight-change, separately for the two groups.
2. Calculate a 95 % confidence interval around the between-group difference in 
mean weight-change.

<br> 


