---
title: "REX"
author: "Mats E. Nilsson"
date: "2024-11-20"
site: bookdown::bookdown_site
documentclass: book
bibliography: ["references.bib"]
link-citations: true
github-repo: 
url: 
description: ""
editor_options: 
  markdown: 
    wrap: 72
---

# Preface {.unnumbered}

learn R by EXample (REX) is a seminar series that introduces the
statistical software R and its help-program R studio. The best way to
learn R is to use R, so the pedagogic philosophy of REX is **learning by
doing**.

The seminars are run as satellites to the master-program and
doctoral-program courses Research Methods 1 ("Method1", PSMT58, PS302F)
and Statistics 1 ("Stat1", PSMT59, PS303F) at the Department of
Psychology, Stockholm university. The primary goal of REX is to prepare
the students of these courses for the assignments that requires coding
in R.

### For whom? {.unnumbered}

The seminars are primarily designed for Method1 and Stat1 students, but
the content will suit anyone interested in learning R from the
beginning. So anyone at the Department of Psychology, including
registered students at all levels and staff are welcome to participate.
It is *highly recommended* that you bring a laptop computer with R and
R-studio installed to the seminars, because, as you already know, the
best way to learn R is to use R.

### What's in it? {.unnumbered}

At the REX seminars we work on scripts designed to solve given tasks:
simulating data sets and conducting a full data analysis of a given data
set. The scripts will evolve gradually over the seminars and the current
version of a script may be copied from the seminar webpage.

The seminar-notes summarize topics discussed at each seminar. If you
find the content elementary, you probably don't need to attend the
seminar. If the notes make no sense at all, please visit the seminar
where everything will be explained in detail.

### When and where? {.unnumbered}

Seminars REX01, REX02, ..., REX10 run as satellite to the **Method1**
and **Stat1** courses, see athena.su.se for schedule.

### Some useful symbols {.unnumbered}

You will need to find some unusual symbols on your keyboard. Learn where
to find them, you will use them a lot!

-   `[`, `]`, for subsetting
-   `$`, for subsetting
-   `&`, logical AND
-   `|`, logical OR
-   `^`, "to the power of", same as `**`
-   `~`, "as a function of", for example in `lm(y ~ x)`
-   `{`, `}`, for functions and control statements
-   `#`, precede comments in script files

### Content {.unnumbered}

| Seminar | R topics                            | Data analysis                          |
|----------------|----------------------------|----------------------------|
| REX01   | Getting started with R and R-studio | Import data                            |
| REX02   | Data types, data structures         | Screening, descriptive statistics      |
| REX03   | Subsetting, Recoding                | Visualization                          |
| REX04   | Functions                           | Point and interval estimation          |
| REX05   | Random numbers                      | Simulations                            |
| REX06   | Functions as arguments              | Linear model using stan_glm() or glm() |
| REX07   | Control statements                  | Categorical variables in linear model  |
| REX08   | Practice all of the above           | Linear model with several predictors   |
| REX09   | Practice all of the above           | Modelling of interaction               |
| REX10   | Practice all of the above           | Logistic models of binary outcome      |
