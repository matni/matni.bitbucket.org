# Notes REX09 {-}




## More practice {-}

Today we will continue practice our R-skills with the data set `kidiq.txt` 
(from @gelman2020regression), that we used back in REX06.

The data is from a survey of adult American women and their 
children. @gelman2020regression describe the data set and use it at several places, 
e.g., pp. 130-136, 156-158, 161, 185-187, and 197. 

[Follow this link to find data](https://bitbucket.org/matni/matni.bitbucket.org/raw/3dee78fc9ace615a849b5ce8cb5abd381ebb1da6/rex/kidiq.txt){target="blank"} 
Save (Ctrl+S on a PC) to download as text file

<br>

**Code book**  

+ **kid_score** \ Test score children, on IQ-like measure (in the sample with 
mean and standard deviation of 87 and 20 points, respectively).
+ **mom_hs** \ Indicator variable: mother did complete high-school (1) or not (0)
+ **mom_iq** \ Test score mothers, transformed to IQ score: mean = 100, sd = 15. do   
+ **mom_work** \ Working status of mother, coded 1-4:
    - 1: mother did not work in first three years of child's life,
    - 2: mother worked in second or third year of child's life, 
    - 3: mother worked part-time in first year of child's life,
    - 4: mother worked full-time in first year of child's life. 
+ **mom_age** Age of mothers, years

The data file is comma-delimited and include variable names.


```r
d <- read.table("kidiq.txt", sep = ",", header = TRUE)
str(d)
```

```
## 'data.frame':	434 obs. of  5 variables:
##  $ kid_score: int  65 98 85 83 115 98 69 106 102 95 ...
##  $ mom_hs   : int  1 1 1 1 1 0 1 1 1 1 ...
##  $ mom_iq   : num  121.1 89.4 115.4 99.4 92.7 ...
##  $ mom_work : int  4 4 4 3 4 1 4 3 1 1 ...
##  $ mom_age  : int  27 25 27 25 27 18 20 23 24 19 ...
```
<br>

<br>


```r
library(dagitty) 

kidiq <- dagitty( "dag {
   mom_hs -> kid_score
   mom_hs -> mom_work
   mom_hs <- U -> mom_age -> kid_score
   U -> mom_work -> kid_score
   U -> mom_iq -> kid_score
   U -> kid_score
}")

coordinates(kidiq) <- list(
  x = c(mom_hs = 1.5, U = 1, mom_age = 3, mom_work = 2.5, mom_iq = 2.5, kid_score = 4),
  y = c(mom_hs = 5, U = 2, mom_age = 2, mom_work = 4.2, mom_iq = 2.8, kid_score = 5))

plot(kidiq)
```

<img src="rex09_files/figure-html/unnamed-chunk-2-1.png" width="576" />

<br>

Our this data to estimate the total average causal effect of 
mom_hs on kid_score. The DAG above is a starting point, U is short for many 
unobserved variables, think mother's background (socioeconomic factors, genetic 
factors, etc.). According to the DAG, the total average causal effect is 
unidentified as we cannot adjust for U. However, let us proceed with the
assumption that the direct causal effect of U on kid_score is negligible. 
(*NOTE:* This is a very strong assumption indeed, 
this example is just for R-practice. Much more data on background variables, 
including longitudinal data would of course be needed to really address the 
question of the causal effect of mothers education on their kids cognitive 
abilities.) 

Last time (REX08), we mainly looked at the crude model `kid_score ~ mom_hs`. Now 
we will evaluate several models, including the potential confounders `mom_is` and 
`age`, and we will also fit a model with the mediator `mom_work` to address the 
direct effect of `mom_hs` on `kid_score` (an approach that is dangerous, as it 
may induce collider bias, as we will discuss further in research Methods 1).

<br>


First we will do some recoding: Center the continuous covariates (to help interpretation of 
coefficients), make mom_work a factor-variable, and recode the outcome, 
to have the IQ-like mean = 100 and sd = 15 (since we have some feeling for IQ-score, 
but not for the kid_score variable measured on an unknown scale).


```r
# Center mom_iq to be used as continuous variable in regression analysis
d$mom_iq_c <- d$mom_iq - mean(d$mom_iq)

# Center mom_age to mean age 
d$mom_age_c <- d$mom_age - mean(d$mom_age)

# Factor variable of mom_work
d$mom_work_f <- factor(d$mom_work, levels = c(1, 2, 3, 4),
                       labels = c("1_nowork", "2_worky2y3", 
                                  "3_parttime", "4_fulltime"))

# Linear transform kid_score to iq-scale: mean = 100, sd = 15
# Can be done in one step, I prefer to do it in two steps, via z-scores
zkid <- (d$kid_score - mean(d$kid_score)) / sd(d$kid_score)
d$kid_iq <- zkid * 15 + 100

str(d)
```

```
## 'data.frame':	434 obs. of  9 variables:
##  $ kid_score : int  65 98 85 83 115 98 69 106 102 95 ...
##  $ mom_hs    : int  1 1 1 1 1 0 1 1 1 1 ...
##  $ mom_iq    : num  121.1 89.4 115.4 99.4 92.7 ...
##  $ mom_work  : int  4 4 4 3 4 1 4 3 1 1 ...
##  $ mom_age   : int  27 25 27 25 27 18 20 23 24 19 ...
##  $ mom_iq_c  : num  21.12 -10.64 15.44 -0.55 -7.25 ...
##  $ mom_age_c : num  4.21 2.21 4.21 2.21 4.21 ...
##  $ mom_work_f: Factor w/ 4 levels "1_nowork","2_worky2y3",..: 4 4 4 3 4 1 4 3 1 1 ...
##  $ kid_iq    : num  84 108.2 98.7 97.2 120.7 ...
```

<br>


Here a scatter plot matrix, to have a look at how our main variables are related


```r
pairs(kid_iq  ~  mom_hs + mom_iq_c + mom_age_c + mom_work_f, data = d)
```

<img src="rex09_files/figure-html/unnamed-chunk-4-1.png" width="672" />



<br>

Fit models, here I am using lm().


```r
# Crude model
crude <- lm(kid_iq ~ mom_hs, data = d)

# Adj1: Adjust for mom_iq
adj1 <- lm(kid_iq ~ mom_hs + mom_iq_c, data = d)  

# Adj2: Adjust for mom_age
adj2 <- lm(kid_iq ~ mom_hs + mom_age_c, data = d)

# Adj3: Adjust for mom_work
adj3 <- lm(kid_iq ~ mom_hs + mom_work_f, data = d)

# Adj4: Adjust for mom_iq and mom_age
adj4 <- lm(kid_iq ~ mom_hs + mom_iq_c + mom_age_c, data = d)

# Adj5: Adjust for mom_iq and mom_age and mom_work
adj5 <- lm(kid_iq ~ mom_hs + mom_iq_c + mom_age_c + mom_work_f, data = d)
```

<br>

Let's look at the largest model, to verify that we understand the values of 
the coefficients


```r
summary(adj5)
```

```
## 
## Call:
## lm(formula = kid_iq ~ mom_hs + mom_iq_c + mom_age_c + mom_work_f, 
##     data = d)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -39.989  -8.889   1.481   8.564  36.084 
## 
## Coefficients:
##                      Estimate Std. Error t value Pr(>|t|)    
## (Intercept)          95.36389    1.83969  51.837   <2e-16 ***
## mom_hs                3.99398    1.70880   2.337   0.0199 *  
## mom_iq_c              0.40631    0.04511   9.008   <2e-16 ***
## mom_age_c             0.15895    0.24510   0.649   0.5170    
## mom_work_f2_worky2y3  2.19198    2.06722   1.060   0.2896    
## mom_work_f3_parttime  4.03336    2.39021   1.687   0.0922 .  
## mom_work_f4_fulltime  1.04305    1.84919   0.564   0.5730    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 13.33 on 427 degrees of freedom
## Multiple R-squared:  0.2213,	Adjusted R-squared:  0.2103 
## F-statistic: 20.22 on 6 and 427 DF,  p-value: < 2.2e-16
```

<br>

Here I extract the coefficient for mom_hs for each model with a user made 
function.

```r
# Extract coefficients for a model, given as input to the function
get_estimate <- function(modelfit) {
 b <- coef(modelfit)["mom_hs"]  # Point estimate for mom_hs
 cilo <- confint(modelfit)["mom_hs", "2.5 %"]  # CI low
 cihi <- confint(modelfit)["mom_hs", "97.5 %"] # CI high
 out <- data.frame(b, cilo = cilo, cihi = cihi)  # Put in a data frame
 
 # Here a trick to get model name as chr: deparse(substitute())
 out$model <- deparse(substitute(modelfit))  # Google is your best friend!
 
 out # Output from function
}

# Since the output is a data frame, I can combined them with rbind()
est <-rbind(get_estimate(crude),
            get_estimate(adj1),
            get_estimate(adj2),
            get_estimate(adj3),
            get_estimate(adj4),
            get_estimate(adj5))
est
```

```
##                b      cilo      cihi model
## mom_hs  8.650806 5.2961944 12.005418 crude
## mom_hs1 4.372795 1.1779394  7.567650  adj1
## mom_hs2 8.312727 4.8773837 11.748069  adj2
## mom_hs3 8.276976 4.8142638 11.739689  adj3
## mom_hs4 4.150143 0.8890467  7.411238  adj4
## mom_hs5 3.993982 0.6352806  7.352683  adj5
```

<br>

Plot model estimates


```r
## Illustrate causal estimate --------------------------------------------------
# This function add a point with confidence interval to an existing plot
# Arguments: Model -- Regression model including the predictor mom_hs
#            xpos -- Position along x-axis of the point and CI
add_estimate <- function(model, xpos) {
  # Model estimate of causal effect
  estimate <- model$coefficients['mom_hs']
  # It's 95 % CI
  ci <- confint(model)['mom_hs',]
  
  # Draw CI in plot
  arrows(x0 = xpos, x1 = xpos, y0 = ci[1], y1 = ci[2], 
         length = 0.05, angle = 90, code = 3)
  # Draw point
  points(xpos, estimate, pch = 21, bg = 'grey')
}

# Make plot
plot(0, xlim = c(0, 5), ylim = c(-1, 15), pch = '', xlab = 'Model', 
     ylab = 'Causal estimate', xaxt='n')

# Add line at zero causal effect
lines(c(-1, 6), c(0, 0), lty = 1, col = 'grey')

# Use function to add data points and error bars (95 % CIs)
add_estimate(crude, 0)
add_estimate(adj1, 1)
add_estimate(adj2, 2)
add_estimate(adj3, 3)
add_estimate(adj4, 4)
add_estimate(adj5, 5)

axis(1, at = 0:5, cex.axis = 0.5, padj = 0.5, 
     labels = c("mom_hs", "mom_hs+\nmom_iq", "mom_hs+\nmom_age",
                "mom_hs+\nmom_work", "mom_hs+\nmom_iq+\nmom_age",
                "mom_hs+\nmom_iq+\nmom_age+\nmom_work"))
```

<img src="rex09_files/figure-html/unnamed-chunk-8-1.png" width="672" />

```r
## -----------------------------------------------------------------------------
```

<br>

Finally, let's plot `kid_iq` versus `mom_iq` with separate lines for the two 
values of `mom_hs`, using fit from adj1.


```r
## Draw figure of additive model

# Empty plot
plot(d$mom_iq_c, d$kid_iq, axes = FALSE, pch = "",
     xlim = c(-30, 40), ylim = c(50, 150), xlab = "Mother IQ-score", 
     ylab = "Kid IQ-score")

# Add points for mom_hs = 0 (blue) and mom_hs = 1 )(white)
points(d$mom_iq_c[d$mom_hs == 0], d$kid_iq[d$mom_hs == 0], 
       pch = 21, bg = "blue")
points(d$mom_iq_c[d$mom_hs == 1], d$kid_iq[d$mom_hs == 1], 
       pch = 21, bg = "white")

# Add regression lines
cf <- adj1$coefficients  # Coefficients of model
xline <- c(-30, 40)  # Two points along the x-axis
yline0 <- cf[1] + cf[3]*xline # Predictions for mom_hs = 0
yline1 <- cf[1] + cf[2] + cf[3]*xline # Predictions for mom_hs = 1
lines(xline, yline0 , col = "blue")
lines(xline, yline1 , col = "black", lty = 3)

# Confidence bands around regression line ---
# Calculate CI-bands mom_hs = 0  
xline <- seq(-30, 40, 0.1)
hs0 <- rep(0, length(xline))
newdata0 <- data.frame(mom_iq_c = xline, mom_hs = hs0)
ci0 <- predict(adj1, newdata=newdata0, interval="confidence", level = 0.95)

# Shade area mom_hs = 0 
polygon(x = c(xline, rev(xline)), y = c(ci0[,2], rev(ci0[,3])), 
        col =  rgb(0, 0, 1, 0.1), border = FALSE)      

# Calculate CI-bands mom_hs = 1 
hs1 <- rep(1, length(xline))
newdata0 <- data.frame(mom_iq_c = xline, mom_hs = hs1)
ci1 <- predict(adj1, newdata=newdata0, interval="confidence", level = 0.95)

# Shade area mom_hs = 1 
polygon(x = c(xline, rev(xline)), y = c(ci1[,2], rev(ci1[,3])), 
        col =  rgb(0, 0, 0, 0.1), border = FALSE)  # shaded area
# ---

# Add axis (transforming back mom_iq to mean = 100)
axis(1, at = seq(-30, 40, 10), 
     labels = seq(-30, 40, 10) + mean(d$mom_iq), # Transforming back to mom_iq
     pos = 50) 
axis(2, at = seq(50, 150, 10), las = 2, pos = -30)

# Add legend
points(-25, 148, pch = 21, bg = "blue", cex = 0.8)
points(-25, 143, pch = 21, bg = "white", cex = 0.8)
text(x = -25, y = 148, labels = "Mother did not complete high-school", 
     pos = 4, cex = 0.7)
text(x = -25, y = 143, labels = "Mother completed high-school", 
     pos = 4, cex = 0.7)
```

<img src="rex09_files/figure-html/unnamed-chunk-9-1.png" width="672" />





<br>

### All the above code, please copy to script {-}

Here is all the code above, please copy and paste into a script file. 


```r
## Analyses of data in kidiq.txt, see REX09 for code book

## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Load libraries --------------------------------------------------------------
# library(rstanarm) # Not used here
## .............................................................................


## Load data -------------------------------------------------------------------
d <- read.table("kidiq.txt", sep = ",", header = TRUE)
## .............................................................................


## Recode variables ------------------------------------------------------------
# Center mom_iq to be used as continuous variable in regression analysis
d$mom_iq_c <- d$mom_iq - mean(d$mom_iq)

# Center mom_age to mean age 
d$mom_age_c <- d$mom_age - mean(d$mom_age)

# Factor variable of mom_work
d$mom_work_f <- factor(d$mom_work, levels = c(1, 2, 3, 4),
                       labels = c("1_nowork", "2_worky2y3", 
                                  "3_parttime", "4_fulltime"))

# Linear transform kid_score to iq-scale: mean = 100, sd = 15
# Can be done in one step, I prefer to do it in two steps, via z-scores
zkid <- (d$kid_score - mean(d$kid_score)) / sd(d$kid_score)
d$kid_iq <- zkid * 15 + 100
## .............................................................................


## Scatter plot matrix for visual inspection of data ---------------------------
pairs(kid_iq  ~  mom_hs + mom_iq_c + mom_age_c + mom_work_f, data = d)
## .............................................................................


## Fit models ------------------------------------------------------------------
# Crude model
crude <- lm(kid_iq ~ mom_hs, data = d)

# Adj1: Adjust for mom_iq
adj1 <- lm(kid_iq ~ mom_hs + mom_iq_c, data = d)  

# Adj2: Adjust for mom_age
adj2 <- lm(kid_iq ~ mom_hs + mom_age_c, data = d)

# Adj3: Adjust for mpm_work
adj3 <- lm(kid_iq ~ mom_hs + mom_work_f, data = d)

# Adj4: Adjust for mom_iq and mom_age
adj4 <- lm(kid_iq ~ mom_hs + mom_iq_c + mom_age_c, data = d)

# Adj5: Adjust for mom_iq and mom_age and mom_work
adj5 <- lm(kid_iq ~ mom_hs + mom_iq_c + mom_age_c + mom_work_f, data = d)
## .............................................................................


## Extract coefficient for mom_hs ----------------------------------------------
# Function that extract coefficients for a model, given as input to the function
get_estimate <- function(modelfit) {
 b <- coef(modelfit)["mom_hs"]  # Point estimate for mom_hs
 cilo <- confint(modelfit)["mom_hs", "2.5 %"]  # CI low
 cihi <- confint(modelfit)["mom_hs", "97.5 %"] # CI high
 out <- data.frame(b, cilo = cilo, cihi = cihi)  # Put in a data frame
 
 # Here a trick to get model name as chr: deparse(substitute())
 out$model <- deparse(substitute(modelfit))  # Google is your best friend!
 
 out # Output from function
}

# Since the output is a data frame, I can combined them with rbind()
est <-rbind(get_estimate(crude),
            get_estimate(adj1),
            get_estimate(adj2),
            get_estimate(adj3),
            get_estimate(adj4),
            get_estimate(adj5))
est  # Print to console
##..............................................................................


## Illustrate causal estimates -------------------------------------------------
# This function add a point with confidence interval to an existing plot
# Arguments: Model -- Regression model including the predictor mom_hs
#            xpos -- Position along x-axis of the point and CI
add_estimate <- function(model, xpos) {
  # Model estimate of causal effect
  estimate <- model$coefficients['mom_hs']
  # It's 95 % CI
  ci <- confint(model)['mom_hs',]
  
  # Draw CI in plot
  arrows(x0 = xpos, x1 = xpos, y0 = ci[1], y1 = ci[2], 
         length = 0.05, angle = 90, code = 3)
  # Draw point
  points(xpos, estimate, pch = 21, bg = 'grey')
}

# Make plot
plot(0, xlim = c(0, 5), ylim = c(-1, 15), pch = '', xlab = 'Model', 
     ylab = 'Causal estimate', xaxt='n')

# Add line at zero causal effect
lines(c(-1, 6), c(0, 0), lty = 1, col = 'grey')

# Use function to add data points and error bars (95 % CIs)
add_estimate(crude, 0)
add_estimate(adj1, 1)
add_estimate(adj2, 2)
add_estimate(adj3, 3)
add_estimate(adj4, 4)
add_estimate(adj5, 5)

axis(1, at = 0:5, cex.axis = 0.5, padj = 0.5, 
     labels = c("mom_hs", "mom_hs+\nmom_iq", "mom_hs+\nmom_age",
                "mom_hs+\nmom_work", "mom_hs+\nmom_iq+\nmom_age",
                "mom_hs+\nmom_iq+\nmom_age+\nmom_work"))
## -----------------------------------------------------------------------------


## Draw model prediction mom_hs + mom_iq ---------------------------------------
# Empty plot
plot(d$mom_iq_c, d$kid_iq, axes = FALSE, pch = "",
     xlim = c(-30, 40), ylim = c(50, 150), xlab = "Mother IQ-score", 
     ylab = "Kid IQ-score")

# Add points for mom_hs = 0 (blue) and mom_hs = 1 )(white)
points(d$mom_iq_c[d$mom_hs == 0], d$kid_iq[d$mom_hs == 0], 
       pch = 21, bg = "blue")
points(d$mom_iq_c[d$mom_hs == 1], d$kid_iq[d$mom_hs == 1], 
       pch = 21, bg = "white")

# Add regression lines
cf <- adj1$coefficients  # Coefficients of model
xline <- c(-30, 40)  # Two points along the x-axis
yline0 <- cf[1] + cf[3]*xline # Predictions for mom_hs = 0
yline1 <- cf[1] + cf[2] + cf[3]*xline # Predictions for mom_hs = 1
lines(xline, yline0 , col = "blue")
lines(xline, yline1 , col = "black", lty = 3)

# Confidence bands around regression line ---
# Calculate CI-bands mom_hs = 0  
xline <- seq(-30, 40, 0.1)
hs0 <- rep(0, length(xline))
newdata0 <- data.frame(mom_iq_c = xline, mom_hs = hs0)
ci0 <- predict(adj1, newdata=newdata0, interval="confidence", level = 0.95)

# Shade area mom_hs = 0 
polygon(x = c(xline, rev(xline)), y = c(ci0[,2], rev(ci0[,3])), 
        col =  rgb(0, 0, 1, 0.1), border = FALSE)      

# Calculate CI-bands mom_hs = 1 
hs1 <- rep(1, length(xline))
newdata0 <- data.frame(mom_iq_c = xline, mom_hs = hs1)
ci1 <- predict(adj1, newdata=newdata0, interval="confidence", level = 0.95)

# Shade area mom_hs = 1 
polygon(x = c(xline, rev(xline)), y = c(ci1[,2], rev(ci1[,3])), 
        col =  rgb(0, 0, 0, 0.1), border = FALSE)  # shaded area
# ---

# Add axis (transforming back mom_iq to mean = 100)
axis(1, at = seq(-30, 40, 10), 
     labels = seq(-30, 40, 10) + mean(d$mom_iq), # Transforming back to mom_iq
     pos = 50) 
axis(2, at = seq(50, 150, 10), las = 2, pos = -30)

# Add legend
points(-25, 148, pch = 21, bg = "blue", cex = 0.8)
points(-25, 143, pch = 21, bg = "white", cex = 0.8)
text(x = -25, y = 148, labels = "Mother did not complete high-school", 
     pos = 4, cex = 0.7)
text(x = -25, y = 143, labels = "Mother completed high-school", 
     pos = 4, cex = 0.7)
```


<br><br>
