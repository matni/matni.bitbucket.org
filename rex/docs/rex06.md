# Notes REX06 {-}



Today we will look at some useful functions that take functions as arguments! 
Such a functional argument may be a standard R-function or it your own user-made 
function. 

For illustration, we will use the data set `kidiq.txt` (from @gelman2020regression).
The data is from a survey of adult American women and their 
children. @gelman2020regression describe the data set and use it at several places, 
e.g., pp. 130-136, 156-158, 161, 185-187, and 197. We will use this data set a 
lot in Stat1 to illustrate linear models, so it's a good idea to get to know it 
already know. 

[Follow this link to find data](https://bitbucket.org/matni/matni.bitbucket.org/raw/3dee78fc9ace615a849b5ce8cb5abd381ebb1da6/rex/kidiq.txt){target="blank"} 
Save (Ctrl+S on a PC) to download as text file

Code book:  

+ **kid_score** \ Test score children, on IQ-like measure (in the sample with 
mean and standard deviation of 87 and 20 points, respectively).
+ **mom_hs** \ Indicator variable: mother did complete high-school (1) or not (0)
+ **mom_iq** \ Test score mothers, transformed to IQ score: mean = 100, sd = 15. 
+ **mom_work** \ Working status of mother, coded 1-4:
    - 1: mother did not work in first three years of child's life,
    - 2: mother worked in second or third year of child's life, 
    - 3: mother worked part-time in first year of child's life,
    - 4: mother worked full-time in first year of child's life. 
+ **mom_age** Age of mothers, years

The data file is comma-delimited and include variable names.


```r
d <- read.table("kidiq.txt", sep = ",", header = TRUE)
str(d)
```

```
## 'data.frame':	434 obs. of  5 variables:
##  $ kid_score: int  65 98 85 83 115 98 69 106 102 95 ...
##  $ mom_hs   : int  1 1 1 1 1 0 1 1 1 1 ...
##  $ mom_iq   : num  121.1 89.4 115.4 99.4 92.7 ...
##  $ mom_work : int  4 4 4 3 4 1 4 3 1 1 ...
##  $ mom_age  : int  27 25 27 25 27 18 20 23 24 19 ...
```

## Functions as arguments {-}

We will start with one of may favorites `aggregate()` useful for 
subgroup analysis. We will then meet the head of the apply-family: `apply()` 
(and save other family members for later, such as `lapply()`, `sapply()`, and 
`tapply()`), before we move on to another favorite `replicate()`, useful 
for simulations of various types, for example power analysis and bootstrapping. 


<br>

### Subgroup analysis using aggregate() {-}

Let's calculate the mean `kid_score` separately for kids to mothers who completed 
high-school and kids to mothers who did not completed high-school. We'll use the 
aggregate() function for this. Note that the `mean()` is used as argument:
`FUN = mean`. Note also that the function require some arguments to be lists, a 
data structure that we have not discussed so far. Any vector x can made a list 
using the `list(x)` function.


```r
# The output is a data frame with mean values per subgroup
m <- aggregate(x = d$kid_score, by = list(d$mom_hs), FUN = mean)
m
```

```
##   Group.1        x
## 1       0 77.54839
## 2       1 89.31965
```

The column names are not very informative. A better way is to name columns directly, 
by specifying them inside `list()`:


```r
# The output is a data frame with mean values per subgroup. Note the naming of
# column names.
m <- aggregate(x = list(kid_score_m = d$kid_score), 
               by = list(mom_hs = d$mom_hs), FUN = mean)
m
```

```
##   mom_hs kid_score_m
## 1      0    77.54839
## 2      1    89.31965
```

<br>

We may split by several variables, here shown for `mom_hs` $\times$ `mom_work`:


```r
m <- aggregate(x = list(kid_score = d$kid_score), 
               by = list(mom_hs = d$mom_hs, mom_work = d$mom_work), 
               FUN = mean)
m
```

```
##   mom_hs mom_work kid_score
## 1      0        1  70.81818
## 2      1        1  90.38636
## 3      0        2  82.65217
## 4      1        2  86.86301
## 5      0        3  85.72727
## 6      1        3  95.40000
## 7      0        4  78.11538
## 8      1        4  88.53073
```
<br>

And we may include several outcome variables, here both `kid_score` and `mom_iq`:


```r
m <- aggregate(x = list(kid_score = d$kid_score, mom_iq = d$mom_iq), 
               by = list(mom_hs = d$mom_hs, mom_work = d$mom_work), 
               FUN = mean)
m
```

```
##   mom_hs mom_work kid_score    mom_iq
## 1      0        1  70.81818  89.05165
## 2      1        1  90.38636 103.44483
## 3      0        2  82.65217  94.35025
## 4      1        2  86.86301  97.94155
## 5      0        3  85.72727  95.28721
## 6      1        3  95.40000 108.01183
## 7      0        4  78.11538  91.87583
## 8      1        4  88.53073 102.19258
```

If you need to specify function arguments, just add them after the `FUN` argument. 
Here I illustrate with the arguments `na.rm = TRUE` and `trim = 0.2` for the 
`mean()` function. (We have no missing data , so `na.rm = TRUE` is not 
needed, but it makes no harm).


```r
m <- aggregate(x = list(kid_score_m = d$kid_score, mom_iq_m = d$mom_iq), 
               by = list(mom_hs = d$mom_hs, mom_work = d$mom_work), 
               FUN = mean, na.rm = TRUE, trim = 0.2)
m
```

```
##   mom_hs mom_work kid_score_m  mom_iq_m
## 1      0        1    70.76190  85.75840
## 2      1        1    91.21429 102.48830
## 3      0        2    85.20000  93.47050
## 4      1        2    88.75556  97.05637
## 5      0        3    89.14286  93.95881
## 6      1        3    97.33333 107.70927
## 7      0        4    79.62500  90.74766
## 8      1        4    90.50459 100.68531
```

<br>

Let's try with a user-made function. Here I first create a function and the use 
it as argument to `aggregate()`


```r
# Here a function that calculates the distance between the upper and lower 10 %,
# a kind of range, but excluding the most extreme values
range90 <- function(x){
  q <- quantile(x, prob = c(0.1, 0.9))
  out <- q[2] - q[1]
  out
}

r90 <- aggregate(x = list(kid_score_r90 = d$kid_score), 
               by = list(mom_hs = d$mom_hs, mom_work = d$mom_work), 
               FUN = range90)
r90
```

```
##   mom_hs mom_work kid_score_r90
## 1      0        1          58.4
## 2      1        1          47.9
## 3      0        2          55.6
## 4      1        2          55.4
## 5      0        3          43.0
## 6      1        3          37.6
## 7      0        4          62.0
## 8      1        4          50.2
```

<br>

In this example, my function outputs a vector with several elements:


```r
my_stats <- function(x){
  m <- mean(x, na.rm = TRUE)
  s <- sd(x, na.rm = TRUE)
  n <- length(x)
  out <- c(m = m, sd = s, n = n)
  out
}

stats <- aggregate(x = list(kid_score_stat = d$kid_score), 
               by = list(mom_hs = d$mom_hs, mom_work = d$mom_work), 
               FUN = my_stats)
stats
```

```
##   mom_hs mom_work kid_score_stat.m kid_score_stat.sd kid_score_stat.n
## 1      0        1         70.81818          20.98579         33.00000
## 2      1        1         90.38636          18.35593         44.00000
## 3      0        2         82.65217          26.34320         23.00000
## 4      1        2         86.86301          20.15586         73.00000
## 5      0        3         85.72727          17.32681         11.00000
## 6      1        3         95.40000          15.70669         45.00000
## 7      0        4         78.11538          21.61541         26.00000
## 8      1        4         88.53073          19.33161        179.00000
```

<br>

Note that the resulting data frame is a bit peculiar:

```r
str(stats)
```

```
## 'data.frame':	8 obs. of  3 variables:
##  $ mom_hs        : int  0 1 0 1 0 1 0 1
##  $ mom_work      : int  1 1 2 2 3 3 4 4
##  $ kid_score_stat: num [1:8, 1:3] 70.8 90.4 82.7 86.9 85.7 ...
##   ..- attr(*, "dimnames")=List of 2
##   .. ..$ : NULL
##   .. ..$ : chr [1:3] "m" "sd" "n"
```

It has three variables, the third has four-element numerical vectors as elements!
It takes some time to understand how to subset specific elements from this vector. 
Here is two ways of finding sd for `kid_score`  at `mom_hs = 0` and 
`mom_work = 4`:


```r
# Two ways to pick out Row 7, Column 3, Element 2 
stats[7, 3][2]  
```

```
## [1] 21.61541
```

```r
stats[stats$mom_hs == 0 & stats$mom_work == 4, "kid_score_stat"][2]
```

```
## [1] 21.61541
```

### Meet the head of the apply family {-}

There is a family of functions called something with `apply`, here we look at the 
head of the family, namely `apply()` and leave `lapply()`,  `sapply()` and others 
for another day (in fact, `aggregate()` that you already met is a close relative, 
related to `tapply()`) 

The `apply()` function let you do things with either rows or columns of a matrix. 
Here I use it to calculate `my_stats()` (my function defined above) on selected 
columns of data frame `d` (that contains the kidiq data).


```r
# Calculate my_stats on columns 1, 3 and 5 of d. Calculate over columns by
# setting MARGIN = 2 (1 would be over rows). 
iq_stats <- apply(X = d[, c(1, 3, 5)], MARGIN = 2, FUN = my_stats)
iq_stats 
```

```
##    kid_score mom_iq   mom_age
## m   86.79724    100  22.78571
## sd  20.41069     15   2.70107
## n  434.00000    434 434.00000
```

<br>

### Simulation using replicate() {-}

Bootstrapping is a powerful frequentist method for generating confidence 
intervals (CIs). Here I bootstrap a 95 % CI around the 20% trimmed mean of 
`d$kid_iq`, with the help of the `replicate()` function.


```r
# Function that draws a sample of n values, with replacement, from a vector 
# with n elements
drawsample <- function(x){
  bootsample <- sample(x, size = length(x), replace = TRUE)
  mean20 <- mean(bootsample, trim = 0.2)
  mean20
}

# Repeat many times on the data in d$kid_score
my_boot <- replicate(1e4, drawsample(d$kid_score))

# Display bootstrap
hist(my_boot, breaks = 100, main = "", 
     xlab = "Kid-score: Bootstrapped 20 % trimmed mean")

# Calculate point estimate and 95 % confidence interval
m20 <- mean(d$kid_score, trim = 0.2)
ci95 <- quantile(my_boot, prob = c(0.025, 0.975))

# Add to histogram
points(m20, 20, pch = 21, bg = "blue", cex = 1.5)
lines(ci95, c(20, 20), col = "blue")
```

<img src="rex06_files/figure-html/unnamed-chunk-12-1.png" width="672" />

```r
# Print to console
round(c(m20 = m20, ci95), 1)
```

```
##   m20  2.5% 97.5% 
##  89.1  86.9  91.1
```

<br>

## Script: VVG and aggregssion {-}

Here a simple script that creates data consistent with this DAG:



```r
library(dagitty) 

vvg_dag <- dagitty( "dag {
   VVG -> AGG
   VVG -> PA -> AGG
   U -> PA -> BMI
   VVG <- Support -> SProb -> AGG
   SProb <- U -> AGG
}")

coordinates(vvg_dag) <- list(
  x = c(VVG = 1, Support = 1, BMI = 2, PA = 3, SProb = 3, U = 4, AGG = 4),
  y = c(VVG = 5, Support = 1, BMI = 3, PA = 4, SProb = 2, U = 1, AGG = 5))

plot(vvg_dag)
```

<img src="rex06_files/figure-html/unnamed-chunk-13-1.png" width="672" />
  
  
Exposure: Violent video games (VVG)   

Outcome: Adult aggression (AGG)  

Observed covariates: Physical activity (PA), Parental support (Support), 
School problems (SProb), BMI (BMI)

Unobserved covariate: U (e.g., genetic predisposition to get into trouble)

<br>


The simulation do not care about units, so you may consider all variables 
as z-transformed with mean (about) zero and standard deviation roughly around one.


```r
# Simulating data consistent with a Directed Acyclical Graph(DAG):
# Violent video games (VVG) as exposure and Adult aggression (AGG) as outcome.
# Covariates: Physical activity (PA), Parental support (Support), 
# School problems (SProb), BMI (BMI).
# Unobserved covariate: U (e.g., genetic predisposition to get into trouble).
# 
# DAG:
# VVG -> AGG
# VVG -> PA -> AGG
# U -> PA -> BMI
# VVG <- Support -> SProb -> AGG
# SProb <- U -> AGG


## Clear things and set random seed --------------------------------------------
rm(list = ls())
graphics.off()
cat("\014")
set.seed(123)
## .............................................................................


## Simulate data ---------------------------------------------------------------
n = 1e3  # Sample size

# Define total causal effect of vvg on agg
dir_ce <- 0.3  # Direct causal effect of vvg on agg
bvvg_pa <- -1  # Causal effect of vvg on pa
bpa_agg <- -0.3 # Causal effect of pa on agg
ind_ce <- bvvg_pa * bpa_agg  # Indirect causal effect
total_ce <- dir_ce + ind_ce # This is what we want to estimate! (= PATE)

# Exogenous variables
support <- rnorm(n)  # Standardized scores, drawn from the Standard normal
u <- rnorm(n)

# Endogenous Variable
vvg <-   rnorm(n) - 0.5 * support
pa <-    rnorm(n) + bvvg_pa * vvg + 0 * u  # Check with non-zero coefficient!
bmi <-   rnorm(n) + -1.2 * pa
sprob <- rnorm(n) + -0.8 * support  + 0.5 * u
agg <-   rnorm(n) + 0.3 * vvg      + bpa_agg * pa   + 1.5 * sprob + 1 * u

# Combine in data frame (not needed, but makes it look like a real data set)
d <- data.frame(vvg, agg, support, pa, bmi, sprob, u)
## .............................................................................


## Estimate causal effects ------------------------------------------------------
# I am using lm() here, please try stan_glm(), same thing. lm() is much faster 
# for large n.

# To be estimated = Population average causal effect (PATE)
total_ce  

adj0 <- lm(agg ~ vvg, data = d)  # Bias: Omitted variable bias (= Confounding bias)

adj1 <- lm(agg ~ vvg + support, data = d) # No bias

adj2 <- lm(agg ~ vvg + sprob, data = d) # Bias: Collider bias

adj3 <- lm(agg ~ vvg + support + sprob, data = d) # No bias

adj4 <- lm(agg ~ vvg + pa + support, data = d) # Bias: Over-adjustment bias 

adj5 <- lm(agg ~ vvg + bmi + support, data = d) # Bias: Over-adjustment bias

# Note that model adj4 estimates correctly the direct causal effect (dir_ce), as 
# long as the path u -> pa is set to zero. If not, adj4 yields a biased estimate  
# of the direct effect (as well as of the total average causal effect). 
## .............................................................................


## Illustrate causal estimate --------------------------------------------------
# This function add a point with confidence interval to an existing plot
# Arguments: Model -- Regression model including the predictor vvg
#            xpos -- Position along x-axis of the point and CI
add_estimate <- function(model, xpos) {
  # Model estimate of causal effect
  estimate <- model$coefficients['vvg']
  # It's 95 % CI
  ci <- confint(model)['vvg',]
  
  # Draw CI in plot
  arrows(x0 = xpos, x1 = xpos, y0 = ci[1], y1 = ci[2], 
         length = 0.05, angle = 90, code = 3)
  # Draw point
  points(xpos, estimate, pch = 21, bg = 'grey')
}

# Make plot
plot(0, xlim = c(0, 5), ylim = c(-0.1, 1.5), pch = '', xlab = 'Model', 
     ylab = 'Causal estimate')
# Add line at true total average causal effect
lines(c(-1, 6), c(total_ce, total_ce), lty = 2)
# Add line at true direct average causal effect
lines(c(-1, 6), c(dir_ce, dir_ce), lty = 3, col = "grey")
# Add line at zero causal effect
lines(c(-1, 6), c(0, 0), lty = 1, col = 'grey')

# Use function to add data points and error bars (95 % CIs)
add_estimate(adj0, 0)
add_estimate(adj1, 1)
add_estimate(adj2, 2)
add_estimate(adj3, 3)
add_estimate(adj4, 4)
add_estimate(adj5, 5)
## .............................................................................
```

<br>

