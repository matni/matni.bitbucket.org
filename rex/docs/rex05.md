# Notes REX05 {-}




We have already generated a lot of random numbers using R. Today some more details 
on how this works. Then we will start with a new simulation. 


## Random numbers {-}
Computers cannot generate true randomness. What you get is pseudo-random
numbers. Fortunately, these behave almost exactly as truly random numbers so
most users, including us, will not notice the difference. A good thing
we pseudo-randomness is that it allows us to make our analyses reproducible, 
using the `set.seed()` function:

```r
set.seed(13)  # Use any integer as argument
rnorm(5)  # Random numbers
```

```
## [1]  0.5543269 -0.2802719  1.7751634  0.1873201  1.1425261
```

```r
rnorm(5)  # New numbers
```

```
## [1]  0.4155261  1.2295066  0.2366797 -0.3653828  1.1051443
```

```r
set.seed(13)
rnorm(5)  # Same as first above
```

```
## [1]  0.5543269 -0.2802719  1.7751634  0.1873201  1.1425261
```

Any script generating random numbers should set the seed at the
beginning to assure that you or someone else can reproduce the results.
The use of random number generators for statistical data analysis has
increased markedly during the last few decades. Whenever using random numbers, 
don't forget to set the seed.

<br> 

### Random number generators {-}
So far, we have used two random-number generators, `sample()` and
`rnorm()`. The `sample()` function let us sample with or without replacement from any 
vector (int, num, cha, or logi), irrespective of the distribution of values in
that vector. Here a simple example, where we roll two dice many times and estimate 
the probability of the sum being 11 and the probability of the sum being 12. 
(Leibniz thought these were the same, known as "Leibniz' mistake".)


```r
n <- 1e5  # Number or rolls
d1 <- sample(x = 1:6, size = n, replace = TRUE)  # First dice
d2 <- sample(x = 1:6, size = n, replace = TRUE)  # Second dice
sumscore <- d1 + d2  # Sum score
estp11 <- mean(sumscore == 11)  # Proportion 11
estp12 <- mean(sumscore == 12)  # Proportion 12
truep11 <- 2/36  # There are two ways to get sum = 11: 5 + 6 and  6+ 5
truep12 <- 1/36 # Only one way to get sum = 12: 6 + 6

# Put results in vector and print to Console
leibniz <- c(true_p11 = truep11, est_p11 = estp11, 
             true_p12 = truep12, est_p12 = estp12)
round(leibniz, 4)
```

```
## true_p11  est_p11 true_p12  est_p12 
##   0.0556   0.0563   0.0278   0.0285
```
<br>

`rnorm()` samples from a normal distribution of infinite size
(so sampling with or without replacement is not an issue). R has random number 
generators for a large set of probability distributions, not only for the normal. 
This code uses three of these.  

```r
n <- 1e5
a <- rnorm(n)  # Normal distribution, default parameters: mean = 0, sd = 1
b <- rexp(n)   # Exponential distribution, default parameter: rate = 1
c <- runif(n)  # Uniform distribution, default parameters: min = 0, max = 1
par(mfrow = c(1,3))  # Locate three subplots in one row
hist(a, breaks = 100, main = 'Normal')
hist(b, breaks = 100, main = 'Exponential')
hist(c, breaks = 100, main = 'Uniform')
```

<img src="rex05_files/figure-html/unnamed-chunk-3-1.png" width="672" />

<br>  

## Simulation to solve hard problems (approximately) {-}  

<br>

**What is the expected distance between two random points in a unit square?**


```r
par(mar = c(0, 0, 0, 0))
plot(c(0, 1), c(0, 1), pch = '')
lines(c(0.45, 0.73), c(0.47, 0.68))
points(c(0.45, 0.73), c(0.47, 0.68), pch = 21, bg = 'grey', cex = 0.7)
```

<img src="rex05_files/figure-html/line_in_box-1.png" width="192" />

<br>

Exact answer: \ \ $\frac{2+\sqrt{2}+5ln(\sqrt{2} + 1)}{15}$ 
= 0.5214054

<br>


With simulation we can easily get an approximate answer, with help of the very 
useful function `replicate()`.


```r
set.seed(111)

# A function that randomly selects tow points in the square and measure its length
# Remember Pythagoras: c = sqrt(a^2 + b^2)
linelength <- function(){
  xdist <- runif(1) - runif(1)  # Distance along x-axis two random points
  ydist <- runif(1) - runif(1)  # Distance along y-axis two random points
  out <- sqrt(xdist^2 + ydist^2)  # Line length as output
  out
}

# Replicate function n times
length_simul <- replicate(n = 1e5, linelength())

hist(length_simul, breaks = 200)  # Show sampling distribution
```

<img src="rex05_files/figure-html/simul-1.png" width="672" />

```r
expected_length <- mean(length_simul)  # Expected value
round(expected_length, 4)  # Print to console
```

```
## [1] 0.5217
```


## The Small sample fallacy {-}

You will find the largest and the smallest effects in studies with small samples, 
because random errors are more likely to cancel out in large than in small samples. 
Sometimes this statistical phenomenon is mistaken for a real causal effect. For 
instance, the average performance on standardized tests will probably be highest 
in small schools, but this is not because small schools make students better but 
because of random variability. If you looked at the other end, the worst 
average performance would probably also be from small schools. Here is a simulation 
of this phenomenon illustrated in a so called *funnel plot*. Here a sneaked in 
the `sapply()` function that is useful for repeated use of a function with 
different arguments each time.


```r
set.seed(999)

# Size of 250 schools, from n = 10 to n = 1000
nn <- sample(10:1000, size = 250, replace = TRUE)  # Random uniform

# Function that simulates a mean score for a school of size n 
mean_score <- function(n){
  out <- mean(rnorm(n, mean = 100, sd = 15))
  out 
 }

# Apply function on each of the school sizes in nn, using the sapply() function
x <- sapply(nn, mean_score)  # My user-made function as argument!

# n for worst school
largest_neg <- nn[x == min(x)]  # Size of school with the worst performance

# n for large effect sizes
largest_pos <- nn[x == max(x)] # Size of school with the best performance
ss <- c(worst_score = min(x), n_worst = largest_neg, 
        best_score = max(x), n_best = largest_pos)

# Funnel plot
plot(x, nn, xlim = c(90, 110), ylab = "School size", 
     xlab = "Mean performance on test")
```

<img src="rex05_files/figure-html/unnamed-chunk-4-1.png" width="384" />

School-size of worst and best average performance

```r
round(ss, 1)
```

```
## worst_score     n_worst  best_score      n_best 
##        95.2        54.0       106.4        36.0
```

<br>  

## Script: Observational study on noise expsoure and waist circumference {-}

Here is a very simple causal scenario illustrated in a 
Directed Acyclical Graph (DAG):


```r
library(dagitty) 

noise_dag <- dagitty( "dag {
   Noise -> Waist
   Noise <- Income -> Waist
}")

coordinates(noise_dag) <- list(
  x = c(Noise = 1, Income = 2, Waist = 3),
  y = c(Noise = 2, Income = 1, Waist = 2))

plot(noise_dag)
```

<img src="rex05_files/figure-html/unnamed-chunk-6-1.png" width="576" />

<br>

This is a simulated longitudinal study on the causal effect of road-traffic noise 
on waist circumference in middle-aged men.

+ Sample: Middle-aged men in urban area, sample size = n
+ Exposure: Residential traffic noise exposure [dB] during the last 10 years
+ Outcome: Waist circumference [cm] measured at end of study
+ Covariate: Income [kSEK/month], average over last 10 years

<br>

Simulate a data set consistent with the DAG above


```r
## Clear things ----------------------------------------------------------------
rm(list = ls())
graphics.off()
cat("\014")
## .............................................................................


## Simulate data ---------------------------------------------------------------
set.seed(123)
n <- 6000
id <- 1:n

# Data simulated in accordance with the DAG, assuming these linear effects:
# Exposure decreases with 0.2 dB per kSEK income.
# Outcome decreases with 0.1 cm per kSEK income, and increases with 0.05 cm 
# per dB.
# NOTE: Simulation below of fixed-effect = unrealistically assuming that all 
# single-unit causal effects are the same 

# Exogenous variable
income <- rnorm(n, mean = 35, sd = 8)

# Endogenous variables (Note centering of causal factors).
db <-    rnorm(n, mean = 60, sd = 4)  + -0.2 * (income - 35) 
waist <- rnorm(n, mean = 100, sd = 4) + -0.1 * (income - 35) + 0.05 * (db - 60)  

# Put in data frame
d <- data.frame(id = id, db = db, income = income, waist = waist)

# Clean global environment
rm(db, id, income, n, waist)
## .............................................................................


## Recode variables ------------------------------------------------------------
d$income_c <- d$income - mean(d$income)
d$db10_c <- (d$db - mean(d$db))/10 # Make unit 10 dB
## .............................................................................


## Estimate the total average causal effect of noise on waist using glm() ------
# If you have installed the library rstanarm, please try stan_glm()

# Crude (unadjusted) model: Biased causal effect estimate
crude0 <- glm(waist ~ db, data = d)
crude <- glm(waist ~ db10_c, data = d)  # Easier to interpret coefficients

# Adjusted, shoudl give an unbiased causal effect estimate (for large n)
adj1 <- glm(waist ~ db10_c + income_c, data = d)
## .............................................................................


## Extract selected output ----------------------------------------------------- 
crude_point <- crude$coefficients[2]  # Crude estimate causal effect of noise
crude_ci <- confint(crude)[2, ] # CI around crude estimaste

adj1_point <- adj1$coefficients[2]  # Adjusted estimate causal effect of noise
adj1_ci <- confint(adj1)[2, ] # CI around adjusted estimate
## .............................................................................


## Visualize causal effect estimates -------------------------------------------
# Empty plot
plot(NA, 
     xlim = c(0.5, 2.5), ylim = c(-1, 2),
     axes = FALSE,
     xlab = '', ylab = '',
     pch = '')

# Add line at zero effect
lines(c(0.5, 2.5), c(0, 0), lty = 2)

# Add line at population average total causal effect (PATE)
pate <- 0.5
lines(c(0.5, 2.5), c(pate, pate), lty = 3, col = "grey")

# Add crude estimate
lines(c(1, 1), crude_ci)
points(1, crude_point, pch = 21, bg = "grey")

# Add adjusted estimate
lines(c(2, 2), adj1_ci)
points(2, adj1_point, pch = 21, bg = "grey")

# Add x-axis
xticks <- c(0.5, 1, 2, 2.5)
axis(side = 1, pos = -1, at = xticks, 
     labels = c("", "Crude", "Adj1", ""),
     tck = 0.01, cex.axis = 0.8)
axis(side = 3, pos = 2,
     at = xticks, labels = c("", "", "", ""),
     tck = 0.01)

# Add y-axis
yticks <- seq(-1, 2, by = 0.2)
axis(side = 2, pos = 0.5,
     at = yticks,
     tck = 0.01, las = 2, cex.axis = 0.8)
axis(side = 4, pos = 2.5,
     at = yticks, labels = rep("", length(yticks)),
     tck = 0.01, las = 2)

# Add axis labels
mtext("Model", side = 1, line = 1)
mtext("Causal effect estimate [cm/10dB]", side = 2, line = 1.4)
## .............................................................................
```

<br>

### Exercises {-}

Try out different scenarios in the simulation to see what happens as you... 

(a) lower the sample size,
(b) try different set.seed() values,
(c) change true regression coefficients, for example, set one or both
coefficients for income to zero.
(d) Hard: rewrite the simulation, but now with random effects.

<br>

