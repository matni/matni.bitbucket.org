# Notes REX01 {-}



This seminar takes a first look at R and R-Studio. We'll
start with the Console, where you may interact directly with R. We will then
move to the text editor and start working on a simulation of a randomized 
experiment that we will continue to work on in the following seminars. 

<br>  

## Getting started with R and R-studio {-}
Make sure you have R (the statistical software) and R-studio (an interface
to R) installed on you laptop before this seminar. You may run R without R-studio
(but not vice versa), but I strongly recommend that you use R-Studio; it makes 
working with R much easier.

## The Console {-}
You may interact directly with R through the Console (typically the 
bottom left panel in R-studio). Here are some examples, just type the commands 
and hit Enter.

### Use as calculator {-}
$27 - 13$

```r
27 - 13
```

```
## [1] 14
```

$(5+3)8^{1/3}$

```r
(5 + 3) * 8^(1/3)
```

```
## [1] 16
```

$7!$

```r
factorial(7)
```

```
## [1] 5040
```

${7 \choose 2}$

```r
choose(7, 2)
```

```
## [1] 21
```

<br>  

### Get help  {-}
You may ask R for help using the `help()` function, e,g,, `help(lm)` will open 
R's help-page for the `lm()` function. You may also use the short-cut is `?lm`. 
R's help-pages are very useful, but takes some time to get used to.  

If you don't understand something or want more details than is provided, just 
Google it! R is an enormous and helpful community; if you have a question, it's 
almost certain that someone already posted it on line, and got several 
answers. 

<br> 

### Define data structures {-}
Store a set of numbers in a vector called x. (More on data structures in REX02)

```r
x <- c(1, 7, 7, 3, 23, -4, 2^3)
```

Note that x is not displayed in the Console unless called specifically

```r
x  # This is shorthand for print(x)
```

```
## [1]  1  7  7  3 23 -4  8
```

Randomly select (with replacement) 10 integers 
between 1 and 20, and store them in a vector called y

```r
y <- sample(seq(1, 20), size = 10, replace = TRUE)
y
```

```
##  [1]  3  1 12 13 10  7 18 15 11 17
```

Repeat 'red', 'green' four times and store them in a vector called condition

```r
condition <- rep(c('red', 'green'), 4)
condition
```

```
## [1] "red"   "green" "red"   "green" "red"   "green" "red"   "green"
```

<br> 

### Check objects {-}
The Console is useful for checking properties of R objects, for example, the 
number of elements in a vector, or its first (or last) values.

```r
a <- rep(month.abb, 5)  # help(month.abb) answers your question
length(a)  # Number of elements in vector a
```

```
## [1] 60
```

```r
head(a, 7)  # The first 7 values of vector a
```

```
## [1] "Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul"
```

```r
tail(a, 5)  # The last 5 values of vector a
```

```
## [1] "Aug" "Sep" "Oct" "Nov" "Dec"
```

A common mistake is to try to do things on the wrong type of data, such as
trying to do math on a character (or string) vector.
Experienced R-users often use `str()` to remind them about what
type of data they are working with (some of this information is also displayed in
the Environment panel of R-studio). 

```r
a + 4
```

```
## Error in a + 4: non-numeric argument to binary operator
```

```r
str(a)
```

```
##  chr [1:60] "Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" ...
```

<br> 

### Asking the Console to produce plots works fine: {-}

```r
random_data <- rnorm(1e4)  # 1e4 means 10000, 2e3 means 2000, ....
hist(random_data)
```

<img src="rex01_files/figure-html/unnamed-chunk-11-1.png" width="672" />

<br>   


<br>

## Script files {-}
You will do most of your typing in the script window (typically located above
the Console in R-Studio). Here you write blocks of code in a script file
(select File/New File/R Script or press Ctrl+Shift+N in  R-Studio to open a new 
script). The code in the script is not executed unless you ask for it, 
for example by pressing Ctrl-R. If you just want to run a part of the script, 
selected this part and then hit Ctrl-R. If you just want to run a single line, 
just put the cursor on the line and hit Ctrl-R. 

<br>  

## Script: Simulation randomized experiment {-}

Here we will simulate a simple randomized experiment, in which 
participants are randomized to one of two diets (diet0 or diet1), and the
weight (kg) of each participant is measured before (pre) and after (post) 
treatment, i.e., being on a specific diet for, say, six month.

Here is code for simulating data, please copy and paste in a new R-Script-file, 
and save the file, you may call it `weight_simul.R`.  
(There is a copy-code button to the right of the code chunk.)  

*NOTE: You don't have to understand this code (yet!)*, just run it to generate  
some data in a text file.


```r
# Simulation randomized experiment. 
# Here we will simulate a simple randomized experiment, in which 
# participants were randomized to one of two diets (diet0 or diet1), and the 
# outcome is weight (kg) after a number of weeks on diet.


## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Simulate data ---------------------------------------------------------------
set.seed(123)  # Any number may be used

# Number of participants
n <- 60   # Make it an even number to have equal sized groups
id <- 1:n # Give each a number

# Baseline weight, before diet
wpre <- rnorm(n, mean = 85, sd = 9)

# Potential outcome if on diet0
diff0 <- rnorm(n, mean = -1, sd = 3)  # diff from pre to post if on diet0
y0 <- wpre + diff0

# Potential outcome if on diet1
diff1 <- rnorm(n, mean = -2.5, sd = 3) # diff from pre to post if on diet1
y1 <- wpre + diff1

# Random assignment to diet0 or diet1, half n to each group
temp <- c(rep(0, n/2), rep(1, n/2))  # Vector with n/2 zeros and n/2 ones
treatment <- sample(temp, replace = FALSE)  # Random assignment using sample()

# Observed weight loss
y_obs <- (1 - treatment) * y0 + treatment * y1

# Add observed variables to data frame d, rounded to 1 decimal
d <- data.frame(id = id, wpre = wpre, treat = treatment, wpost = y_obs)
d <- round(d, 1)
rm(list = ls()[ls() != "d"])  # Clear the global environment, keep d

# Save data to a text-file called weight.txt
# Write data in data-frame d to a text file called "weight.txt"
write.table(d, file = "weight.txt", sep = ",", row.names = FALSE)
## .............................................................................
```

<br>

### Importing data from text file {-}

If you ran the code above, you should have a file called "weight.txt" in your 
working directory. You may use `getwd()` to check your working directory, and 
`list.files()` to list the files in that directory. Or you may look under 
the tab called "Files" in one of R-studios four panels, typically the lower right 
panel.  

[If for some reason this didn't work, you may find the data file here: 
[download data file](weight.txt)]

<br>
 
Import the data from the `weight.txt` file as if it were 
a real data file. The code below is how most of my analysis scripts starts: 

1. Remove everything to start fresh (1st code block)
2. Import data from a text-file (2nd code block)

Please copy this code into a new script-file, and save it to the same folder as 
the text-file. You may call the script `weight_analys.R`. We will continue working 
on this script in REX02 and REX03.


```r
# Simulation randomized experiment. 
# Import simulated data stored in text-file named "weight.txt". Data is 
# a simulation of a simple randomized experiment, in which 
# participants were randomized to one of two diets (diet0 or diet1), and the 
# outcome is weight (kg) after a number of weeks on diet.


## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Import data -----------------------------------------------------------------
d <- read.table("weight.txt", header = TRUE, sep = ",")
## .............................................................................
```

<br>

### Exercises {-}

Next time we will do some screening of the data. But you may start already now 
with the three exercises below, for which you need to find two 
special characters on your keyboard: `$` and `~`, remember them, you'll use them 
a lot!  

1. Explore the data in `d` with `summary(d)`
2. Check out the number of participants in the two groups, with 
`table(d$treat)`. 
3. Visualize pre-measurements with `boxplot(d$wpre ~ d$treat)`





