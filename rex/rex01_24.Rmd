# Notes REX01 {-}

```{r setup, cache = FALSE, echo = FALSE}
# This will allow error messages to be converted to HTML
knitr::opts_chunk$set(error = TRUE)
```

This seminar takes a first look at R and R-Studio. We'll
start with the Console, where you may interact directly with R. We will then
move to the text editor and start working on a data analysis that we will continue 
to work on in the following seminars. 

<br>  

## Getting started with R and R-studio {-}
Make sure you have R (the statistical software) and R-studio (an interface
to R) installed on you laptop before this seminar. You may run R without R-studio
(but not vice versa), but I strongly recommend that you use R-Studio; it makes 
working with R much easier.

## The Console {-}
You may interact directly with R through the Console (typically the 
bottom left panel in R-studio). Here are some examples, just type the commands 
and hit Enter.

### Use as calculator {-}
$27 - 13$
```{r}
27 - 13
```

$(5+3)8^{1/3}$
```{r}
(5 + 3) * 8^(1/3)
```

$7!$
```{r}
factorial(7)
```

${7 \choose 2}$
```{r}
choose(7, 2)
```

<br>  

### Get help  {-}
You may ask R for help using the `help()` function, e,g, `help(lm)` will open 
R's help-page for the `lm()` function. You may also use the short-cut is `?lm`. 
R's help-pages are very useful, but takes some time to get used to.  

If you don't understand something or want more details than is provided, just 
ask Google! R is an enormous and helpful community; 
if you have a question, it's almost certain that someone already posted it on 
line, and got several answers. AI-tools like ChatGPT and Perplexity are also 
very useful. Ask them, and they will provide you with the R-code you need!

<br> 

### Define data structures {-}
Store a set of numbers in a vector called x. (More on data structures in REX02)
```{r}
x <- c(1, 7, 7, 3, 23, -4, 2^3)
```

Note that x is not displayed in the Console unless called specifically
```{r}
x  # This is shorthand for print(x)
```

Randomly select (with replacement) 10 integers 
between 1 and 20, and store them in a vector called y
```{r}
y <- sample(seq(1, 20), size = 10, replace = TRUE)
y
```

Repeat 'red', 'green' four times and store them in a vector called condition
```{r}
condition <- rep(c('red', 'green'), 4)
condition
``` 

<br> 

### Check objects {-}
The Console is useful for checking properties of R objects, for example, the 
number of elements in a vector, or its first (or last) values.
```{r}
a <- rep(month.abb, 5)  # help(month.abb) answers your question
length(a)  # Number of elements in vector a
head(a, 7)  # The first 7 values of vector a
tail(a, 5)  # The last 5 values of vector a
```

A common mistake is to try to do things on the wrong type of data, such as
trying to do math on a character (or string) vector.
Experienced R-users often use `str()` to remind them about what
type of data they are working with (some of this information is also displayed in
the Environment panel of R-studio). 
```{r}
a + 4
str(a)
```

<br> 

### Asking the Console to produce plots works fine {-}
```{r}
random_data <- rnorm(1e4)  # 1e4 means 10000, 2e3 means 2000, ....
hist(random_data)
```

<br>   

## Script files {-}
You will do most of your typing in the script window, typically located above
the Console in R-Studio. You write blocks of code in a script file: 
select File/New File/R Script or press Ctrl+Shift+N in  R-Studio to open a new 
script. The code in the script is not executed unless you ask for it, 
for example by pressing the Run button in R-studio. If you just 
want to run a part of the script, selected this part and then hit Run. If you just 
want to run a single line, just put the cursor on the line and hit Run.

<br>

## Script: Analyze Michal & Shah (2024), Experiment 1 {-}

Today will start analyzing a data set: Experiment 1 of @michal2024practical.

![](../rmsnotes/figs/michal2024_title.PNG){width=80%}


We will continue working with this data set in the coming few REX-seminars, in 
order to learn R by EXample! 

[Follow this link to find the data file](https://bitbucket.org/matni/matni.bitbucket.org/raw/445b8080d595d70c33fa2c81e22a38f241ed3735/rmsnotes/datasets/michal2024_data_study1.txt){target="blank"} 
Save (Ctrl+S on a PC) to download as text file. 

### Organize data on your computer {-}

Before diving into analysis, it's essential to organize your workspace by creating 
directories for data and analysis scripts. A well-structured directory system 
helps keep everything related to your project in one place, making it easier to 
manage and find files.

Below is an example of how I might structure my directories:

1. Create a general directory called `myREX` (for all material related to REX). I 
put it as a sub-directory of my `Documents` directory.
2. In `myREX` create a directory called `michal24_analys`. This will be the main 
directory for this *data analysis project*.
3. Save the data file, `michal2024_data_study1.txt`, in the  `michal24_analys` directory 
(follow link above to download the data file)
4. Open a new script file in R-studio, and save it in the same directory as your data file,  
`michal2024_analys`. I called my script file `analys.R`

You will end up with a data structure that looks like this, starting from your 
Documents folder (you may of course put your data analysis projects in some other 
folder than `Documents`, this is just an example.)


```
Documents/
|
├── myREX/
|   └── michal24_analys/
|   |   ├── michal2024_data_study1.txt
|   |   └── analys.R
|

```


Finally, make sure to guide R to the data analysis project,
using the `setwd()` function. In my case, it would be 
`setwd("C:/Users/matni/Documents/myREX/michal2024_analys")`

Now we are ready to start analyzing!

### Create analysis script {-}

Please copy the text in the code chunk below into your script file, and try to run 
it. You should see one object in your Global Environment called `d`. This is the 
data frame we will be working with. Try `str(d)` to check that it looks OK. 
  
```{r, eval = FALSE}
# Analysis of data from Experiment 1 of 
#  Michal and Shah (2024), A Practical Significance Bias in Laypeople's 
#  Evaluation of Scientific Findings. Psychological Science, 35(4), 315-327.
#  https://doi.org/10.1177/09567976241231506
# 
# Data stored in comma separated txt-file: michal2024_data_study1.txt
#
# Code book:
# id -- Participant id (int)
# rating -- Rating: "On a scale from 0 (not at all) to 100 (very much), to what 
#                    extent do you think the improvement in math proficiency
#                    from completing the new math curriculum is worth the
#                    additional cost?" (int)
# condition -- Experimental condition: "LargeEffectNoPrompt",  
#              SmallEffectNoPrompt", SmallEffectWithPrompt", "NoEffectSize" (chr)
# numeracy -- Numeracy score: The total number of questions for which participants 
#             responded correctly, ranging from 0 to 8 (int)
#
# For details, see Michal and Shah (2024)
#
# Script created by MN 2024-10-17. Revised: 

 
## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Import data -----------------------------------------------------------------
d <- read.table("michal2024_data_study1.txt", header = TRUE, sep = ",")
## .............................................................................
```

<br>

## Exercise {-}

Next time we will do some screening of the data. But you may start already now 
with the three exercises below, for which you need to find two 
special characters on your keyboard: `$` and `~`, remember them, you'll use them 
a lot!  

1. Explore the data in `d` with `summary(d)`
2. Check out the number of participants in the four groups, with 
`table(d$condition)`. 
3. Visualize with `boxplot(d$rating ~ d$condition)`





