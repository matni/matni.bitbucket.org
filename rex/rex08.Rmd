# Notes REX08 {-}


```{r setup, cache = FALSE, echo = FALSE}
# This will allow error messages to be converted to HTML
knitr::opts_chunk$set(error = TRUE)
```


## Practice {-}

Today we will practice our R-skills, using the data set `kidiq.txt` 
(from @gelman2020regression), that we used back in REX06.

The data is from a survey of adult American women and their 
children. @gelman2020regression describe the data set and use it at several places, 
e.g., pp. 130-136, 156-158, 161, 185-187, and 197. 

[Follow this link to find data](https://bitbucket.org/matni/matni.bitbucket.org/raw/3dee78fc9ace615a849b5ce8cb5abd381ebb1da6/rex/kidiq.txt){target="blank"} 
Save (Ctrl+S on a PC) to download as text file

Code book:  

+ **kid_score** \ Test score children, on IQ-like measure (in the sample with 
mean and standard deviation of 87 and 20 points, respectively).
+ **mom_hs** \ Indicator variable: mother did complete high-school (1) or not (0)
+ **mom_iq** \ Test score mothers, transformed to IQ score: mean = 100, sd = 15. do   
+ **mom_work** \ Working status of mother, coded 1-4:
    - 1: mother did not work in first three years of child's life,
    - 2: mother worked in second or third year of child's life, 
    - 3: mother worked part-time in first year of child's life,
    - 4: mother worked full-time in first year of child's life. 
+ **mom_age** Age of mothers, years

The data file is comma-delimited and include variable names.

```{r}
d <- read.table("kidiq.txt", sep = ",", header = TRUE)
str(d)
```

<br>

```{r, fig.width = 6, fig.height = 2}
library(dagitty) 

kidiq <- dagitty( "dag {
   mom_hs -> kid_score
   mom_hs -> mom_work
   mom_hs <- U -> mom_age -> kid_score
   U -> mom_work -> kid_score
   U -> mom_iq -> kid_score
   U -> kid_score
}")

coordinates(kidiq) <- list(
  x = c(mom_hs = 1.5, U = 1, mom_age = 3, mom_work = 2.5, mom_iq = 2.5, kid_score = 4),
  y = c(mom_hs = 5, U = 2, mom_age = 2, mom_work = 4.2, mom_iq = 2.8, kid_score = 5))

plot(kidiq)
```

<br>

Let us use this data to estimate the total average causal effect of 
mom_hs on kid_score. The DAG above is a starting point, U is short for many 
unobserved variables, think mother's background (socioeconomic factors, genetic 
factors, etc.). According to the DAG, the total average causal effect is 
unidentified as we cannot adjust for U. However, let us proceed with the
assumption that the direct causal effect of U on kid_score is negligible. 
(*NOTE:* This is a very strong assumption indeed, 
this example is just for R-practice. Much more data on background variables, 
including longitudinal data would of course be needed to really address the 
question of the causal effect of mothers education on their kids cognitive 
abilities.) 
 

<br>

### Screening {-}

First use summary() to verify that everything looks OK and check the number of 
missing data (there are no NAs in this data set)

```{r}
summary(d)
```

<br>



<br>

Here a boxplot to inspect the main variables, kidiq ~ mom_hs (Not for publication!) 

```{r}
# Boxplot
bp <- boxplot(kid_score ~ mom_hs, data = d)

# Add individual data points
yhs0 <- d$kid_score[d$mom_hs == 0]
yhs1 <- d$kid_score[d$mom_hs == 1]
points(rep(1.1, length(yhs0)), yhs0, pch = 21, bg = "blue")
points(rep(1.9, length(yhs1)), yhs1, pch = 21, bg = "green")
```
<br>

Here I define three potential outliers based on visual inspection of the 
boxplot figure above. For robustness checks later on, 
doing analysis with these excluded

```{r}
# Values of potential outliers
hs0min <- min(d$kid_score[d$mom_hs == 0])
hs0max <- max(d$kid_score[d$mom_hs == 0])
hs1max <- max(d$kid_score[d$mom_hs == 1])

# Make indicator variable
d$outlier <- 1 * (d$kid_score <= hs0min & d$mom_hs == 0) + 
             1 * (d$kid_score >= hs0max & d$mom_hs == 0) + 
             1 * (d$kid_score >= hs1max & d$mom_hs == 1)

table(d$outlier)  # Should be three
```
<br>

Here an if-statement to allow for simple reanalysis with outliers excluded.
```{r}
remove_outliers <- 0 

if (remove_outliers == 1)  {
  d <- d[d$outlier == 0, ] # Include only rows with outlier = 0
  }
```


<br>

### Descriptive stats {-}

Let us do some descriptive statistics for the outcome (kidscore), 
separately for each level of the "exposure" (mom_hs). These might go in to
Table 1 of our paper.

```{r}
# Frequencies for mom_hs and mom_work
n <- table(mom_hs = d$mom_hs, mom_work = d$mom_work)  
nrow <- apply(n, 1, sum)  # Freq for mom_hs, same as table(mom_hs = d$mom_hs)
ncol <- apply(n, 2, sum)  # Freq for mom_work: same as table(mom_work = d$mom_work)

# Mean values for continuous variables, split by mom_hs
m <- aggregate(list(kidscore = d$kid_score, mom_age = d$mom_age, mom_iq = d$mom_iq),
               list(mom_hs = d$mom_hs), mean)
# Standard deviations for continuous variables, split by mom_hs
s <- aggregate(list(kidscore = d$kid_score, mom_age = d$mom_age, mom_iq = d$mom_iq),
               list(mom_hs = d$mom_hs), sd)

# Print to console
n
m
s
```


<br>

### Fit linear model {-}

Here we derive point estimate and compatibility intervals for a model with 
mom_hs as the only predictor (crude model). First using lm() in base R, then 
using rstanarm::stan_glm().  


**lm()**
```{r}
# Crude model using lm()
crude_lm <- lm(kid_score ~ mom_hs, data = d)
coef_lm <- coef(crude_lm)    # Point estimates
ci95lm <- confint(crude_lm)  # CI

# Put point estimate and CI in a vector
crude_est_lm <- c(b = coef_lm[2], ci95lo = ci95lm[2, 1], 
                                  ci95hi = ci95lm[2, 2])
crude_est_lm  # Print to console
```

<br>

**rstanarm::stan_glm()**. Note: The argument refresh = 0 not needed, included 
here just to skip a lot of intermediate printing to the console.

```{r, warning = FALSE, message = FALSE}
library(rstanarm)  # You may need to install rstanarm first

# Crude model using stan_glm()
crude_stan <- stan_glm(kid_score ~ mom_hs, data = d, refresh = 0)
coef_stan <- coef(crude_stan)    # Point estimates
ci95stan <- posterior_interval(crude_stan, prob = 0.95)  # CI

# Put point estimate and CI in a vector
crude_est_stan <- c(b = coef_stan[2], ci95lo = ci95stan[2, 1], 
                                      ci95hi = ci95stan[2, 2])
crude_est_stan # Print to console
```
<br>

### Plot model prediction and data {-}

Here a plot of the crude the model and data. I use output from stan_glm(). It can 
be used to draw many plausible lines (= differences in this simple model) to 
illustrate the uncertainty in our estimate (for the advanced user).

```{r, fig.width = 3, fig.height = 6}
# Empty plot
plot(d$mom_hs, d$kid_score, 
     xlim = c(-0.2, 1.2), ylim = c(10, 150),
     axes = FALSE,
     xlab = 'mom_hs', ylab = 'kidscore',
     pch = '')


# Add points
jitter <- rnorm(length(d$kid_score), mean = 0, sd = 0.01)
points(d$mom_hs + jitter, d$kid_score, pch = 21, bg = "grey")

# Add x-axis
xticks <- c(-0.2, 0, 1, 1.2)
axis(side = 1, pos = 10,
     at = xticks, labels = c("", "0", "1", ""),
     tck = 0.01, cex.axis = 0.8)

# Add y-axis
yticks <- seq(10, 150, by = 20)
axis(side = 2, pos = -0.2,
     at = yticks,
     tck = 0.01, las = 2, cex.axis = 0.8)

# Add regression line for point estimate
x <- c(0, 1)
y  <- coef_stan[1] + coef_stan[2] * x
lines(x, y, col = "blue")



# NOTE: Below is for the advanced user. Please let me know if you want me to 
# walk you through this in a separate session.

# Below code to add a set of plausible lines to the data.
ss <- as.data.frame(crude_stan) # Extract samples from the posterior

# Function to make predictions for one set of parameters (one line in samples)
sample_pred <- function(j){
  b0 <- ss[j, 1]
  b1 <- ss[j, 2]
  y <- b0 + b1 * x
  y
}

# Make predictions for x for each row in samples. 
# y_pred is a matrix, each row a prediction for values in x
y_pred <- sapply(1:nrow(ss), sample_pred)

# Select random rows from samples
rr <- sample(1:nrow(ss), size = 100, replace = FALSE)

# Plot regression lines corresponding to the selected rows of the posterior
for (j in rr){
  lines(x, y_pred[, j], col = rgb(0, 0, 1, 0.1))
}

```


<br>

### Fit and plot linear model with continous predictor {-}

Now let us look at the relationship between outcome (`kid_score`) and one of the 
potential confounders, `mom_iq` (for the moment, ignoring `mom_hs`). Again, I do 
the same thing twice, fist with `lm()` showing how a CI-band can be drawn to 
around the regression line, and then by `stan_glm()` drawing several plausible 
lines.

**lm()**: Fit model.
```{r}
# Fit model using lm()
iqlm <- lm(kid_score ~ mom_iq, data = d)
b_iqlm <- coef(iqlm)
ci95_iqlm <- confint(iqlm)

# Combine slope estimates in a vector
iqlm_est <- c(b = b_iqlm[2], ci95lo = ci95_iqlm[2, 1], 
                             ci95hi = ci95_iqlm[2, 2])
iqlm_est  # Print to console
```

<br>

**lm()**: Plot model and data
```{r}
# Plot
plot(d$kid_score ~ d$mom_iq, xlab = "Mother IQ-score", 
     ylab = "Kid score")

# x-scores for which y will be predicted using model fit
x <- seq(from = 70, to = 140, by = 1)

# predicted y-scores
y <- b_iqlm[1] + b_iqlm[2] * x

# Add predicted line
lines(x, y)

# Add a 95% CI-band around the line, using predict()
pred <- predict(iqlm, newdata = list(mom_iq = x), interval = "confidence", 
                  level = 0.95)
# Note: pred[ , 1] = main line; pred[ , 2] = lower CI; pred[ , 3] = higher CI

# CI as dashed lines
#lines(x, pred[ , 2], lty = 2)  
#lines(x, pred[ , 3], lty = 2)  

# CI as colored band
polygon(x = c(x, rev(x)), y = c(pred[ , 2], rev(pred[ , 3])),
         border = NA, col = rgb(0, 0, 1, 0.2))
```

<br>

**rstanarm::stan_glm()**: Fit model.
```{r}
# Fit model using stan_glm()
iq_stan <- stan_glm(kid_score ~ mom_iq, data = d, refresh = 0)
b_iq_stan <- coef(iq_stan)
ci95_iq_stan <- posterior_interval(iq_stan, prob = 0.95)

# Combine slope estimates in a vector
iq_stan_est <- c(b = b_iq_stan[2], ci95lo = ci95_iq_stan[2, 1], 
                             ci95hi = ci95_iq_stan[2, 2])
iq_stan_est  # Print to console
```

<br>

**rstanarm::stan_glm()**: Plot model and data
```{r}
# Plot
plot(d$kid_score ~ d$mom_iq, xlab = "Mother IQ-score", 
     ylab = "Kid score")

# x-scores for which y will be predicted using model fit
x <- seq(from = 70, to = 140, by = 1)

# predicted y-scores
y <- b_iq_stan[1] + b_iq_stan[2] * x

# Add predicted line
lines(x, y)


# NOTE: Below is for the advanced user. Please let me know if you want me to 
# walk you through this in a separate session.

# Below code to add a set of plausible lines to the data.

samples <- as.data.frame(iq_stan) # Extract samples from the posterior

# Function to make predictions for one set of parameters (one line in samples)
sample_pred <- function(j){
  b0 <- samples[j, 1]
  b1 <- samples[j, 2]
  y <- b0 + b1 * x
  y
}

# Make predictions for x for each row in samples. 
# y_pred is a matrix, each row a prediction for values in x
y_pred <- sapply(1:nrow(samples), sample_pred)

# Select random rows from samples
rr <- sample(1:nrow(samples), size = 100, replace = FALSE)

# Plot regression lines corresponding to the selected rows of the posterior
for (j in rr){
  lines(x, y_pred[, j], col = rgb(0, 0, 1, 0.1))
}
```

<br>

### All the above code, please copy to script {-}

Here is all the code above, please copy and paste into a script file. 

```{r, eval = FALSE}
## Analyses of data in kidiq.txt, see REX08 for code book

## Clear things to start clean! ------------------------------------------------
rm(list = ls())  # Clear the global environment
graphics.off()  # Clear all plots
cat("\014")  # Clear the console
## .............................................................................


## Load libraries --------------------------------------------------------------
library(rstanarm)
## .............................................................................


## Load data -------------------------------------------------------------------
d <- read.table("kidiq.txt", sep = ",", header = TRUE)
## .............................................................................


## Inspect ---------------------------------------------------------------------
str(d)
summary(d)

# Visual inspection
# Boxplot
bp <- boxplot(kid_score ~ mom_hs, data = d)

# Add individual data points
yhs0 <- d$kid_score[d$mom_hs == 0]
yhs1 <- d$kid_score[d$mom_hs == 1]
points(rep(1.1, length(yhs0)), yhs0, pch = 21, bg = "blue")
points(rep(1.9, length(yhs1)), yhs1, pch = 21, bg = "green")
## .............................................................................


## Define potential outlier ---------------------------------------------------
# Values of potential outliers
hs0min <- min(d$kid_score[d$mom_hs == 0])
hs0max <- max(d$kid_score[d$mom_hs == 0])
hs1max <- max(d$kid_score[d$mom_hs == 1])

# Make indicator variable
d$outlier <- 1 * (d$kid_score <= hs0min & d$mom_hs == 0) + 
  1 * (d$kid_score >= hs0max & d$mom_hs == 0) + 
  1 * (d$kid_score >= hs1max & d$mom_hs == 1)

table(d$outlier)  # Should be three

# Set to 1 if you want to exclude outliers in analysis below
remove_outliers <- 0 

if (remove_outliers == 1)  {
  d <- d[d$outlier == 0, ] # Include only rows with outlier = 0
}
##..............................................................................


## Descriptive stat ------------------------------------------------------------
# Frequencies for mom_hs and mom_work
n <- table(mom_hs = d$mom_hs, mom_work = d$mom_work)  
nrow <- apply(n, 1, sum)  # same as table(mom_hs = d$mom_hs)
ncol <- apply(n, 2, sum)  # same as table(mom_work = d$mom_work)

# Mean values for continuous variables, split by mom_hs
m <- aggregate(list(kidscore = d$kid_score, mom_age = d$mom_age, 
                    mom_iq = d$mom_iq),
               list(mom_hs = d$mom_hs), mean)
# Standard deviations for continuous variables, split by mom_hs
s <- aggregate(list(kidscore = d$kid_score, mom_age = d$mom_age, 
                    mom_iq = d$mom_iq),
               list(mom_hs = d$mom_hs), sd)
## .............................................................................


## Crude model using lm() ------------------------------------------------------
# Crude model using lm()
crude_lm <- lm(kid_score ~ mom_hs, data = d)
coef_lm <- coef(crude_lm)    # Point estimates
ci95lm <- confint(crude_lm)  # CI

# Put point estimate and CI in a vector
crude_est_lm <- c(b = coef_lm[2], ci95lo = ci95lm[2, 1], 
                  ci95hi = ci95lm[2, 2])
crude_est_lm  # Print to console
##..............................................................................


## Crude model using stan_glm() ------------------------------------------------
crude_stan <- stan_glm(kid_score ~ mom_hs, data = d, refresh = 0)
coef_stan <- coef(crude_stan)    # Point estimates
ci95stan <- posterior_interval(crude_stan, prob = 0.95)  # CI

# Put point estimate and CI in a vector
crude_est_stan <- c(b = coef_stan[2], ci95lo = ci95stan[2, 1], 
                    ci95hi = ci95stan[2, 2])
crude_est_stan # Print to console
##..............................................................................


## Plot of crude model ---------------------------------------------------------
# Empty plot
plot(d$mom_hs, d$kid_score, 
     xlim = c(-0.2, 1.2), ylim = c(10, 150),
     axes = FALSE,
     xlab = 'mom_hs', ylab = 'kidscore',
     pch = '')


# Add points
jitter <- rnorm(length(d$kid_score), mean = 0, sd = 0.01)
points(d$mom_hs + jitter, d$kid_score, pch = 21, bg = "grey")

# Add x-axis
xticks <- c(-0.2, 0, 1, 1.2)
axis(side = 1, pos = 10,
     at = xticks, labels = c("", "0", "1", ""),
     tck = 0.01, cex.axis = 0.8)

# Add y-axis
yticks <- seq(10, 150, by = 20)
axis(side = 2, pos = -0.2,
     at = yticks,
     tck = 0.01, las = 2, cex.axis = 0.8)

# Add regression line for point estimate
x <- c(0, 1)
y  <- coef_stan[1] + coef_stan[2] * x
lines(x, y, col = "blue")

# NOTE: Below is for the advanced user. Please let me know if you want me to 
# walk you through this in a separate session.

# Below code to add a set of plausible lines to the data.
ss <- as.data.frame(crude_stan) # Extract samples from the posterior

# Function to make predictions for one set of parameters (one line in samples)
sample_pred <- function(j){
  b0 <- ss[j, 1]
  b1 <- ss[j, 2]
  y <- b0 + b1 * x
  y
}

# Make predictions for x for each row in samples. 
# y_pred is a matrix, each row a prediction for values in x
y_pred <- sapply(1:nrow(ss), sample_pred)

# Select random rows from samples
rr <- sample(1:nrow(ss), size = 100, replace = FALSE)

# Plot regression lines corresponding to the selected rows of the posterior
for (j in rr){
  lines(x, y_pred[, j], col = rgb(0, 0, 1, 0.1))
}
##..............................................................................


## Model kid_score ~ mom_iq, using lm() ----------------------------------------
# Fit model using lm()
iqlm <- lm(kid_score ~ mom_iq, data = d)
b_iqlm <- coef(iqlm)
ci95_iqlm <- confint(iqlm)

# Combine slope estimates in a vector
iqlm_est <- c(b = b_iqlm[2], ci95lo = ci95_iqlm[2, 1], 
              ci95hi = ci95_iqlm[2, 2])
iqlm_est  # Print to console

# Plot data and model prediction
plot(d$kid_score ~ d$mom_iq, xlab = "Mother IQ-score", 
     ylab = "Kid score")

# x-scores for which y will be predicted using model fit
x <- seq(from = 70, to = 140, by = 1)

# predicted y-scores
y <- b_iqlm[1] + b_iqlm[2] * x

# Add predicted line
lines(x, y)

# Add a 95% CI-band around the line, using predict()
pred <- predict(iqlm, newdata = list(mom_iq = x), interval = "confidence", 
                level = 0.95)
# Note: pred[ , 1] = main line; pred[ , 2] = lower CI; pred[ , 3] = higher CI

# CI as dashed lines
#lines(x, pred[ , 2], lty = 2)  
#lines(x, pred[ , 3], lty = 2)  

# CI as colored band
polygon(x = c(x, rev(x)), y = c(pred[ , 2], rev(pred[ , 3])),
        border = NA, col = rgb(0, 0, 1, 0.2))
##..............................................................................


## Model kid_score ~ mom_iq, using stan_glm() ----------------------------------
# Fit model using stan_glm()
iq_stan <- stan_glm(kid_score ~ mom_iq, data = d, refresh = 0)
b_iq_stan <- coef(iq_stan)
ci95_iq_stan <- posterior_interval(iq_stan, prob = 0.95)

# Combine slope estimates in a vector
iq_stan_est <- c(b = b_iq_stan[2], ci95lo = ci95_iq_stan[2, 1], 
                 ci95hi = ci95_iq_stan[2, 2])
iq_stan_est  # Print to console

# Plot data and model predictions (many plausible lines)
plot(d$kid_score ~ d$mom_iq, xlab = "Mother IQ-score", 
     ylab = "Kid score")

# x-scores for which y will be predicted using model fit
x <- seq(from = 70, to = 140, by = 1)

# predicted y-scores
y <- b_iq_stan[1] + b_iq_stan[2] * x

# Add predicted line
lines(x, y)


# NOTE: Below is for the advanced user. Please let me know if you want me to 
# walk you through this in a separate session.

# Below code to add a set of plausible lines to the data.

samples <- as.data.frame(iq_stan) # Extract samples from the posterior

# Function to make predictions for one set of parameters (one line in samples)
sample_pred <- function(j){
  b0 <- samples[j, 1]
  b1 <- samples[j, 2]
  y <- b0 + b1 * x
  y
}

# Make predictions for x for each row in samples. 
# y_pred is a matrix, each row a prediction for values in x
y_pred <- sapply(1:nrow(samples), sample_pred)

# Select random rows from samples
rr <- sample(1:nrow(samples), size = 100, replace = FALSE)

# Plot regression lines corresponding to the selected rows of the posterior
for (j in rr){
  lines(x, y_pred[, j], col = rgb(0, 0, 1, 0.1))
}
##..............................................................................
```


<br><br>

## On request: Simulate Single-N study with ABAB design {-}

Here we will simulate data from a Single-N study where individuals were tested 
repeatedly under a control (A) and treatment (B) condition. I am thinking of 
a study that tested performance on a selective-attention test, conducted either 
after exposure to 30 min of ambient sound (control) or 30 min of nature sounds 
(treatment). Each individual was tested in one session per day for totally 30 
days, with conditions alternating each five days: 5 days ambient, 5 days  nature, 
5 days ambient, etc. Outcome on a standardized test with a mean around 50 and 
a standard deviation of around 2 units. 


```{r}
## Experimental design ---------------------------------------------------------
ndays <- 30
day <- 1:ndays  # Days
treat <- rep(c(0, 1, 0, 1, 0, 1), each = ndays/6)  # Treatment per day: ABABAB


## Below simulation for one individual called "A" ------------------------------

# Causal effects
ce <- 2 # Causal effect of treatment
timetrend <- 0.4  # Trend over time, change per doubling of days

# Potential outcomes
set.seed(123)
y0 <- rnorm(ndays, mean = 50, sd = 2) + timetrend * log2(day)
y1 <- rnorm(ndays, mean = (50 + ce), sd = 2) + timetrend * log2(day)

# Observed outcomes
y <- (1 - treat)*y0 + treat*y1

# Put in data frame
id <- rep("A", length(y))
g <- data.frame(id, day, treat, y0, y1, y)

# Plot data for individual A
ylim = c(min(y)-5, max(y)+5)
plot(day, y, type = "b", ylim = ylim, xlab = "Day", ylab = "Test score")
points(day[treat == 1], y[treat == 1], pch = 21, bg = "grey")
text(c(1, ylim[2]-2), label = "ID: A")

for (j in seq(from = 5.5, to = 25.5, by = 5)) {
  lines(c(j, j), ylim, lty = 2)  # Add vertical lines separating conditions
}

# Estimate causal effect of treat, adjusting for trend = day.
summary(lm(y ~ treat + log2(day), data = g))
```




