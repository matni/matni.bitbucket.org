data{
  int N;        // Number of observations
  vector[N] x;  // Predictor
  vector[N] y;    // Outcome 
}

parameters{
  real b0;  
  real b1;  
  real<lower=0> sigma; 
}

model{
  b0 ~ normal(50, 10);
  b1 ~ normal(0, 5);
  sigma ~ exponential(0.1);
  y ~ normal(b0 + b1 * x, sigma); 
}

generated quantities {
  vector[N] log_lik;
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(y[n] | b0 + b1 * x[n], sigma);
  }
}

