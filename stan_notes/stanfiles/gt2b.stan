data {
  int<lower=0> N;  // Number of trials
  array[N] int y;  // Indicator of success
}

parameters {
  real p;  // Probability of success
}

model {
  p ~ beta(1, 1);  // Uniform prior on interval 0,1
  y ~ bernoulli(p);  // Likelihood, vectorized function
}
