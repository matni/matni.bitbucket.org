data{
  int N;        // Number of observations
  int J;        // Number of experimental conditions
  array[N] int cond;  // Index variable, experimental conditions
  array[N] real y;    // Outcome data
}

parameters{
  vector[J] b;  // Coefficients for mu
  real<lower=0> sigma; 
}

model{
  b ~ normal(100, 5);
  sigma ~ exponential(0.1);
  y ~ normal(b[cond], sigma); 
}

