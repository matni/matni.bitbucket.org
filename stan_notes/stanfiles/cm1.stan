data {
  int<lower=1> N;       // number of stimuli 
  int<lower=0> trials;  // number of trials in total 
  array[trials] int<lower=1, upper=N> stimulus;  // presented stimuli
  array[trials] int<lower=1, upper=N> response;  // subject's responses
}

parameters {
  array[N] simplex[N] theta; // probability vectors for each stimulus
}

model {
  // Dirichlet prior for each row
  for (i in 1:N) {
    vector[N] alpha;
    for (j in 1:N) {
      alpha[j] = (i == j) ? 8.0 : 1.0;  // 8 for correct, 1 for incorrect
    }
    theta[i] ~ dirichlet(alpha);
  }
  
  // Likelihood
  for (t in 1:(trials)) {
    response[t] ~ categorical(theta[stimulus[t]]);
  }
}

generated quantities {
  // To be announced 
  }
