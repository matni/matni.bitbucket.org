data{
  int<lower=0> W;   // Number of successes
  int<lower=1> n;  // Number of trials
}     
parameters{
  real<lower=0, upper=1> p;  // Probability of success
}         
model{
  p ~ beta(1, 1); // Prior, uniform
  W ~ binomial(n, p);
}