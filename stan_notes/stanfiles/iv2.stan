data{
  int N;       // Number of observations
  int J;       // Number of conditions variable 1
  int K;       // Number of conditions variable 2
  array[N] int cond1;    // Index variable, experimental condition 1
  array[N] int cond2;    // Index variable, experimental condition 2
  array[N] real y;  // Outcome data
}

parameters{
  matrix[J, K] b;  // Coefficients for mu in JxK matrix
  real<lower=0> sigma; 
}

model{
  to_vector(b) ~ normal(100, 15); //Same prior on all b-matrix elements
  sigma ~ exponential(0.1);

 for (i in 1:N){
   real mu;
   mu = b[cond1[i], cond2[i]];
   y[i] ~ normal(mu, sigma);
 }

}

