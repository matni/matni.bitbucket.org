# MCMC

## Concepts {-}
```{block M4, type='concepts'}
+ Estimating the posterior
    - Analytic method
    - Grid approximation
    - Quadratic approximation
    - **Markov Chain Monte Carlo** (MCMC)
        + Metropolis
        + Hamiltonian (HMC)
+ `rethinking::ulam()` help us to run HMC through `Stan`
+ `rstan::stan_model(), rstan::sample()` to run `Stan` directly
+ `rstanarm::stan_glm()` for pre-compiled HMC
+ **Diagnostics**
     - Trace plot
     - neff, R

Readings:  

- @mcelreath2020statistical, Chapter 9
```

<br>

Packages used below.
```{r, echo = TRUE, message = FALSE, warning = FALSE}
library(rethinking)
library(dagitty)
library(rstan)
library(rstanarm)
library(brms)
```

## Bayesian estimation {-}

Q: What is estimated?  
A: The posterior.   

Estimation methods include: 

- **Analytic methods**. Limited to special cases (conjugate distributions)
- **Grid approximation**. Very computationally costly, practically impossible to 
use for all but the smallest models.
- **Quadratic approximation**. Efficient, but make the assumption of a gaussian posterior.
- **MCMC**. Computationally costly (it takes time!), but works for complex models with non-gaussian posteriors

<br>

**Globe-tossing data as example**

+ Data: 6 Water in 9 trials  
+ Prior: We know that there is some water and some land, but not
much more. Let us use Beta(1.5, 1.5) as prior.
```{r, fig.width = 4, fig.height = 4}
pgrid <- seq(0, 1, 0.01)
prior <- dbeta(pgrid, shape1 = 1.5, shape2 = 1.5)
plot(pgrid, prior, type = 'l', ylab = "Prior plausability", xlab = "Proportion water")
```

<br>

*Analytic solution*

```{r}
# Data
s  <- 6  # successes
n <- 9  # trials

# Distributions
prior <- dbeta(pgrid, shape1 = 1.5, shape2 = 1.5)
likelihood <- dbinom(x = s, size = n, prob = pgrid)
posterior <- dbeta(pgrid, shape1 = s + 1.5, shape2 = n - s + 1.5)

# Plot
par(mfrow = c(1, 3))
plot(pgrid, prior, type = 'l', ylab = "Prior probability density", 
     xlab = "p: proportion water")
plot(pgrid, likelihood, type = 'l', ylab = "Likelihood", 
     xlab = "p: proportion water")
plot(pgrid, posterior, type = 'l', ylab = "Posterior probability density", 
     xlab = "p: proportion water")

# Mean posterior with 95 % PI  (here analytic solutions are easily available)
a <- s + 1.5
b <- n - s + 1.5
analytic_result <- c(mean = a/(a+b), 
                    ci95lo = qbeta(0.025, shape1 = a, shape2 = b),  
                    ci95hi = qbeta(0.975, shape1 = a, shape2 = b))


# Median posterior with 95 % HID. This is the stat that I usually use, to get the
# median and HDI I sample from the posterior (as I don't know how to calculate  
# them analytically). Approximate formula for median: (a-(1/3))/(a+b-2/3)

asamples <- rbeta(1e5, shape1 = a, shape2 = b)
analytic_results2 <- c(median = median(asamples), 
                       HDI95 = HPDI(asamples, prob = 0.95))
round(analytic_results2, 2)
```



*Grid approximation*

```{r}
pgrid <- seq(0, 1, 0.001)

# Data
s  <- 6  # successes
n <- 9  # trials

# Distributions
prior <- dbeta(pgrid, shape1 = 1.5, shape2 = 1.5)
likelihood <- dbinom(x = s, size = n, prob = pgrid)
relative_posterior <- prior * likelihood

# Plot
par(mfrow = c(1, 3))
plot(pgrid, prior, type = 'l', ylab = "Prior probability density", 
     xlab = "p: proportion water")
plot(pgrid, likelihood, type = 'l', ylab = "Likelihood", 
     xlab = "p: proportion water")
plot(pgrid, relative_posterior, type = 'l', 
     ylab = "Un-normalized posterior density", xlab = "p: proportion water")

# Extract samples
grid_samples <- sample(pgrid, prob = relative_posterior, size = 1e4, replace = TRUE)
# NOTE: sample() normalizes argument prob to sum to 1

# Mean posterior with 95 % PI
grid_result <- c(median = median(grid_samples), 
                    HDI95 = HPDI(grid_samples, prob = 0.95))
round(grid_result, 2)
```

<br>

*Quadratic approximation*

```{r}
# Data
s  <- 6  # successes
n <- 9  # trials

m_quap <- quap(
  flist = alist(
    s ~ dbinom(n, p),
    p <- a,
    a ~ dbeta(1.5, 1.5)
), data = list(s = s, n = n))

# precis(m_quap, prob = 0.95)

# Plot
mu <- precis(m_quap, prob = 0.95)$mean  # posterior mean
std <- precis(m_quap, prob = 0.95)$sd # posterior sd
posterior_gaus <- dnorm(pgrid, mean = mu, sd = std)

par(mfrow = c(1, 3))
plot(pgrid, prior, type = 'l', ylab = "Prior probability density", 
     xlab = "p: proportion water")
plot(pgrid, likelihood, type = 'l', ylab = "Likelihood", 
     xlab = "p: proportion water")
plot(pgrid, posterior_gaus, type = 'l', 
     ylab = "Posterior density (normal)", xlab = "p: proportion water")


# Median posterior with 95 % HDI. Note: for normal, mu = median, 
# HDI = percentile interval
quap_result <- c(median = mu, 
                 hdi95lo = qnorm(0.025, mean = mu, sd = std),
                 hdi95hi = qnorm(0.975, mean = mu, sd = std))
round(quap_result, 2)
```

Note: We could have done better with a link-function to avoid impossible 
parameter values in the interval. But that is for coming chapters (Generlized 
Linear Models)

<br>

*MCMC* using `ulam()` and Hamiltonian Monte Carlo. Note the `refresh = 0` argument 
to skip a lot of intermediate output from the sampling.

```{r, fig.width = 8, fig.height = 5, message = FALSE, warning = FALSE}
# Data
s  <- 6  # successes
n <- 9  # trials

# Model
m <- alist(
    s ~ dbinom(n, p),
    p <- a,
    a ~ dbeta(1.5, 1.5))

# Do MCMC    
m_ulam <- ulam(flist = m, data = list(s = s, n = n), iter = 1e4, chains = 1, 
               refresh = 0)

# Show summary and traceplot
precis(m_ulam, prob = 0.95)
traceplot(m_ulam)

# Extract samples
ulam_samples <- extract.samples(m_ulam)

# Mean posterior with 95 % PI
ulam_result <- c(median = median(ulam_samples$a), 
                HDI95 = HPDI(ulam_samples$a, prob = 0.95))
round(ulam_result, 2)
```

<br>

Here the corresponding "trace-plot" for grid approximation!
```{r, fig.width = 8, fig.height = 5}
pgrid <- seq(0, 1, 0.001)
sample_no <- 1:length(pgrid)
plot(sample_no, pgrid, type = "l", xlab = "Sample number", ylab = "p")
```


## MCMC: Random walks in the posterior {-}


Goal: Estimate the posterior distribution (target distribution), $p(\theta|Data)$, where $\theta$ is one or several parameter values. We have to settle 
with relative probabilities of the posterior, that is, the unnormalized posterior distribution, $p(\theta) p(Data|\theta)$ that is proportional to the posterior.  

To do MCMC, we must first assume a model for which we can calculate the 
unnormalized posterior distribution for any value(s) of $\theta$.  

Then we use some clever algorithm for our random walk.

<br>

## Metropolis algorithm {-}

1. Start at arbitrary point for which the target distribution is non zero.
2. Propose a move to a new location in parameter space. This is done randomly, 
e.g., drawing a random location from a normal centered on the current location.
3. Decide whether to accept the new move, or to stay. This is accomplished by 
computing the ratio $p_{move} = P(\theta_{proposed})/ P(\theta_{current})$. Then 
a random number from Uniform(0, 1) is generated, and if this number is between 
0 and $p_{move}$, the move is accepted, otherwise it is rejected and we stay at 
current location.
4. Store new (or current) location in vector.
5. Repeat steps 2-4 many times.
6. Use the vector of stored locations to estimate the unstandardized posterior 
distribution, $P(\theta) P(Data|\theta)$.

<br>

Data: McElreath's Monastery example, books per day. 
```{r, fig.width = 5, fig.height = 3}
# Outcome y = number of books per day in num_days
set.seed(999)
num_days <- 30
y <- rpois(num_days, lambda = 1.5)  # True lambda = 1.5 books per day
y
mean(y)
```

<br>

**Model**

$y \sim Poisson(\lambda)$, [Likelihood] 

$\lambda \sim Gamma(3, 2)$ [Prior]


Poisson distribution (expected value = lambda)
```{r, fig.width = 8, fig.height = 3}
books <- seq(0, 12)
par(mfrow = c(1, 3))
plot(books, dpois(books, lambda = 1.2), type = 'h', main = 'lambda = 1.2', xlim = c(0, 13))
plot(books, dpois(books, lambda = 2.5), type = 'h', main = 'lambda = 2.5')
plot(books, dpois(books, lambda = 5.7), type = 'h', main = 'lambda = 5.7')
```


<br>

Prior: Gamma distribution.

+ Expected value: shape * scale
+ Mode: (shape - 1) * scale, for shape >= 1


```{r, fig.width = 5, fig.height = 3}
# Prior, Gamma distribution, in R's parametrization,
shape <- 3
scale <- 2  # or rate = 1/scale, same thing. Stan uses shape and rate parameters
plot(seq(0, 20, 0.1), dgamma(seq(0, 20, 0.1), shape = shape, scale = scale), xlab = 'Lambda', 
     ylab = 'Density prior', type = 'l', main = 'Prior')
```


<br>

Likelihood given our data (by grid approximation)
```{r, fig.width = 8, fig.height = 3}
# Our data is in vector y
lgrid <- seq(0.01, 20, by = 0.01)
loglikelihood <- sapply(lgrid, function(lgrid) sum(dpois(y, lgrid, log = TRUE)))
likelihood <- exp(loglikelihood - max(loglikelihood)) # Antilog
plot(lgrid, likelihood, type = 'l', ylab = 'Likelihood', xlab = 'Lambda')
```

```{r, fig.width = 7, fig.height = 3}
prior <- dgamma(lgrid, shape = shape, scale = scale)
posterior <- prior * likelihood
par(mfrow = c(1, 3))
plot(lgrid, prior, type = 'l')
plot(lgrid, likelihood, type = 'l')
plot(lgrid, posterior, type = 'l', ylab = "Relative posterior")
```

<br><br>

Do MCMC (Metropolis algorithm):
```{r, fig.width = 5, fig.height = 3}
set.seed(999)
# Number of samples to draw
mc <- 1e4  
# Empty vector for storing parameter values (samples from the posterior)
samples <- numeric(mc) 
# Standard deviation of rnorm() generating proposals for moving
step_size <- 0.2  

# Step 1
current <- 4  # Start value
samples[1] <- current  # Put start value first

# Steps 2-5
for (j in 2:mc) {  # Step 5, loop mc times
  
  ## Step 2
  jump <- rnorm(1, mean = 0, sd = step_size)
  proposed <- abs(current + jump)  # abs() eliminate impossible negative lambda
  
  ## Step 3
  # Calculate log(likelihood * prior) for current and proposed
  logcurrent <- sum(dpois(y, current, log = TRUE)) + 
                    dgamma(current, shape = shape, scale = scale, log = TRUE)
  logproposed <- sum(dpois(y, proposed, log = TRUE)) + 
                     dgamma(proposed, shape = shape, scale = scale, log = TRUE)
  
  # Acceptance ratio
  pmove <- exp(logproposed - logcurrent)

  # Move or stay?
  current <- ifelse(runif(1) < pmove, proposed, current)
  
  ## Step 4, store current location
  samples[j] <- current
}

# Step 6, summarize results
rethinking::dens(samples, xlab = "lambda (books per day)", show.HPDI=0.95)
poststats <- c(median = median(samples), HPDI(samples, prob = 0.95))
round(poststats, 2)

```

<br>

Compare to analytic solution (blue dashed line)
```{r, fig.width = 5, fig.height = 3}
dens(samples, xlab = "lambda (books per day)", lwd = 2)
analytic <- dgamma(lgrid, shape = shape + sum(y), rate = (1/scale + length(y)))
lines(lgrid, analytic, col = 'blue', lty = 2)
```

<br>

traceplot
```{r, fig.width = 8, fig.height = 3}
plot((1:mc)/1e3, samples, type = 'l', xlab = "Step x 1,000 ", ylab = "lambda")
```


<br>

zoom in
```{r, fig.height = 10}
# Inspect the chain
step_number <- 1:mc

par(mfrow = c(3, 1))
plot(samples[(mc-100):mc], step_number[(mc-100):mc], type = 'l', 
     main = 'Last 100', xlim = c(0.5, 5), 
     xlab = '', ylab = 'Step')
plot(samples[0.5 * ((mc-100):(mc+100))], step_number[0.5 * ((mc-100):(mc+100))], 
     type = 'l', main = 'Middle 100', xlim = c(0.5, 5), 
     xlab = '', ylab = 'Step')
plot(samples[1:100], step_number[1:100], type = 'l', main = 'First 100', 
     xlim = c(0.5, 5), xlab = 'Lambda', ylab = 'Step')
```


<br>

## Hamiltonian algorithm (using stan) {-}

```{r, fig.width = 8, fig.height = 8, warning = FALSE, message = FALSE}
# Using rethinking
model <- alist(y ~ dpois(lambda),
               lambda ~ gamma(3.0, 0.5)) 

# NOTE: lambda ~ dgamma(3, 2) didn't work with ulam(). 
# But found that lambda ~ gamma(3, 0.5). Pure Stan code, such as 
# gamma() (not dgamma as in R), is also possible to use in ulam()

ulamout <- ulam(model, data = list(y = y), chains = 4, iter = 5e3, cores = 4, 
                refresh = 0)

# This prints stan code
# stancode(ulamout)

# This is the traceplot
traceplot(ulamout)

# Extract samples lambda
ss <- rethinking::extract.samples(ulamout)$lambda

# Plot posterior
rethinking::dens(ss, xlab = "lambda (books per day)", show.HPDI=0.95)
lines(lgrid, analytic, col = 'blue', lty = 2)  # Add analytic solution

# some stats
poststats2 <- c(median = median(ss), HPDI(ss, prob = 0.95))
round(poststats2, 2)
```



<br>

Using **rstan**, and revised model code, with gamma-prior for lambda.  
Note that Stan uses shape and rate parameters (rate = 1/scale) 

```{r}
model_code <- "
data{
  int<lower = 0> N;
  int y[N];
}
  parameters{
    real<lower=0> lambda;
}
  model{
    lambda ~ gamma(3.0, 1/2.0); // Prior, 2nd parameter in Stan = 1/shape
    y ~ poisson(lambda);        // Likelihood
}
"

# Compile model 
stanmodel <- stan_model(model_code = model_code)  

# Run model
dat <- list(y = y, N = length(y))  # Put data in list
stanfit <- sampling(stanmodel, data = dat, chains = 4, 
                  iter = 2e3, warmup = 1e3, cores = 4)
print(stanfit)
```  

<br> 

Traceplot and summary of posterior
```{r, fig.width = 8, fig.height = 3}
# Traceplot
traceplot(stanfit) 

# Sample from posterior
draws <- extract(stanfit)
lambda <- as.numeric(draws$lambda)
dens(lambda, xlab = "lambda", show.HPDI = 0.95)
median(draws$lambda)
HPDI(as.numeric(draws$lambda), prob = 0.95)
```

Compare to analytic solution
```{r, fig.width = 5, fig.height = 3}
shape <- 3
scale <- 2
draws <- extract(stanfit)
dens(as.numeric(draws$lambda), xlab = "lambda", show.HPDI = 0.95)
analytic <- dgamma(lgrid, shape = shape + sum(y), rate = (1/scale + length(y)))
lines(lgrid, analytic, col = 'blue')
```

<br>


## Made simpler: rstanarm {-}

Here a multiple regression model (interaction model from exercise 8H4), using 
rstanarm() for a quick fit. Note that rstanarm uses default priors. These can be 
changed, see details: `help(stan_glm)`.    

and then using `rethinking::ulam()` and `rstan::sample()` to compare. 

First load data from exercise 8H4 on language diversity and mean growing season.
Note that the outcome is a count variable, so Poisson or similar count-variable 
model may be considered!

```{r}
data(nettle)
d <- nettle

# Outcome, z-score transform of log(languages per 1000 inhabitants)
d$lang.per.cap <- log(d$num.lang / d$k.pop) 
d$lang.per.cap_z <- (d$lang.per.cap - mean(d$lang.per.cap))/sd(d$lang.per.cap)

# Center predictors (to keep unit to months)
d$mean_season_c <- d$mean.growing.season - mean(d$mean.growing.season)
d$sd_season_c <- d$sd.growing.season - mean(d$sd.growing.season)

# Put data in a list (there was no missing data in d)
dat <- data.frame(y = d$lang.per.cap_z, 
            mean_season = d$mean_season_c,
            sd_season  =  d$sd_season_c)
```

<br>

Run `rstanarm::stan_glm()`
```{r}
m <- stan_glm(y ~ mean_season + sd_season + mean_season*sd_season, 
              data = dat, refresh = 0)
print(m, digits = 2)

# Extract samples
ss <- as.matrix(m)

# Plot and summarize posterior for mean_season (increase log(y) per month, 
# at sd = to mean)
rethinking::dens(ss[, 2], show.HPDI = 0.95)
out_stat <- c(median = median(ss[, 2]),
              hdi95 = HPDI(ss[, 2], prob = 0.95))
round(out_stat, 2)
```
<br>

Here a useful way of evaluating model fit. This type of diagnostic plots have 
become popular in the Bayesian community. The idea is simple: If your model make 
sense, it should be able to simulate data that looks like your observed data (that 
you used to update your prior model). We will do this below, from scratch using 
the function rethinking::sim().

```{r}
pp_check(m)
```

<br>



Compare with `rethinking::ulam()`
```{r, warning=FALSE, message=FALSE}

mspec <- alist(
  y ~ dnorm(mu, sigma),
  mu <- b0 + b1 * mean_season + b2 * sd_season + b3*mean_season*sd_season,
  b0 ~ dnorm(0, 0.2), 
  c(b1, b2, b3) ~ dnorm(0, 1), 
  sigma ~ dexp(1)
)

m_ulam <- ulam(mspec, data = dat, refresh = 0, chains = 4, 
                  iter = 2e3, warmup = 1e3, cores = 4)
precis(m_ulam, digits = 2)
```
<br>
 
Do diagnostic plot, using rethinking::sim()
 
```{r}
set.seed(999)
ss <- data.frame(extract.samples(m_ulam))  # parameter samples
rr <- sim(m_ulam, post = ss)  # simulated observations

# Empty plot
plot(NULL, xlim = c(-4, 4), ylim = c(0, 0.6), 
     main = "", xlab = "log.lang.per.cap [std]", ylab = "density")

# Plot kernel density curves
rrow <- sample(1:dim(rr)[1], size = 100)  # Random rows from rr

for (j in rrow) {
  lines(density(rr[j, ]), col = rgb(1, 0, 0, 0.2))
}

lines(density(dat$y), lwd = 2)  # Make data density more visible
```
 
<br>

Same as above, but without sim(), just to make sure I understand what is going on!
```{r}
set.seed(999)

# Function that simulates one new data set, from predictors
# ss is data frame with extracted samples, created above
ysim <- function(j) {
  pp <- ss[j, ]  # Select one set of parameters from ss
  mu <- pp$b0 + pp$b1*dat$mean_season + pp$b2*dat$sd_season +
                  pp$b3*dat$mean_season*dat$sd_season
  yrep <- rnorm(length(mu), mean = mu, sd = pp$sigma)
  
}

# Empty plot
plot(NULL, xlim = c(-4, 4), ylim = c(0, 0.6), 
     main = "", xlab = "log.lang.per.cap [std]", ylab = "density")

# rrow = random rows from ss, rrow created above
for (j in rrow) {
  lines(density(ysim(j = j)), col = rgb(1, 0, 0, 0.2))
}

lines(density(dat$y), lwd = 2)  # Make data density more visible
```


<br>

Here the model directly in `Stan` using `rstan`.
```{r lastcode}
m_stan_code <- "
data{
    int<lower = 0> N;
    vector[N] y;
    vector[N] sd_season;
    vector[N] mean_season;
}
parameters{
    real b0;
    real b3;
    real b2;
    real b1;
    real<lower=0> sigma;  // sigma cannot be negative
}
model{
    vector[N] mu;
    sigma ~ exponential(1);
    b1 ~ normal(0, 1);
    b2 ~ normal(0, 1);
    b3 ~ normal(0, 1);
    b0 ~ normal(0, 0.2);
    
    for (i in 1:N) {
        mu[i] = b0 + b1 * mean_season[i] + b2 * sd_season[i] + 
                b3 * mean_season[i] * sd_season[i];
    }
    
    y ~ normal(mu , sigma);
}
"

# Compile model 
m_stan <- stan_model(model_code = m_stan_code)  

# Compile data in a list
datlist <- list(
            N = length(d$lang.per.cap_z),
            y = d$lang.per.cap_z, 
            mean_season = d$mean_season_c,
            sd_season  =  d$sd_season_c)

# Run Stan
m_stanfit <- sampling(m_stan, data = datlist, chains = 4, 
                  iter = 2e3, warmup = 1e3, cores = 4)
print(m_stanfit, digits = 2)
```

<br>

... brms under construction ...

And finally, same model using using brms::brm. It takes longer time than rstanarm::stan_glm, because 
the Stan model has first to be compiled (once that is done, it goes fast for 
this simple model).

```{r}
m_brms <- brm(y ~ mean_season + sd_season + mean_season*sd_season, 
              data = dat, refresh = 0)
print(m_brms)
```