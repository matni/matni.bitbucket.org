# Models with memory

## Concepts {.unnumbered}

```{block M13, type='concepts'}
+ **Multilevel model** (MLM), also known as Hierarchical model, Mixed effects model, 
Random effects model, Varying effects model, ...
+ **Partial pooling**, the main idea of MLMs, versus 
    - Complete pooling: Ignore levels of data 
    - No pooling: Treat each level as a separate data set 
+ **Shrinkage** 
+ **Hyperparameter**
+ **Varying intercepts**
+ **Centered versus non-centered priors**

Readings:  
- @mcelreath2020statistical, Chapter 13
```

<br>

Packages used below.

```{r, echo = TRUE, message = FALSE, warning = FALSE}
library(rethinking)
library(dagitty)
library(rstan)
library(rstanarm)
library(MASS)

# Below for code taken from Bayes Rules! Spottily example
library(bayesrules)
library(tidyverse)
library(bayesplot)
library(tidybayes)
library(broom.mixed)
library(forcats)
```

Load precalculated data

```{r}
load("13_precalc.Rdata")  # Contain ulam() and stan_lmer() outputs
```

<br>

## Why Multi-level modelling? {.unnumbered}

Multilevel models, also known as Hierarchical model, Mixed effects model, Random effects model, Varying effects model, ... are models that "... remember features of each cluster in the data as they learn about all of the clusters. Depending upon the variation among clusters, which is learned from the data as well, the model pools information across clusters. This pooling tends to improve estimates about each cluster" (the Book, p. 400). This has at least four benefits:

1.  *Improved estimates for repeat sampling*.
2.  *Improved estimates for imbalance in sampling*.
3.  *Estimates of variation*.
4.  *Avoid averaging, retain variation*.

The Book: "When it comes to regression, multilevel regression deserves to be the default approach" (@mcelreath2020statistical, p. 400). But as we shall s MLM comes with the price of complexity and thereby lack of transparency compared to simpler methods. I would advice to sample a lot of data from each unit, to be sure that separate models for each individual (No pooling approach, see below) yield the same same as estimates from one large MLM (partial pooling).

Remember @wilkinson99apa

```{block C13a, type='citations'}
**Choosing a minimally sufficient analysis.**
*... Do not choose an analytic method to impress your readers or to deflect*
*criticism. If the assumptions and strength of a simpler method are reasonable* 
*for your data and research problem, use it. Occam's razor applies to methods as* 
*well as to theories.*  
(Wilkinson et al., 1999, p. 598)
```

<br>

```{r echo = FALSE, out.width = '70%', fig.cap = "Tirado et al., 2021, Fig. 2"}

knitr::include_graphics("figs/tirado2021_fig2.PNG")
                       
```

<BR>

```{r echo = FALSE, out.width = '70%', fig.cap = "Tirado et al., 2021, Fig. 3"}
knitr::include_graphics("figs/tirado_perception.PNG")
                     
```

<br>

## Pooling {.unnumbered}

Pooling is statistical jargon for averaging across groups or across observations within groups. In texts on MLM, three types of pooling is typically discussed:

-   **Complete pooling**, just treat all data as from one group and ignore any clusters or levels of data. This may be obviously wrong if we fit a model that assumes independent observations to data with repeated observation from individuals. At other times, it may be less obvious, for instance averaging over observations of a sample of individuals from different cities (should city be treated as a level?). When clusters differ, this approach will *underfit* compared to a model that take clusters into account.
-   **No pooling**, treat each cluster separately. For example, in a within subject experiment, run a separate analysis for each participant. This is like saying that the performance of participant *i* is of no help in updating my beliefs about participant *j*. Prone to *overfitting*, especially in clusters with few observations.
-   **Partial pooling**. This is the main idea of MLM. Treat clusters separately, but let estimates of observations in one cluster be informed by observations in other clusters. For example, if an observation in cluster *k* is largest, not only in its own cluster, but also compared to observations in other clusters, it is probably overestimated. Partial pooling will shrink it towards a more reasonable estimate. In this way, MLM strikes a balance between underfitting and overfitting.

<br>

Note 1: Be aware that *Complete pooling* may be calculated in two ways that not necessarily are the same. With different sample sizes in clusters, an average of all data may not be the same as the average of cluster averages. This is for example the case for the tadpole example. The average proportion of survival in 48 tanks (each with a different number of tadpoles) is about 0.72, whereas the proportion of tadpoles that survived was about 0.70 of the 1,120 tadpoles in the experiment (see below for details on the data).

Note 2: Be aware that *No pooling* may be defined in slightly different ways.

1.  Separate analysis of data in separate clusters. If done using regularizing priors, these estimates may not be identical to the observed data.
2.  One model, with cluster as "fixed effect". May involve complete pooling of some parameters, for example $\sigma$ in a Normal regression, with separate $\mu_i$ for each group.
3.  Just the observed mean. This is how McElreath's use the term in his Tadpole example, see for instance Section 13.2.4 where observed (simulated) proportions represent no pooling (blue points in Fig, 13.3).

<br>

## Tadpole example {.unnumbered}

This is data from Experiment 1 of Vonesh and Bolker (2005). Compensatory larval responses shift trade-offs associated with predator-induced hatching plasticity. Ecology 86:1580-1591.

Three conditions in a factorial 3x2x2 design, with four tanks per cell:

-   **density**: 10, 25, or 35 tadpoles per tank
-   **pred**: Aquatic predators present or absent
-   **size**: small or large tadpoles

In total, 48 tanks, 4 of each of 3 (density) x 2 (pred) x 2 (size). Outcome is survival, that is, number of tadpoles surviving.

<br>

This was a controlled experiment, so the DAG is straight forward.

```{r, echo = FALSE, fig.width = 6, fig.height = 2}
tpole <- dagitty( "dag {
   Tank -> Surv <- Dens
   Pred -> Surv <- Size
   }")

coordinates(tpole) <- list(
  x = c(Tank = 1.5, Surv = 2, Dens = 1.5, Pred = 2, Size = 2.5),
  y = c(Tank = 1, Surv = 1, Dens = 1.5, Pred = 2, Size = 1.5))

plot(tpole)
```

<br>

Get tadpole data

```{r Rcode 13.1}
# Rcode 13.1
data(reedfrogs)
d <- reedfrogs
str(d)

# Make the tank index variable
d$tank <- 1:nrow(d)
```

<br>

Some stats,

```{r}
mystat <- function(x, digits = 2){
  out <- x
  out <- round(out, digits)
  out
}

stats <- aggregate(list(psurv = d$propsurv), 
                   list(density = d$density, pred = d$pred, size = d$size), 
                   mystat)
stats
```

<br>

Below three models illustrating the three varieties of pooling

-   Model m13.0 (Complete pooling)\
-   Model m13.1 (No pooling)
-   Model m13.2 (Partial pooling, i.e. MLM)

These models ignore predictors (except tank), and just look at separate intercepts (survival proportion) for each of the 48 tanks. Note that I used a slightly wider prior for $\alpha$ in models m13.0 and m.13.1, N(0, 2.5) compared to the books N(0, 1.5). The book's regularizing prior will substantially shrink estimates for tanks with few tadpoles and observed proportions close to 1.0 (or 0.0).

<br>

**Complete pooling** (intercept only model)

Model notation, intercept only model.

$S_i \sim Binomial(N_i, p)$

$logit(p) = \alpha$

$\alpha \sim Normal(0, 2.5)$

<br>

Note: No need really for a link here, I used it to be consistent with the book. I could just have fitted this model:

$S_i \sim Binomial(N_i, p)$

$p \sim Beta(1, 1)$ Flat prior for $p$

<br>

```{r, warning = FALSE, message = FALSE}
# Data in a list for ulam()
dat <- list(
  S = d$surv,
  N = d$density)

# Estimate posterior (this model not in the chapter)
m13.0 <- ulam(
  alist(
    S ~ dbinom(N, p),
    logit(p) <- a,
    a ~ dnorm(0, 2.5)  # Wider prior than McE
  ), data = dat , chains = 4 , log_lik = TRUE, refresh = 0)

precis(m13.0, depth = 2)
#ss0 <- extract.samples(m13.0)
#ss0$p <- plogis(ss0$a)
#dens(ss0$p)
ss0 <- extract.samples(m13.0)
```

<br>

**No pooling model** (Rcode 13.2)

Model notation in McElreath (p. 402).

$S_i \sim Binomial(N_i, p_i)$

$logit(p_i) = \alpha_{TANK[i]}$

$\alpha_j \sim Normal(0, 2.5) \ \ \ \ for \ j = 1..48$

```{r Rcode13.2, warning = FALSE, message = FALSE}
# Data in a list for ulam()
dat <- list(
  S = d$surv,
  N = d$density,
  tank = d$tank)

# Estimate posterior
m13.1 <- ulam(
  alist(
    S ~ dbinom(N, p),
    logit(p) <- a[tank],
    a[tank] ~ dnorm(0, 2.5)  # Wider prior than McE
  ), data = dat , chains = 4 , log_lik = TRUE, refresh = 0)

# precis(m13.1, depth = 2)
ss1 <- extract.samples(m13.1)
```

<br>

**Partial pooling**

Model notation in McElreath (p. 403).

$S_i \sim Binomial(N_i, p_i)$

$logit(p_i) = \alpha_{TANK[i]}$

$\alpha_j \sim Normal(\bar{\alpha}, \sigma) \ \ \ for \ j = 1..48$

$\bar{\alpha} \sim Normal(0, 1.5)$

$\sigma \sim Exponential(1)$

```{r Rcode13.3, warning = FALSE, message = FALSE}
m13.2 <- ulam(
  alist(
    S ~ dbinom(N, p),
    logit(p) <- a[tank],
    a[tank] ~ dnorm(a_bar, sigma),
    a_bar ~ dnorm(0, 1.5),
    sigma ~ dexp( 1 )
  ), data = dat, chains = 4, log_lik = TRUE, refresh = 0)

# precis(m13.2, depth = 2)
ss2 <- extract.samples(m13.2)
```

<br>

Here posterior estimates versus observed data, see Figure 13.1 in the book.

```{r}
# Rcode 13.5 with minor adjustments

# Display raw proportions surviving in each tank (No pooling of a kind)
plot(d$propsurv , ylim = c(0, 1) , pch = 16 , xaxt = "n" ,
      xlab = "tank" , ylab = "proportion survival" , col=rangi2)
axis(1, at = c(1, 16, 32, 48), labels = c(1, 16, 32,48))

# Overlay posterior medians from complete, partial and no pooling estimates
#abline(h = plogis(median(ss0$a)), lty = 2, col = "red") # Complete pooling, m13.0
d$propsurvest1 <- plogis(apply(ss1$a,2, median)) # No pooling, m13.1
#points(d$propsurvest1, pch = "+", col = "red")  # No pooling
d$propsurvest2 <- plogis(apply(ss2$a,2, median)) # Partial pooling (MLM)
points(d$propsurvest2)  # Partial pooling (MLM)
abline(h = median(plogis(ss2$a_bar)), lty = 2, col = "black")  # a_bar from MLM

# Draw vertical dividers between tank densities
abline( v=16.5 , lwd=0.5 )
abline( v=32.5 , lwd=0.5 )
text( 8 , 0 , "small tanks" )
text( 16+8 , 0 , "medium tanks" )
text( 32+8 , 0 , "large tanks" )
```

<br>

Here again with two additions: (1) Complete pooling estimate from m13.0 
(red horizontal line) and (2) partial pooling estimates from m13.1 (red pluses).

```{r}
# Rcode 13.5 with some adjustments

# Display raw proportions surviving in each tank (No pooling of a kind)
plot(d$propsurv , ylim = c(0, 1) , pch = 16 , xaxt = "n" ,
      xlab = "tank" , ylab = "proportion survival" , col=rangi2)
axis(1, at = c(1, 16, 32, 48), labels = c(1, 16, 32,48))

# Overlay posterior medians from complete, partial and no pooling estimates
abline(h = plogis(median(ss0$a)), lty = 2, col = "red") # Complete pooling, m13.0
d$propsurvest1 <- plogis(apply(ss1$a,2, median)) # No pooling, m13.1
points(d$propsurvest1, pch = "+", col = "red")  # No pooling
d$propsurvest2 <- plogis(apply(ss2$a,2, median)) # Partial pooling (MLM)
points(d$propsurvest2)  # Partial pooling (MLM)
abline(h = median(plogis(ss2$a_bar)), lty = 2, col = "black")  # a_bar from MLM

# Draw vertical dividers between tank densities
abline( v=16.5 , lwd=0.5 )
abline( v=32.5 , lwd=0.5 )
text( 8 , 0 , "small tanks" )
text( 16+8 , 0 , "medium tanks" )
text( 32+8 , 0 , "large tanks" )
```

<br>

Here is the same partial pooling fit using `rstanarm::stan_lmer()`. Note that it 
uses a parametrization where individual estimates are deviations from the 
global parameter.   

$S_i \sim Binomial(N_i, p_i)$

$logit(p_i) = \alpha + \beta_{TANK[i]}$

$\alpha \sim Normal(0, 2.5)$

$\beta_j \sim Normal(0, \sigma)\ \ \ for \ j = 1..48$

$\sigma \sim Exponential(1)$

<br>

To do binomial regression in `rstanarm::stan_lmer()`, the 
outcome variable is specified as a nx2 matrix with successes and failures in the 
first and second column, respectively. 

```{r, warning=FALSE, message=FALSE}
# Make data frame
tp <- data.frame(dat)  # Make data frame

# Fit model, with default priors. Note the outcome: cbind(S, N-S)
tpfit <- stan_glmer(cbind(S, N-S) ~ 1 + (1|tank), 
                    family = binomial(link = "logit"),
                    data = tp, refresh = 0)

# Check default priors. Note that prior of sigma is defined by two parameters
# (shape and scale) of the Gamma distribution, and that Gamma(1,1) is the same as 
# Exp(1)
prior_summary(tpfit)

# Print output, show level 1 parameters
print(tpfit)
```

Note. $\sigma \sim Exponential(1)$ correspond to `shape = 1, scale = 1` 
(Gamma(1, 1) is equivalent to Exp(1)).

<br>

Here a plot that compares the 
`stan_lmer()` and `ulam()` model fits, practically identical (as expected)

```{r}
# Extract samples
sstp <- as.data.frame(tpfit)

# This strange code just adds the global parameter in sstp[, 1] to each individual, 
# before calculating mdn(ind_para). This "propagates" uncertainty in the global parameter 
# before estimating the individual mean
tp_indpara  <- apply(sstp[, 2:49], 2, function(x) sstp[, 1] + x)

# Median posterior for each id
#tpind <- apply(sstp[, 2:49], 2, median) + median(sstp[, 1])  # Wrong: No "propagation"
tpind <- apply(tp_indpara, 2, median)

# Compare to estimates from estimates above, non-centered model m13.2
s2ind <- apply(ss2$a,2, median)

# Plot on log-odds scale (left) and probability scale (right)
par(mfrow = c(1, 2))
plot(s2ind, tpind, xlim = c(-4, 4), ylim = c(-4, 4))
abline(0, 1)
plot(plogis(s2ind), plogis(tpind), xlim = c(0, 1), ylim = c(0, 1))
abline(0, 1)
```

<br>

## Running example {.unnumbered}

Here example from Ch. 15 of the *Bayes Rules!* book. Data is 185 running times (minutes) in an annual 10-mile race, from 36 runners that participated between 4 and 7 times. The runners are in their 50's and 60's and participated in multiple years. Below analysis using the rstanarm package.

<br>

Load data

```{r}
data("cherry_blossom_sample")
g <- as.data.frame(cherry_blossom_sample)
g <- g[complete.cases(g), c("runner", "age", "net")]
g$runner <- as.integer(g$runner)
g$age_c <- g$age - 55  # Center at 55 year
```

<br>

**Complete pooling**

Default priors

```{r}
g0 <- stan_glm(net ~ age_c, data = g, refresh = 0)
print(g0)

# Check default priors
prior_summary(g0)
```

<br>

User-specified priors

```{r}
g0 <- stan_glm(net ~ age_c, data = g, 
               prior_intercept = normal(90, 20),
               prior = normal(0, 5),
               prior_aux = exponential(0.05),
               refresh = 0)
print(g0)

# Check default priors
prior_summary(g0)
```

<br>

Extract samples, summarize posterior and plot ala McElreath

```{r}
# Extract samples
ss0 <- as.data.frame(g0)
colnames(ss0) <- c("b0", "age_c", "sigma")
head(ss0)

# Link by hand, see the Book, R.code 4.58
mu.link <- function(age_c) ss0$b0 + ss0$age_c * age_c
xx <- seq(-5, 6)
mu0 <- sapply(xx, mu.link)  # Many plausible means, for each of the values in xx
mu0_mdn <- apply(mu0, 2, median)
mu0_ci <- apply(mu0, 2, HPDI, prob = 0.9)

# Plot, no un-centering by adding 55 y
plot(g$age_c + 55, g$net, ylab = "Running time [min]", xlab = "Age [y]")

# Add lines
lines(xx + 55, mu0_mdn)
shade(mu0_ci, xx + 55)
```

<br>

**No pooling**

No pooling, by estimating a separate model for each participant. Below plot for 9 runners

```{r, warning=FALSE, message=FALSE}
ind <- function(id, ylim = c(60, 130)) {
  gg <- g[g$runner == id, ]
  gg0 <- stan_glm(net ~ age_c, data = gg, 
               prior_intercept = normal(90, 20),
               prior = normal(0, 5),
               prior_aux = exponential(0.05),
               refresh = 0)
  # Extract samples
  ss0 <- as.data.frame(gg0)
  colnames(ss0) <- c("b0", "age_c", "sigma")
  head(ss0)
  
  # Link by hand, see the Book, R.code 4.58
  mu.link <- function(age_c) ss0$b0 + ss0$age_c * age_c
  xx <- seq(-5, 6)
  mu0 <- sapply(xx, mu.link)  # Many plausible means, for each of the values in xx
  mu0_mdn <- apply(mu0, 2, median)
  mu0_ci <- apply(mu0, 2, HPDI, prob = 0.9)
  
  # Plot, no un-centering by adding 55 y
  plot(gg$age_c + 55, gg$net, ylab = "Running time [min]", xlab = "Age [y]",
       xlim = c(50, 61), ylim = ylim, pch = 21, bg = "darkgrey")
  
  # Add lines
  lines(xx + 55, mu0_mdn)
  shade(mu0_ci, xx + 55)
  
  lines(xx + 55, 90 + xx*0.3, col = "blue", lty = 2)  # Complete pool line
  
  # Add runner id on top
  mtext(side = 3, text = paste("Runner ", id), cex = 0.7)
}

# Show random 9
par(mfrow = c(3, 3), mar = c(3, 3, 2, 0))
set.seed(123)
inds <- sort(sample(g$runner, 9, replace = FALSE))
invisible(sapply(inds, ind))  # invisible() to skip some printing
```

<br>

Here for the three runners highlighted in Ch 15 of Bayes Rules!

```{r}
# Show three specific runners (from book, ch 15), with no pool estimate (blue line)
par(mfrow = c(1, 3))
ind(1, c(60, 160))
ind(20, c(60, 160))
ind(22, c(60, 160))
```

<br>

Here plot with all types of pooling for three selected runners

```{r, eval = FALSE, warning=FALSE, message=FALSE}
# This is for later when we discuss varying intercept and slope models
g2 <- stan_glmer(net ~ age_c + (age_c | runner), data = g, refresh = 0,
           prior_intercept = normal(90, 20),
           prior = normal(0, 5),
           prior_aux = exponential(0.05),
           prior_covariance = decov(reg = 1, conc = 1, shape = 1, scale = 1))

```

```{r}
indcf <- coef(g2)  # Individual estimates from partial-pooling model

# Show three specific runners (from book, ch 15), with no pool estimate (blue line)
par(mfrow = c(1, 3))
ind(1, c(60, 160))
lines(xx + 55, indcf$runner[1, 1] + indcf$runner[1, 2]*xx, 
      col = "red", lty = 3) 
ind(20, c(60, 160))
lines(xx + 55, indcf$runner[20, 1] + indcf$runner[20, 2]*xx, 
      col = "red", lty = 3) 
ind(22, c(60, 160))
lines(xx + 55, indcf$runner[22, 1] + indcf$runner[22, 2]*xx, 
      col = "red", lty = 3) 
```

<br>

## Simulating varying effects {.unnumbered}

Here McElreath's tadpole simulation, Section 13.2.

$S_i \sim Binomial(N_i, p_i)$

$logit(p_i) = \alpha_{POND[i]}$

$\alpha_j \sim Normal(\bar{\alpha}, \sigma)$

$\bar{\alpha} \sim Normal(0, 1.5)$

$\sigma \sim Exponential(1)$

For simulation, we need to assign value to:

-   $\bar{\alpha}$, the average log-odds of survival in the entire population of ponds
-   $\sigma$, the standard deviation of the distribution of log-odds of survival among ponds
-   $\alpha$, a vector of individual pond intercepts, one for each pond

<br>

Set simulation parameters
```{r}
a_bar <- 1.5
sigma <- 1.5
nponds <- 60
ni <- as.integer(rep(c(5, 10, 25, 35), each = 15))
```

<br>

Simulate:  

(a) true ("unobserved") values per pond, using `rnorm()`, and 
(b) "observed" data, using `rbinom()`.

```{r}
set.seed(5005)

# Simulate true values
true_a <- rnorm(nponds, mean = a_bar, sd = sigma)

# Make data frame to keep everything we need in one place
dsim <- data.frame(pond = 1:nponds, ni = ni, true_a = true_a) 
dsim$true_p <- plogis(dsim$true_a)

# Simulate observed values and add to data frame
dsim$si <- rbinom(nponds, size = dsim$ni, prob = dsim$true_p)

head(dsim)
```

<br>

No-pooling estimate:

```{r}
dsim$p_nopol <- dsim$si/dsim$ni
```

"These are the same no-pooling estimates you'd get by fitting a model with a 
dummy variable for each pond and flat priors to induce no regularization" 
(p. 411). 

<br>

Partial pooling estimate:

```{r, eval = FALSE, warning=FALSE, message=FALSE}
# Data
dat <- list(si = dsim$si, ni = dsim$ni, pond = dsim$pond)

# Fit partial pooling model

m13.3 <- ulam(
  alist(
    si ~ dbinom(ni, p),
    logit(p) <- a_pond[pond],
    a_pond[pond] ~ dnorm(a_bar, sigma),
    a_bar ~ dnorm(0, 1.5),
    sigma ~ dexp(1)
    ), 
  data = dat, chains = 4, refresh = 0
)
```

<br>

Extract samples and look at predicted survival proportion per pond
```{r}
post <- extract.samples(m13.3)
dsim$p_partpool <- apply(plogis(post$a_pond), 2, median)
head(dsim)
```

<br>

Here plot similar to the Book's Fig. 13.1
```{r}
plot(dsim$pond, dsim$p_nopol, pch = 21, bg = "blue", xlab = "Pond ID", 
     ylab = "Estimated Survival Probability")
points(dsim$pond, dsim$p_partpool, pch = 21)
# points(dsim$pond, dsim$true_p, pch = "X")

pbar <- plogis(median(post$a_bar))
lines(c(0, 61), c(pbar, pbar), col = "red", lty = 3)
lines(c(15.5, 15.5), c(0, 1.1), col = "grey")
lines(c(30.5, 30.5), c(0, 1.1), col = "grey")
lines(c(45.5, 45.5), c(0, 1.1), col = "grey")
```

<br>

And here as Fig. 13.3, with absolute error re true value.

```{r}
# Calculate errors
nopool_error <- abs(dsim$p_nopol-dsim$true_p)
partpool_error <- abs(dsim$p_partpool-dsim$true_p)

# Average error per pond-size
error_avg <- aggregate(list(nopool_error = nopool_error), 
                        list(pondsize = dsim$ni), mean)
error_avg$partpool_error <- aggregate(partpool_error, list(dsim$ni), mean)[, 2]
round(error_avg, 2)

# Plot
plot(dsim$pond, nopool_error, pch = 21, bg = "blue", xlab = "Pond ID", 
     ylab = "Absolute error")
points(dsim$pond, partpool_error, pch = 21, bg = "white")
lines(c(0, 61), c(pbar, pbar), col = "red", lty = 3)
lines(c(15.5, 15.5), c(0, 1.1), col = "grey")
lines(c(30.5, 30.5), c(0, 1.1), col = "grey")
lines(c(45.5, 45.5), c(0, 1.1), col = "grey")

# Add lines for average error
for (j in 0:3) {
  lines(c(1 + 15*j, 14 + 15*j), c(error_avg[j+1, 2], error_avg[j+1, 2]), col = "blue" )
  lines(c(1 + 15*j, 14 + 15*j), c(error_avg[j+1, 3], error_avg[j+1, 3]), lty = 2)
}
```


## Chimpanzee example {.unnumbered}

<br>

This data from an experiment on prosocial tendencies in chimpanzees, from an 
article that gives the main result "Chimpanzees are indifferent to the welfare of 
unrelated group members" (Silk et al. 2005. Nature 437:1357-1359). 

Experimental setup (McElreath, Fig 11.2):

```{r echo = FALSE, out.width = '50%', fig.cap = ""}

knitr::include_graphics("figs/chimp.PNG")

```

Fig. 11.2 "Chimpanzee prosociality
experiment, as seen from the perspective
of the focal animal. The left and right
levers are indicated in the foreground.
Pulling either expands an accordion device
in the center, pushing the food trays
towards both ends of the table. Both food
trays close to the focal animal have food
in them. Only one of the food trays on the
other side contains food. The partner condition
means another animal, as pictured,
sits on the other end of the table. Otherwise,
the other end was empty." (McElreath, p. 326)

<br>

Get data and make data list

```{r}
data(chimpanzees)
d <- chimpanzees
```

<br>

**Code book** (see `help(chimpanzees)`)

+ **actor**: Name of actor
+ **recipient**: Name of recipient (NA for partner absent condition)
+ **condition**: Partner absent (0), partner present (1)
+ **block**: Block of trials (each actor x each recipient 1 time)
+ **trial**: Trial number (by chimp = ordinal sequence of trials for each chimp, ranges from 1-72; partner present trials were interspersed with partner absent trials)
+ **prosocial_left**: 1 if prosocial (1/1) option was on left
+ **chose_prosoc**: Choice chimp made (0 = 1/0 option, 1 = 1/1 option)
+ **pulled_left**: Which side did chimp pull (1 = left, 0 = right)

<br>

We will follow McWElreath and model `pulled_left`  as a function of 
`treatment` defined as combinations of `prosocial_left` and `condition`.

Treatment is created as an index variable with four levels:

1. `prosocial_left = 0` and `condition = 0`
2. `prosocial_left = 1` and `condition = 0`
3. `prosocial_left = 0` and `condition = 1`
4. `prosocial_left = 1` and `condition = 1`

<br>

```{r}
# Create index variable of prosoc_left x condition
d$treatment <- 1 + d$prosoc_left + 2*d$condition
```

Note. The standard way would be to build an interaction model. But it is easier 
to define priors for the index variable. Any interaction can then be evaluated 
from the posterior.

The main contrasts are defined like this (cf. R code 11.14, p. 331):  

+ `treatment == 1 - treatment == 3`, if prosocial behavior, this contrast is 
expected to be positive, that is, less left-pulls (= more right pulls) with 
prococial option on the right and partner present (`treatment == 3`) 
compared to absent (`treatment == 1`)
+ `treatment == 2 - treatment == 4`, if prosocial, this contrast is expected 
to be negative, that is, more left-pulls with prococial option on the left 
and partner present (`treatment == 4` compared to absent (`treatment == 2`)

There are other ways to define contrasts that may make even more sense, but let's 
stick to McElreath's approach.

I will however calculate this one:

+ 1/2((`treatment == 1 - treatment == 3`) + (`treatment == 4 - treatment == 2`)), 
this is the average effect of partner, positive means prosocial, negative means 
anti-social.

<br>

Define data list

```{r}
dat_list <- list(
  pulled_left = d$pulled_left,
  actor = d$actor,
  block_id = d$block,
  treatment = as.integer(d$treatment))
```

<br>

Trials per actor x treatment
```{r}
# Trials per actor x treatment
table(dat_list$actor, dat_list$treatment)

```

<br>

Trials per actor x block
```{r}
# Trials per actor x block
table(dat_list$actor, dat_list$block)

```

<br>

Rows in data file
```{r}
# Number of data points
length(dat_list$actor)
```

<br>

Plot data (see code 11.15-16, p. 332 of the book)

```{r}
# Prop left for each actor 
pl <- by( d$pulled_left , list( d$actor , d$treatment ) , mean )
# pl[1,]

plot( NULL , xlim=c(1,28) , ylim=c(0,1) , xlab="" ,
     ylab="proportion left lever" , xaxt="n" , yaxt="n" )

axis( 2 , at=c(0,0.5,1) , labels=c(0,0.5,1) )
abline( h=0.5 , lty=2 )

for ( j in 1:7 ) abline( v=(j-1)*4+4.5 , lwd=0.5 )

for ( j in 1:7 ) text( (j-1)*4+2.5 , 1.1 , concat("actor ",j) , xpd=TRUE )

for ( j in (1:7)[-2] ) {
  lines( (j-1)*4+c(1,3) , pl[j,c(1,3)] , lwd=2 , col=rangi2 )
  lines( (j-1)*4+c(2,4) , pl[j,c(2,4)] , lwd=2 , col=rangi2 )
  }

points( 1:28 , t(pl) , pch=16 , col="white" , cex=1.7 )

points( 1:28 , t(pl) , pch=c(1,1,16,16) , col=rangi2 , lwd=2 )

yoff <- 0.01
text( 1 , pl[1,1]-yoff , "R/N" , pos=1 , cex=0.8 )
text( 2 , pl[1,2]+yoff , "L/N" , pos=3 , cex=0.8 )
text( 3 , pl[1,3]-yoff , "R/P" , pos=1 , cex=0.8 )
text( 4 , pl[1,4]+yoff , "L/P" , pos=3 , cex=0.8 )
```
Here just calculation of my aggregated contrasts based on observed proportions, 
positive means prosocial behavior when partner to the right (cright), to the 
left (cleft) and averaged over partner left and right (cmean).

```{r}
obs <- aggregate(list(leftpull = d$pulled_left), 
                 list(tretment = d$treatment, actor = d$actor), mean)

# Contrasts
cleft <- numeric(7)
cright <- numeric(7)
ct <- numeric(7)
for (j in 1:7) {
  yy <- obs[obs$actor == j, 3]
  cright[j] <- yy[1] - yy[3]
  cleft[j] <-  yy[4] - yy[2]                  
  ct[j] <- mean(c(cright[j], cleft[j]))
}

round(data.frame(cright, cleft, cmean = ct), 2)

```

<br>

Fixed intercept model (as the book, m11.4, p. 330 but with block added)

$L_i \sim Binomial(1, p_i)$

$logit(p_i) = \alpha_{ACTOR[i]} + \gamma_{BLOCK[i]} + \beta_{TREATMENT[i]}$

$\beta_j \sim Normal(0, 0.5) \ \ \ , for \ j = 1..4$

$\alpha \sim Normal(0, 1.5) \ \ \ , for \ j = 1..7$

$\gamma_j \sim Normal(0, 0.5) \ \ \ , for \ j = 1..6$

```{r, eval = FALSE, warning=FALSE, message=FALSE}
set.seed(13)

m13.4fixed <- ulam(
  alist(
  pulled_left ~ dbinom(1, p) ,
  logit(p) <- a[actor] + g[block_id] + b[treatment] ,
  
  ## non-adaptive priors
  b[treatment] ~ dnorm(0, 0.5),
  a[actor] ~ dnorm(0, 1.5),
  g[block_id] ~ dnorm(0, 0.5)
  
), data=dat_list , chains=4 , cores=4 , log_lik=TRUE, refresh = 0)

```

<br>

Summarize

```{r}
precis(m13.4fixed , depth = 2)
ss <- extract.samples(m13.4fixed)
```

<br>

Plot ala McElreath, with observed proportions (blue) and estimates with CI (black), 
separately for each actor. 


```{r}
## Compute mean for each actor in each treatment (to plot raw data)
pl <- by(d$pulled_left, list(d$actor, d$treatment), mean)

## Generate data for link
datp <- expand.grid(treatment=rep(1:4),  actor=rep(1:7), block_id=rep(5))

## Generate posterior predictions using link, to be used in plot function
p_post <- link(m13.4fixed, data=datp)
p_mu <- apply(p_post, 2, mean)
p_ci <- apply(p_post, 2, PI)

chimpplot <- function(p_mu, p_ci) {
  ## Set up plot
  plot( NULL , xlim=c(1,28) , ylim=c(0,1) , xlab="" ,
        ylab="proportion left lever" , xaxt="n" , yaxt="n" )
  axis( 2 , at=c(0,0.5,1) , labels=c(0,0.5,1) )
  abline( h=0.5 , lty=2 )
  
  for ( j in 1:7 ) abline( v=(j-1)*4+4.5 , lwd=0.5 )
  
  for ( j in 1:7 ) text( (j-1)*4+2.5 , 1.1 , concat("actor ",j) , xpd=TRUE )
  xo <- 0.1 # offset distance to stagger raw data and predictions
  
  # Raw data
  for ( j in (1:7)[-2] ) {
    lines( (j-1)*4+c(1,3)-xo , pl[j,c(1,3)] , lwd=2 , col=rangi2 )
    lines( (j-1)*4+c(2,4)-xo , pl[j,c(2,4)] , lwd=2 , col=rangi2 )
  }
  points( 1:28-xo , t(pl) , pch=16 , col="white" , cex=1.7 )
  points( 1:28-xo , t(pl) , pch=c(1,1,16,16) , col=rangi2 , lwd=2 )
  yoff <- 0.175
  text( 1-xo , pl[1,1]-yoff , "R/N" , pos=1 , cex=0.8 )
  text( 2-xo , pl[1,2]+yoff , "L/N" , pos=3 , cex=0.8 )
  text( 3-xo , pl[1,3]-yoff , "R/P" , pos=1 , cex=0.8 )
  text( 4-xo , pl[1,4]+yoff , "L/P" , pos=3 , cex=0.8 )
  
  # posterior predictions
  for ( j in (1:7)[-2] ) {
    lines( (j-1)*4+c(1,3)+xo , p_mu[(j-1)*4+c(1,3)] , lwd=2 )
    lines( (j-1)*4+c(2,4)+xo , p_mu[(j-1)*4+c(2,4)] , lwd=2 )
  }
  
  for ( i in 1:28 ) lines( c(i,i)+xo , p_ci[,i] , lwd=1 )
  points( 1:28+xo , p_mu , pch=16 , col="white" , cex=1.3 )
  points( 1:28+xo , p_mu , pch=c(1,1,16,16) )
}

chimpplot(p_mu, p_ci)
```

<br>

Contrast plot (of treatment, group level)

```{r}
db13 <- ss$b[, 1] - ss$b[, 3]
db24 <- ss$b[, 2] - ss$b[, 4]
dens(db13, xlim = c(-1, 1), show.HPDI = 0.9, col = "blue")
dens(db24, xlim = c(-1, 1), add = TRUE, col = "red", show.HPDI = 0.9)
lines(c(0, 0), c(0, 2), lty = 2)
```

My aggregated contrast, positive means prosocial.
```{r}
psoc <- 0.5*((ss$b[, 1] - ss$b[, 3]) + (ss$b[, 4] - ss$b[, 2]))
dens(psoc, show.HPDI  = 0.9)
```


<br>

Varying intercepts, with two clusters

$L_i \sim Binomial(1, p_i)$

$logit(p_i) = \alpha_{ACTOR[i]} + \gamma_{BLOCK[i]} + \beta_{TREATMENT[i]}$

$\beta_j \sim Normal(0, 0.5) \ \ \ , for \ j = 1..4$

$\alpha_j \sim Normal(\bar{\alpha}, \sigma_{\alpha}) \ \ \ , for \ j = 1..7$

$\gamma_j \sim Normal(0, \sigma_{\gamma}) \ \ \ , for \ j = 1..6$

$\bar{\alpha} \sim Normal(0, 1.5)$

$\sigma_{\alpha} \sim Exponential(1)$

$\sigma_{\gamma} \sim Exponential(1)$

<br>

Fit varying intercept model using ulam()

```{r, eval = FALSE, warning=FALSE, message=FALSE}
set.seed(13)

m13.4 <- ulam(
  alist(
  pulled_left ~ dbinom(1, p) ,
  logit(p) <- a[actor] + g[block_id] + b[treatment] ,
  b[treatment] ~ dnorm(0, 0.5),
  
  ## adaptive priors
  a[actor] ~ dnorm(a_bar, sigma_a),
  g[block_id] ~ dnorm(0, sigma_g),
  
  ## hyper-priors
  a_bar ~ dnorm(0, 1.5),
  sigma_a ~ dexp(1),
  sigma_g ~ dexp(1)
  
), data=dat_list , chains=4 , cores=4 , log_lik=TRUE, refresh = 0)

```

```{r}
precis(m13.4 , depth = 2)
ss <- extract.samples(m13.4)
```

<br>

Plot model estimates and observed proportions (for block = 5, with estimate 
close to zero).

```{r}
#  Link for input to plot-function
p_post <- link(m13.4, data=datp)
p_mu2 <- apply(p_post, 2, mean)
p_ci2 <- apply(p_post, 2, PI)

# Plot
chimpplot(p_mu2, p_ci2)
```

<br>


Partial pooling on treatment: "Some people will scream "No!" at this suggestion, 
because they have been taught that varying effects are only for variables that 
were not experimentally controlled. Since treatment was "fixed" by the experimenter, 
the thinking goes, we should use un-pooled "fixed" effects" (p. 419, Note: they 
would then have screamed already when we added block as varying effect). Those
people presumably include the authors of Bayes Rules! (Johnson et al., 2022, 
see Ch. 16, Section 16.7 "Not everything is hierarchical"). 

Something to discussed, but meanwhile we may do it just to see what happens:

```{r, eval = FALSE, warning=FALSE, message=FALSE}
set.seed(15)

m13.6 <- ulam(
  alist(
    pulled_left ~ dbinom( 1 , p ) ,
    logit(p) <- a[actor] + g[block_id] + b[treatment] ,
    b[treatment] ~ dnorm( 0 , sigma_b ),
    a[actor] ~ dnorm( a_bar , sigma_a ),
    g[block_id] ~ dnorm( 0 , sigma_g ),
    a_bar ~ dnorm( 0 , 1.5 ),
    sigma_a ~ dexp(1),
    sigma_g ~ dexp(1),
    sigma_b ~ dexp(1)
  ), 
  data=dat_list, chains=4, cores=4, log_lik=TRUE, refresh = 0)
```

```{r}

#  Link for input to plot-function
p_post <- link(m13.6, data=datp)
p_mu3 <- apply(p_post, 2, mean)
p_ci3 <- apply(p_post, 2, PI)

# Plot
chimpplot(p_mu3, p_ci3)
```

<br>
Hyperparameters

```{r, message=FALSE, warning=FALSE}
precis(m13.4)
precis(m13.6)
```

<br>

Divergent transitions per model
```{r}
divergent(m13.4)
divergent(m13.6)
```
<br>

## Non-centered parametrization {-}

we will take this in chapter 14...

<br>

## Multi-level posterior predictions {-}

Model checking always important. One way is compare sample data 
with posterior predictions to get some clue to sanity of your model. 
"Every models is a merger of sense and nonsense. When we understand a model, we 
can find its sense and control its nonsense" (p. 426)

But with MLM we should not expect posterior predictive distribution to match 
the raw data, even when the model is working. The whole point of partial pooling 
is to shrink estimates towards the grand mean, so estimates and raw data may 
differ when applying partial pooling.

<br>

Here posterior predictions for chimp 2, using link.

```{r}
d_pred <- expand.grid(actor = 2, treatment = 1:4, block_id = 5)
p <- link(m13.4, data = d_pred)
str(p)
p_mu <- apply(p, 2, median)
round(p_mu, 2)

# By the way, HPDI include 1, whereas PI do not
p_hdi <- apply(p, 2, HPDI, prob = 0.9)
p_pi <- apply(p, 2, PI, prob = 0.9)
round(p_hdi, 3)
round(p_pi, 3)
```

<br>

Here "by hand" (to remember what is going on)

1. extract samples
```{r}
post <- extract.samples(m13.4)
str(post)
```
<br>

2. Make function that calculates p for specific values of predictors

```{r}
p_link <- function(treatment, actor = 1, block_id = 1){
  logodds <- post$a[, actor] + 
             post$g[, block_id] + 
             post$b[, treatment]
  out <- plogis(logodds)
  out
}

```

<br>

2. use function to derive posterior predictions
```{r}
p_raw <- sapply(1:4, p_link, actor = 2, block_id = 5)
p_mu <- apply(p_raw, 2, median)
p_ci <- apply(p_raw, 2, HPDI, prob = 0.9)
```

Here density plot for treatment 1
```{r}
par(mfrow = c(1, 2))
dens(p_raw[, 1], xlab = "p, actor 2, treat = 1")
dens(qlogis(p_raw[, 1]), xlab = "logodds, actor 2, treat = 1")
```

<br> 

Here posterior predictions for an average observer, with intercept at 
$\bar\alpha$:

Link function for observer at $\bar\alpha$

```{r}
# Link for average observer, ignoring block assumed to have zero effect
p_link_abar <- function(treatment) {
  logodds <- with(post, a_bar + b[,treatment])
  out <- plogis(logodds)
  out
}
```

<br>

Use this function and summarize as before.
```{r}
post <- extract.samples(m13.4)
p_raw <- sapply(1:4 , p_link_abar)
p_mu <- apply(p_raw, 2 , median )
p_ci <- apply(p_raw, 2 , HPDI, prob = 0.9 )

# Plot points and CI (using shade)
plot(NULL , xlab="treatment", ylab="proportion pulled left",
     ylim=c(0, 1), xaxt="n", xlim=c(1,4))
axis(1 , at=1:4 , labels=c("R/N","L/N","R/P","L/P"))
lines(1:4, p_mu)
shade(p_ci, 1:4)
```
<br>

This says something about the "effect". If we want expected variability across 
observers, then we need to consider variability around $\bar\alpha$, i.e., 
$\sigma_{\alpha}$. Remember the `sim` function from previous chapters

```{r}
# Simulate
a_sim <- rnorm(length(post$a_bar), post$a_bar, post$sigma_a)
p_link_asim <- function(treatment) {
     logodds <-a_sim + post$b[,treatment]
     out <- plogis(logodds)
     out
}

p_raw_asim <- sapply(1:4, p_link_asim)

# Plot lines for 100 new observers
plot(NULL , xlab="treatment" , ylab="proportion pulled left" , 
     ylim=c(0,1) , xaxt="n" , xlim=c(1,4) )
axis( 1 , at=1:4 , labels=c("R/N","L/N","R/P","L/P") )
for (i in 1:100) lines(1:4 , p_raw_asim[i,] , col = rgb(0, 1, 0, 0.1), lwd = 3 )
lines(1:4, p_mu)
```


## Chimps in rstanarm {-}

Here is model m13.4 using rstanarm::stan_glmer. (Not sure how to set prior for
a_bar ...)
```{r, message=FALSE, warning=FALSE}
# Make data frame for 
dd <- data.frame(dat_list)
dd$treatment = factor(dd$treatment)

# Fit model, with default priors. 
chimparm <- stan_glmer(pulled_left ~ 0 + treatment + (1|actor) + (1|block_id), 
                    family = binomial(link = "logit"),
                    data = dd, refresh = 0,
                    prior = normal(0, 0.5))
```

```{r}
print(chimparm)
prior_summary(chimparm)
```

<br>


<br><br><br>


(This code saves precalculated data)

```{r}
save(m13.0, m13.1, m13.2, g2, m13.3, m13.4fixed, m13.4, m13.6, chimparm,
     file = "13_precalc.Rdata")
```
