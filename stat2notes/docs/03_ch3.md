# Sampling the Imaginary

## Concepts {-}
<div class="concepts">
<ul>
<li><strong>Simulation</strong>, nothing Bayesian about simulation,
e.g., bootstrapping</li>
<li><strong>Compatibility interval</strong>
<ul>
<li>Confidence interval (frequentist)</li>
<li>Likelihood interval (likelihood theory)</li>
<li>Credible interval (Bayesian)</li>
</ul></li>
<li><strong>Sampling from the posterior</strong> to estimate the
relative posterior</li>
</ul>
<p>Readings:<br />
- <span class="citation">@mcelreath2020statistical</span>, Chapter 3</p>
</div>

## Simulation {-}

A game changer. Has become a standard tool in most areas of statistics. 
Bootstrapping ("A frequentist machine") has been a standard tool for decades, 
MCMC is now a standard tool in Bayesian analysis.

Some uses  

+ Estimate sampling distribution from data (bootstrapping),
+ Sample from posterior distributions,
+ Generate data from models, to explore model implications
+ Power analysis (in the strict as well as the broader sense of the term)
+ Problem solving, to find approximate answers to hard problems, see example below

<br>

**Expected distance between two randomly selected points in a unit square**


```r
par(mar = c(0, 0, 0, 0))
plot(c(0, 1), c(0, 1), pch = '')
lines(c(0.45, 0.73), c(0.47, 0.68))
points(c(0.45, 0.73), c(0.47, 0.68), pch = 21, bg = 'grey', cex = 0.7)
```

<img src="03_ch3_files/figure-html/line_in_box-1.png" width="192" />

<br>

Exact answer: \ \ $\frac{2+\sqrt{2}+5ln(\sqrt{2} + 1)}{15}$ 
= 0.5214054

<br>

Here approximate answer derived using simulation:  


```r
set.seed(111)  # Set seed of random number generator

# Create function that outputs length of a random line 
my_line <- function() {
  xx <- runif(2)  # x-coordinates of two points
  yy <- runif(2)  # y-coordinates of two point
  
  # Pythagoras formula for calculating line length
  line_length <- sqrt((xx[2] - xx[1])**2 + (yy[2] - yy[1])**2)
  line_length
}

# Run simulation using function replicate()
many_line_lengths <- replicate(1e5, my_line())

# Summarize
expected_length <- mean(many_line_lengths)
expected_length
```

```
## [1] 0.521678
```

<br>

Sampling distribution below. Not asked for, but why not!  


```r
hist(many_line_lengths, breaks = 200, main = "", xlab = "Line length")
```

<img src="03_ch3_files/figure-html/unnamed-chunk-1-1.png" width="672" />

<br>

## Compatibility intervals {-}

<br>

**Compatibility interval**, general term for intervals around point estimates, see 
@Amrhein2019.

+ **Confidence interval**, frequentist interval, e.g., derived using bootstrapping or 
from the T-distribution
+ **Likelihood interval**, defined from the likelihood function, same as Bayesian 
credible interval with no prior.
+ **Credible interval**, Bayesian summary of posterior distribution.

All of these terms may invoke overconfidence; it's easy to forget that the 
"confidence" or "credibility" refer to the small world of our statistical model. 
"Compatibility" is more neutral (and more correct) in that it just is a statement 
of the relationship between our data and various hypothesis (parameter values), given 
our statistical model.


## Sampling from the posterior (Section 3.2) {-}

Data generating process ("data story"): Using a *Small world* scenario (that we 
understand) to model a complex *Large world* problem. 

**Model**  

$W \sim Binomial(n = 9, p)$   

$p \sim Uniform(0, 1)$ 

<br>

Only one "estimator" in Bayesian statistics: The posterior distribution. If possible, 
present the full distribution. If not, you need to summarize:

+ Point estimate (mean, median, mode)
+ Interval
    - Percentile interval
    - Density interval (HPDI)
+ Standard deviation or similar, e.g., +/- 2 MAD of posterior (kind of standard error)
+ Why not show multiple intervals, e.g., 50 % and 95 %?


<br>

Sampling from the posterior, derived by grid approximation

```r
set.seed(999)

# Data
n = 9  # Number of trials
s = 6 # Number of successes

# Grid
pgrid <- seq(0, 1,  length.out = 1e4)  

# Prior
prior <- rep(1, length(pgrid))

# Likelihood: probability of s as a function of theta
likelihood <- dbinom(s, n, prob = pgrid)

# Relative posterior
rel_posterior <- likelihood * prior
  
# Plot relative posterior
plot(pgrid, rel_posterior, type = 'l', ylab = "Relative posterior")

# Sample from the posterior
samples <- sample(pgrid, size = 1e6, prob = rel_posterior, replace = TRUE)

# Summarize: Point estimates
pmode <-  pgrid[which(rel_posterior == max(rel_posterior))]  # Mode from posterior
smode <- rethinking::chainmode(samples, adj = 0.01)  # Mode from samples
```

<img src="03_ch3_files/figure-html/unnamed-chunk-2-1.png" width="672" />

```r
m <- mean(samples) # mean from samples
mdn <- median(samples) # median from samples

# Summarize: Dispersion
post_sd <- sd(samples)
post_mad <- mad(samples)  # Note: default adjustment constant = 1.4826

# Summarize: Intervals
rethinking::dens(samples, show.HPDI = 0.95)  # Plot with interval
```

<img src="03_ch3_files/figure-html/unnamed-chunk-2-2.png" width="672" />

```r
ci95 <- quantile(samples, c(0.025, 0.25, 0.75, 0.975))
hpdi95 <- rethinking::HPDI(samples, prob = c(0.5, 0.95))

# Print
stats <- c(observed = s/n, pmode = pmode, smode = smode, mean = m, 
           sd = post_sd, median = mdn, mad = post_mad)
round(stats, 2)
```

```
## observed    pmode    smode     mean       sd   median      mad 
##     0.67     0.67     0.65     0.64     0.14     0.64     0.15
```

```r
round(ci95, 2)
```

```
##  2.5%   25%   75% 97.5% 
##  0.35  0.54  0.74  0.88
```

```r
round(hpdi95, 2)
```

```
## |0.95  |0.5  0.5| 0.95| 
##  0.36  0.56  0.76  0.89
```

<br>

Note. In the examples above, I did calculate the relative posterior 
`rel_posterior <- likelihood * prior`. I did not bother with McElreath's 
normalization `posterior <- posterior / sum(posterior)` 
(e.g., R code 3.11, p. 56). Normalization is not necessary for calculating intervals, 
because the shape of the distribution remains the same no matter what constant it 
is multiplied with, as illustrated below.  



```r
# Plot analytic posterior (flat prior)
n <- 9
s <- 6
posterior <- dbeta(pgrid, shape1 = s+1, shape2 = (n-s+1))

# 95 % percentile of posterior
ci95 <- qbeta(c(0.025, 0.975), shape1 = s+1, shape2 = (n-s+1))

# Calculate 95 % percentile interval for exact posterior


# Relative posterior (from above)
rel_posterior <- likelihood * prior
rel_posterior2 <- rel_posterior / sum(rel_posterior) # McElreaths method
rel_posterior3 <- rel_posterior * 23 # Arbitrary scale

# Plot three relative posteriors
par(mfrow = c(2,2))

plot(pgrid, posterior, type = 'l', 
     ylab = "Posterior")
lines(ci95, c(0.1,0.1), col = 'blue')
      
plot(pgrid, rel_posterior, type = 'l', 
     ylab = "relative posterior")
lines(ci95, c(0.01,0.01), col = 'blue')

plot(pgrid, rel_posterior2, type = 'l', 
     ylab = "relative posterior")
lines(ci95, c(0.00001,0.00001), col = 'blue')

plot(pgrid, rel_posterior3, type = 'l', 
     ylab = "relative posterior")
lines(ci95, c(0.1,0.1), col = 'blue')
```

<img src="03_ch3_files/figure-html/unnamed-chunk-3-1.png" width="672" />

<br>

And also note that McElreath's 'normalization' doesn't really make sense, since we estimate a probability density function (for a continuous variable), but McElreath's normalization acts as if we were estimating a probability mass function (for a discrete variables). The area under the curve should be 1, but with McElreath's method it's
clear less than 1 (the sum of the vertical heights of the discrete grid points is normalize to 1, but that is not what we want for a continuous parameter).  

A better way would be to normalize using  
`posterior_grid <- relative_posterior/sum(relative_posterior * grid_density)` as 
illustrated below (cf. this to Fig 2.7 of the book).

```r
n <- 9 # Number of trials
s <- 6 # Number of successes
f <- n-s # Number of failures
a <- 1  # shape1 parameter of dbeta(), for uniform a = b = 1
b <- 1  # shape2 parameter of dbeta()


plot_posterior <- function(gridpoints) {
# Grid 
pgrid <- seq(from = 0, to = 1, length.out = gridpoints)
grid_density <- pgrid[2] - pgrid[1]

# Prior: Parameters of beta distribution (1, 1) is the same as Unif(0, 1)
prior <- dbeta(pgrid, shape1 = a, shape2 = b)

# Grid approximation of relative posterior
likelihood <- dbinom(s, n, pgrid)
relative_posterior <- prior * likelihood

# Normalize to posterior with area under the curve  = 1 (approximately), 
# irrespective of grid density
posterior_grid <- relative_posterior/sum(relative_posterior * grid_density)

# Analytic posterior
pgrid2 <- seq(from = 0, to = 1, by = 0.001)  # Dense grid to display analytic solution
posterior_analytic <- dbeta(pgrid2, shape1 = s + a, shape2 = f + b)

# Plot
plot(pgrid, posterior_grid, ylim = c(0, max(posterior_analytic)),
     ylab = "Posterior density")
lines(pgrid2, posterior_analytic, col = "red", lty = 2)
}

# Plot (cf. Fig. 2.7, but now with probability density on the y-axis)
par(mfrow = c(1, 2))
plot_posterior(5)
plot_posterior(20)
```

<img src="03_ch3_files/figure-html/unnamed-chunk-4-1.png" width="672" />

Circles are grid approximation of density, red curve is exact answer using analytic solution. Unlike McElreaths normalization method, the estimated values (y-axis numbers) does not depend on grid size.


## Sampling to simulate prediction (Section 3.3) {-}

Why?

+ *Model design*, sample from the prior to understand our model
+ *Model checking*
+ *Software validation*, simulate data and see if the model can recovery the (simulated) true values
+ *Research design*, including power analysis
+ *Forecasting*  


### Figure 3.6 {-}


```r
par(mfrow = c(1, 3))

# McElreaths upper diagram, here to the left 
rethinking::dens(samples, main = "Posterior distribution")

# McElreaths middle diagram, p = 0.6 (sixth diagram fom the left)
w <- rbinom(1e4, size = 9, prob = 0.6)
plot(table(w), ylab = "frequency", ylim = c(0, 5000), 
     main = "Sampling distribution, p = 0.6")

# McElreaths lower diagram, here to the right
w <- rbinom(1e4, size = 9, prob = samples)
plot(table(w), ylab = "frequency", ylim = c(0, 5000), 
     main = "Posterior predictive distribution")
```

<img src="03_ch3_files/figure-html/unnamed-chunk-5-1.png" width="672" />


### Figure 3.7 {-}
Not easy to understand how McElreath's derived his Fig 3.7. I guess he simulated it through 9 Bernoulli trials.  Here is one way to reproduce his figure.



```r
# Count number of switches
switches <- function(x) {
  out <-sum(x[-1] != x[-length(x)]) 
  out
}

# Count number of runs, can also be done using rle()
runs <- function(x) {
  switch <- x[-1] != x[-length(x)]
  idx <- c(which(switch), length(x))
  runlength <- diff(c(0, idx))
  # Use this if you want only the successes
  # value <- x[idx]  # value of run
  # r1 <- runlength[value == 1] 
  # r1 <- c(0, runlength)  # To avoid empty vector and warnings from max()
  # out <- max(r1)
  out <- max(runlength)
  out
}

# Create matrix with binomial experiments, number of experiment = length of
# vector with samples, it was created above
experiments <- sapply(samples, function(p) {rbinom(n = 9, size = 1, prob = p)})  

# Analyse data
longest_runs <- apply(experiments, 2, runs)
number_of_switches <- apply(experiments, 2, switches)
rr <- table(longest_runs)
ss <- table(number_of_switches)
ymax <- max(c(rr, ss)) + 1e3

# Plot result
par(mfrow = c(1, 2))
plot(rr, xlim = c(1, 9), ylim = c(0, ymax), xlab = "Longest run length", 
     ylab = "Frequency")
lines(c(3, 3), c(0, length(longest_runs[longest_runs == 3])), col = 'red')

plot(ss, xlim = c(0, 8), ylim = c(0, ymax), xlab = "Number of switches", 
     ylab = "Frequency")
lines(c(6, 6), c(0, length(number_of_switches[number_of_switches == 6])), 
      col = 'red')
```

<img src="03_ch3_files/figure-html/unnamed-chunk-6-1.png" width="672" />

```r
# Calculate tail probabilities ("Baysian p-value", one-tailed)
mean(longest_runs <= 3)
```

```
## [1] 0.418957
```

```r
mean(number_of_switches >= 6)
```

```
## [1] 0.097978
```

**Why is longest run = 9 more likely than longest run = 8?**


```r
p <- seq(0, 1, 0.01)
run9 <- p^9
run8 <- (1-p)*p^8 + p^8*(1-p)
plot(p, p, pch = '', ylim = c(0, 0.15), xlab = "Binomial probability", 
     ylab = "P_run9 (black), P_run8 (red)")
points(p, run8, type = 'l', col = 'red')
points(p, run9, type = 'l')
```

<img src="03_ch3_files/figure-html/unnamed-chunk-7-1.png" width="672" />

<br>

## Example two independent binomials {-}

### Data {-}

Data on smoking and prominent wrinkles around the eyes, as determined blindly using
a valid and reliable instrument! (Remember the Marlboro man)

```r
n1 = 150 # Number of smokers
s1 = 96  # Smokers with prominent wrinkles
n2 = 250 # Number of non-smokers
s2 = 105 # Non-smokers prominent wrinkles
```

Our outcome measure: Risk difference

```r
risk_smoke <- s1/n1
risk_nonsmoke <- s2/n2
riskdiff <- risk_smoke - risk_nonsmoke
outcome <- c(risk_smoke = risk_smoke, risk_nonsmoke = risk_nonsmoke, 
             riskdiff = riskdiff)
round(outcome, 6)
```

```
##    risk_smoke risk_nonsmoke      riskdiff 
##          0.64          0.42          0.22
```

We'll use grid approximation to obtain an interval around the estimated risk difference

### Grid aproximation {-}
We model the data as two independent binomial experiments with unknown ps and n = 150 (smokers) and n = 250 (non-smokers)


```r
# 2D-grid using expand.grid()
p <- seq(0.01, 0.99, by = 0.001)  
grid <- expand.grid(p1 = p, p2 = p)  


# Log likelihood
grid$loglike <- dbinom(s1, n1, prob = grid$p1, log = TRUE) +
                dbinom(s2, n2, prob = grid$p2, log = TRUE)

# Likelihood (standardized to maximum = 1)
grid$likelihood <- exp(grid$loglike) / exp(max(grid$loglike))

# Plot likelihood
rethinking::contour_xyz(grid$p1, grid$p2, grid$likelihood,
                        xlim = c(0.3, 0.8), ylim = c(0.3, 0.8), 
                        main = "Likelihood", xlab = "p smokers", 
                        ylab = "p non-smokers")
```

<img src="03_ch3_files/figure-html/unnamed-chunk-10-1.png" width="576" />



### Samples from the posterior distribution {-}


```r
set.seed(999)

# Flat prior
grid$prior <- rep(1, length(nrow(grid)))

# Posterior, standardized ala McElreath (sum = 1), not needed
grid$posterior <- (grid$prior * grid$likelihood) / sum(grid$likelihood)

# Risk difference for each pair of parameters in the grid
grid$pdiff <- grid$p1 - grid$p2

# Sampling from the posterior
samples <- sample(grid$pdiff, size = 1e5, prob = grid$posterior, replace = TRUE)
```



### Summary based on sampling from the posterior distribution  {-}


```r
rethinking::dens(samples, show.HPDI = 0.95)  # Plot distribution
```

<img src="03_ch3_files/figure-html/unnamed-chunk-12-1.png" width="672" />

```r
hpdi <- rethinking::HPDI(samples, prob = c(0.95))
pi95 <- quantile(samples, prob = c(0.025, 0.975))
pmode <- grid$pdiff[which(grid$posterior == max(grid$posterior))]
mdn <- median(samples)
list(observed_diff = riskdiff, mode = pmode, median = mdn, HPDI = hpdi, PI95 = pi95)
```

```
## $observed_diff
## [1] 0.22
## 
## $mode
## [1] 0.22
## 
## $median
## [1] 0.218
## 
## $HPDI
## |0.95 0.95| 
## 0.120 0.314 
## 
## $PI95
##  2.5% 97.5% 
## 0.119 0.314
```



### Relative Risk {-}


```r
set.seed(999)

# Risk ratio for each pair of parameters in the grid
grid$rr <- grid$p1 / grid$p2

# Sampling from the posterior
samples <- sample(grid$rr, size = 1e5, prob = grid$posterior, replace = TRUE)

rethinking::dens(samples, show.HPDI = 0.95)  # Plot distribution
```

<img src="03_ch3_files/figure-html/unnamed-chunk-13-1.png" width="672" />

```r
hpdi <- rethinking::HPDI(samples, prob = c(0.95))
pi95 <- quantile(samples, prob = c(0.025, 0.975))
pmode <- grid$rr[which(grid$posterior == max(grid$posterior))]
mdn <- median(samples)
observed_rr <- risk_smoke / risk_nonsmoke
list(observed_rr = observed_rr, mode = pmode, median = mdn, HPDI = hpdi, PI95 = pi95)
```

```
## $observed_rr
## [1] 1.52381
## 
## $mode
## [1] 1.52381
## 
## $median
## [1] 1.518248
## 
## $HPDI
##    |0.95    0.95| 
## 1.249433 1.823848 
## 
## $PI95
##     2.5%    97.5% 
## 1.258352 1.836114
```



### Odds ratio {-}


```r
set.seed(999)

# Odds ratio for each pair of parameters in the grid
grid$or <- (grid$p1/(1 - grid$p1)) / (grid$p2/(1 - grid$p2))

# Sampling from the posterior
samples <- sample(grid$or, size = 1e5, prob = grid$posterior, replace = TRUE)

rethinking::dens(samples, show.HPDI = 0.95)  # Plot distribution
```

<img src="03_ch3_files/figure-html/unnamed-chunk-14-1.png" width="672" />

```r
hpdi <- rethinking::HPDI(samples, prob = c(0.95))
pi95 <- quantile(samples, prob = c(0.025, 0.975))
mdn <- median(samples)
maxpost <- grid$or[grid$posterior == max(grid$posterior)]
observed_or <- (risk_smoke/ (1 - risk_smoke)) / (risk_nonsmoke/(1 - risk_nonsmoke))
list(observed_or = observed_or, mode = maxpost, median = mdn, HPDI = hpdi, PI95 = pi95)
```

```
## $observed_or
## [1] 2.455026
## 
## $mode
## [1] 2.455026
## 
## $median
## [1] 2.436687
## 
## $HPDI
##    |0.95    0.95| 
## 1.524334 3.574548 
## 
## $PI95
##     2.5%    97.5% 
## 1.616626 3.715710
```

<br>

Just to show all the "data" obtained by grid approximation

```r
maprow <- which(grid$posterior == max(grid$posterior))  # row with highest posterior
grid[c(1, 2, maprow-1, maprow, maprow + 1, nrow(grid)-1, nrow(grid)), ]
```

```
##           p1   p2     loglike    likelihood prior     posterior  pdiff
## 1      0.010 0.01 -665.220228 3.627076e-287     1 4.755284e-291  0.000
## 2      0.011 0.01 -656.125024 3.232612e-283     1 4.238122e-287  0.001
## 402840 0.639 0.42   -5.667058  9.996748e-01     1  1.310625e-04  0.219
## 402841 0.640 0.42   -5.666733  1.000000e+00     1  1.311052e-04  0.220
## 402842 0.641 0.42   -5.667059  9.996743e-01     1  1.310625e-04  0.221
## 962360 0.989 0.99 -650.980257 5.544946e-281     1 7.269712e-285 -0.001
## 962361 0.990 0.99 -656.029988 3.554897e-283     1 4.660654e-287  0.000
##               rr        or
## 1      1.0000000 1.0000000
## 2      1.1000000 1.1011122
## 402840 1.5214286 2.4444005
## 402841 1.5238095 2.4550265
## 402842 1.5261905 2.4657116
## 962360 0.9989899 0.9081726
## 962361 1.0000000 1.0000000
```

<br>

*Below with ordinary logistic regression using glm()*

```r
# Remember
# n1 Number of smokers
# s1 Smokers with prominent wrinkles
# n2 Number of non-smokers
# s2 Non-smokers prominent wrinkles

# Data
smoking <- c(rep(1, n1), rep(0, n2))
wrinkles <- c(rep(1, s1), rep(0, n1-s1), rep(1, s2), rep(0, n2-s2))

# Logistic regression
mylogit <- glm(wrinkles ~ smoking, family = binomial(link = "logit"))
summary(mylogit)
```

```
## 
## Call:
## glm(formula = wrinkles ~ smoking, family = binomial(link = "logit"))
## 
## Deviance Residuals: 
##     Min       1Q   Median       3Q      Max  
## -1.4294  -1.0438   0.9448   1.3172   1.3172  
## 
## Coefficients:
##             Estimate Std. Error z value Pr(>|z|)    
## (Intercept)  -0.3228     0.1281  -2.519   0.0118 *  
## smoking       0.8981     0.2130   4.217 2.47e-05 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for binomial family taken to be 1)
## 
##     Null deviance: 554.51  on 399  degrees of freedom
## Residual deviance: 536.17  on 398  degrees of freedom
## AIC: 540.17
## 
## Number of Fisher Scoring iterations: 4
```

```r
# Model summaries
maxlike <- exp(mylogit$coefficients[2])  # Point estimate, maximum likelihood
ci <- exp(confint(mylogit))[2, ]
```

```
## Waiting for profiling to be done...
```

```r
round(c(observed_or = observed_or, maxlike = maxlike, ci = ci), 2)
```

```
##     observed_or maxlike.smoking        ci.2.5 %       ci.97.5 % 
##            2.46            2.46            1.62            3.74
```

<br>

