---
editor_options: 
  markdown: 
    wrap: 72
---

# Linear models

## Concepts {.unnumbered}

<div class="concepts">
<ul>
<li><strong>Normal distribution</strong></li>
<li><strong>Model notation</strong>, see McElreath Section 4.2
book<br />
</li>
<li><strong>Linear model</strong> and it’s link to the normal
distribution</li>
<li><strong>Parametrization</strong></li>
<li><strong>Sampling from the posterior</strong> of a linear model,
understand these functions:
<ul>
<li><code>rethinking::link()</code>  to derive Compatibility
interval</li>
<li><code>rethinking::sim()</code>  to derive Prediction interval</li>
</ul></li>
<li><strong>Polynomial regression</strong></li>
</ul>
<p>Readings:<br />
- <span class="citation">@mcelreath2020statistical</span>, Chapter 4</p>
</div>

<br>

Packages used below.


```r
library(rethinking)
library(rstanarm)
```

## Normal distribution {.unnumbered}

The sum of many small independent random variables will be approximately
distributed as the Normal distribution (this follows from the Central
Limit Theorem).

**Notation**

$Y \sim N(\mu, \sigma)$, random variable Y is distributed as a normal
distribution with location (mean) $\mu$ and scale (sd) $\sigma$ (Note,
the second parameter is often given as the variance $\sigma^2$.)

<br>

Example: Y is scores on an IQ-test, normally distributed with mean =
100, sd = 15: $Y \sim N(\mu=100, \sigma=15)$

<br>

**Probability density function (PDF)**

For a discrete variable, the probability mass function (PMF) sums to one
(see examples above). For a continuous variable, the probability density
function (PDF) has an area under the curve equal to one.


```r
iq <- seq(from = 50, to = 150, by = 1)
iqdens <- dnorm(iq, mean = 100, sd = 15)
plot(iq, iqdens, type = 'l', xlab = "y [IQ-score]", ylab = "density")
    lines(c(40, 160), c(0, 0), col = "grey")
```

<img src="04_ch4_files/figure-html/unnamed-chunk-2-1.png" width="672" />

**NOTE**: Density is not the same as probability. It can be greater than
1!


```r
# Denisty for uniform(0, 0,5)
x <- seq(from = -0.1, to = 0.6, by = 0.0001)
xdens <- dunif(x, min = 0, max = 0.5)
plot(x, xdens, type = 'l', xlab = "x", ylab = "density", ylim = c(0, 2.2))
lines(c(-0.2, 0.7), c(0, 0), col = "grey")
```

<img src="04_ch4_files/figure-html/unnamed-chunk-3-1.png" width="672" />

*What is the probability that a randomly selected individual has an
IQ-score of* *exactly 100*, if $IQ \sim N(\mu=100, \sigma=15)$? The
probability correspond to an area under the curve (PDF) of the normal
distribution. The answer to the question depends on what we mean with
"exactly". If we really mean it, as in $100.000000...$, then the answer
is $P(IQ = 100.000...) = 0$, because the area under the curve has zero
width. But I guess that IQ-scores are measured without decimals, if so,
$IQ = 100$ can be interpreted as $99.5 < IQ < 100.5$. This is an
interval, and an area under the curve can be calculated:
$P \approx .03$.


```r
par(mfrow = c(1, 2))

# Draw PDF, and add line for mu = 100.0000...
iq <- seq(from = 50, to = 150, by = 1)
iqdens <- dnorm(iq, mean = 100, sd = 15)
plot(iq, iqdens, type = 'l', xlab = "y [IQ-score]", ylab = "density",
     xlim = c(70, 130))
lines(c(40, 160), c(0, 0), col = "black")
lines(c(100, 100), c(0, dnorm(100, mean = 100, sd = 15)), 
      col = "grey", lwd = 0.2, lty = 3)

# Draw PDF, and add area for 99.5 < mu < 100.5
iq <- seq(from = 50, to = 150, by = 1)
iqdens <- dnorm(iq, mean = 100, sd = 15)
plot(iq, iqdens, type = 'l', xlab = "y [IQ-score]", ylab = "density",
     xlim = c(70, 130))
lines(c(40, 160), c(0, 0), col = "black")
mm <- seq(99.5, 100.5, 0.01)
d <- dnorm(mm, mean = 100, sd = 15)
polygon(x = c(mm, rev(mm)), y = c(rep(0, length(d)), rev(d)),
          col = rgb(0, 0, 0, 0.1), border = FALSE)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-4-1.png" width="672" />

<br>

Calculate probability (area under the curve):


```r
# P(mu == 100)  
pnorm(100, mean = 100, sd = 15) -  pnorm(100, mean = 100, sd = 15)
```

```
## [1] 0
```

```r
# P(mu > 99.5 | mu < 100.5)
pnorm(100.5, mean = 100, sd = 15) -  pnorm(99.5, mean = 100, sd = 15)
```

```
## [1] 0.02659123
```

**Cumulative distribution function (CDF)**

The CDF has the same definition for discrete and continuous variables:
It gives the probability that a random variable is less than or equal to
a specific value of a random variable.


```r
iq <- seq(from = 50, to = 150, by = 1)
iqcdf <- pnorm(iq, mean = 100, sd = 15)
plot(iq, iqcdf, type = 'l', xlab = "y [IQ-score]", ylab = "P(Y < y)")
lines(c(40, 160), c(0, 0), col = "grey")
```

<img src="04_ch4_files/figure-html/unnamed-chunk-6-1.png" width="672" />

**Location and scale of normal distribution**

This plot shows three normal distributions with...

-   Left panel: Same location (mean), different scale (standard
    deviation)

-   Right panel: Different location, same scale


```r
x <- seq(from = 0, to = 100, by = 1)

par(mfrow = c(1, 2))

# Different scales, same location 
plot(x, dnorm(x, 50, 10), ylim = c(0, 0.2), pch = "", xlab = "x", ylab = "density")
lines(x, dnorm(x, 50, 2), col = "red")
lines(x, dnorm(x, 50, 10))
lines(x, dnorm(x, 50, 20), col = "blue")

# Different location, same scale 
plot(x, dnorm(x, 50, 10), ylim = c(0, 0.2), pch = "", xlab = "x", ylab = "density")
lines(x, dnorm(x, 30, 3), col = "red")
lines(x, dnorm(x, 50, 3))
lines(x, dnorm(x, 55, 3), col = "blue")
```

<img src="04_ch4_files/figure-html/unnamed-chunk-7-1.png" width="960" />

<br>

#### Standard normal {.unnumbered}

Remember that linear transformations doesn't change the shape of
distributions. The standard normal is just a linear transformation
(z-scores) of another normal distribution with the purpose of setting
$\mu = 0$ and $\sigma = 1$.

$Y \sim N(\mu, \sigma)$ Original (non-standard) normal,

$Z = (Y - \mu)/\sigma$ Linear transformation,

$Z \sim N(0, 1)$ Standard normal.


```r
par(mfrow = c(1, 2))
z <- seq(-4, 4, 0.1)

# PDF
dd <- dnorm(z)
plot(z, dd, type = 'l', ylab = "density")
lines(c(-5, 5), c(0, 0), col = "grey")
mtext("PDF", 3, cex = 0.8)

# CDF
dd <- pnorm(z)
plot(z, dd, type = 'l', ylab = "probability, P(Z <= z)")
lines(c(-5, 5), c(0, 0), col = "grey")
mtext("CDF", 3, cex = 0.8)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-8-1.png" width="672" />

<br>

#### The 68-95-99.7 rule {.unnumbered}

The 68-95-99.7 rule, also known as the empirical rule, describes the
distribution of data in a normal distribution. According to this rule:

-   Approximately 68 % of the data falls within one standard deviation
    of the mean. Put equivalently, the probability that a random
    observation, $x$ falls within one standard deviation of the mean is
    approximately 0.68, that is,
    $Pr(\mu - \sigma < x < \mu + \sigma) \approx 0.68)$

-   About 95 % of the data lies within two standard deviations of the
    mean.

-   About 99.7 % of the data is found within three standard deviations
    of the mean.

For the standard normal, $N(\mu = 0, \sigma = 1)$, these three intervals
corresponds to the intervals [-1, 1], [-2, 2], and, [-3, 3],
respectively.

<img src="04_ch4_files/figure-html/unnamed-chunk-9-1.png" width="1056" />

In R, you can calculate the exact probabilities like this:


```r
# Calculate (pm1sd for +/- 1 sd, etc.)
pm1sd <- pnorm(1) - pnorm(-1)
pm2sd <- pnorm(2) - pnorm(-2)
pm3sd <- pnorm(3) - pnorm(-3)

# Round and print
round(c(pm1sd = pm1sd, pm2sd = pm2sd, pm3sd = pm3sd), 5)
```

```
##   pm1sd   pm2sd   pm3sd 
## 0.68269 0.95450 0.99730
```

<br>

### How normal is the Normal? {.unnumbered}

Example from @gelman2020regression, Fig. 3.6:

Assume that height is normally distributed, with different means for men
and women:

-   Men: $h_{male} \sim N(\mu = 180, \sigma = 8)$, unit cm.
-   women: $h_{female} \sim N(\mu = 168, \sigma = 8)$, unit cm.


```r
set.seed(999)
male <- rnorm(5e4, 180, 8)
female <- rnorm(5e4, 168, 8)
combined <- c(male, female)

# Blank histogram (note argument lty = 'blank')
hist(male, breaks = seq(120, 230, by = 20), freq = FALSE, lty = 'blank',
     col = "white", main = '', xlab = "Height (cm) ", ylim = c(0, 0.06))
# Add density for Yes trials
dens_male <- density(male)
polygon(dens_male, col = rgb(0, 1, 0, 0.5))

# Add density for No trials
dens_female <- density(female)
polygon(dens_female, col = rgb(1, 0, 0, 0.5))
```

<img src="04_ch4_files/figure-html/unnamed-chunk-11-1.png" width="672" />

Height in the combined population of men and women is however not
normally distributed.


```r
# Blank histogram (note argument lty = 'blank')
hist(combined, breaks = seq(120, 230, by = 20), freq = FALSE, lty = 'blank', 
     col = "white", main = '', xlab = "Height (cm) ", ylim = c(0, 0.06))
# Add density for Yes trials
dens_combined <- density(combined)
polygon(dens_combined, col = rgb(0, 0, 1, 0.5))
```

<img src="04_ch4_files/figure-html/unnamed-chunk-12-1.png" width="672" />

<br> The density plot for the combined population looks normal, but
actually it is not. If normally distributed, about 84 % of values are
less than 1 standard deviation above the mean. This is true for our
simulated male and female populations, but not for the combined
population.


```r
# Exact probability of x < 1, if X ~ N(mean = 0, sd = 1)
pnorm(1)  
```

```
## [1] 0.8413447
```

```r
# Estimate from our simulated male population
mean(male < mean(male) + sd(male))
```

```
## [1] 0.8416
```

```r
# Estimate from our female popualtion
mean(female < mean(female) + sd(female)) 
```

```
## [1] 0.84062
```

```r
# Estimate from the combined popualtion
mean(combined < mean(combined) + sd(combined))
```

```
## [1] 0.83395
```

A common method for evaluating normality is to visualize using so called
QQ-plots (points fall on the red line if the data is perfectly normal).
*QQ-plots are not part of the course*, but I use them here to drive home
the point that the (simulated) combined population (vector `combined`)
is not normally distributed.


```r
# QQ-plot
zmale <- (male - mean(male))/sd(male)
zfemale <- (female - mean(female))/sd(female)
zcombined <- (combined - mean(combined))/sd(combined)
ylim <- c(-4, 4)
par(mfrow = c(1, 3))
qqnorm(zmale, main = "Population male weight, z-score", cex = 0.7, ylim = ylim)
qqline(zmale, col = "red")
qqnorm(zfemale, main = "Population female weight, z-score", cex = 0.7, ylim = ylim)
qqline(zfemale, col = "red")
qqnorm(zcombined, main = "Population combined, z-score ", cex = 0.7, ylim = ylim)
qqline(zcombined, col = "red")
```

<img src="04_ch4_files/figure-html/unnamed-chunk-14-1.png" width="672" />

<br> <br>

## Model notation {.unnumbered}

From the book, Section 4.2:

Recipe:

1.  Recognize a set of variables, some observed (*data*) others
    unobserved (*parameters*). Note that for McElreath, data and
    parameters are both the some kind of thing: data!
2.  Define each variable either in terms of other variables or in terms
    of a probability distribution.
3.  The combination of variables and their distributions defines a
    *joint generative model* that can be used to simulate potential
    observations and analyze observed data

Example: The globe tossing experiment,

$W \sim Binomial(n = 9, p)$\
$p \sim Uniform(0, 1)$

with data being

$W = 6$,

that is, six water out of n = 9 tosses:

<br>

Note: We could also have defined the model in terms of the Bernoulli
distribution,\
because $Bernoulli(p)$ is the same as $Binomial(n = 1, p)$:

$y_i \sim Bernoulli(p)$\
$p \sim Uniform(0, 1)$,

with data being nine independent tosses:

$y = (1, 0, 1, 1, 1, 0, 1, 0, 1)$,

where 1 = Water, and 0 = land.

<br>

## Notation regression models {.unnumbered}

In many text books, something like this:

$y_i = \beta_0 + \beta_1x_i + \epsilon_i$\
$\epsilon_i \sim Normal(0, \sigma)$

<br>

The preferred way: "A farewell to epsilon", McElreath, p. 84:

|                                  |                  |
|----------------------------------|------------------|
| $y_i \sim Normal(\mu_i, \sigma)$ | Likelihood       |
| $\mu_i= \beta_0 + \beta_1 x_i$   | Linear predictor |
| $b_0 \sim Normal(0, 1)$          | Prior            |
| $b_1 \sim Normal(0, 1)$          | Prior            |
| $\sigma \sim Exp(1)$             | Prior            |

<br>

## Regression in R {.unnumbered}

Many ways, here some:

-   **lm()**, Ordinary Least Square (OLS)
-   **glm()**, Maximum Likelihood
-   **rethinking::quap()**, Bayesian, quadratic approximation
-   **rethinking::ulam()**, Bayesian, MCMC (Hamiltonian Monte Carlo, HMC)
-   **rstanarm::stan_glm()**, Bayesian, MCMC (HMC)
-   **brms::brm()**, Bayesian, MCMC (HMC)
-   **rstan::stan()**, Bayesian, MCMC (HMC)

## One-sample t-test as regression {.unnumbered}

Data: Howell1, heights for $age\geq 18$


```r
data(Howell1)
d <- Howell1
rm(Howell1)
d2 <- d[d$age >= 18, ]

hist(d2$height, breaks = seq(129, 191, by = 2), ylab = '', yaxt = 'n', prob = TRUE, 
     xlab = sprintf('Height (N = %.0f)', length(d2$height)), main = '')
z <- density(d2$height)
polygon(z, col = rgb(0, 1, 1, 0.1))
```

<img src="04_ch4_files/figure-html/unnamed-chunk-15-1.png" width="672" />

```r
round(c(mean = mean(d2$height), sd = sd(d2$height)), 2)
```

```
##   mean     sd 
## 154.60   7.74
```

**NOTE: Height is generally not normally distributed, unless you
stratify on sex!**

<br>

Model (p. 82):

$height_i \sim Normal(\mu, \sigma)$

$\mu = \beta_0$

$\beta_0 \sim Normal(178, 20)$

$\sigma \sim Uniform(0, 50)$

<br>

**t.test()**


```r
tout <- t.test(d2$height)
tout
```

```
## 
## 	One Sample t-test
## 
## data:  d2$height
## t = 374.63, df = 351, p-value < 2.2e-16
## alternative hypothesis: true mean is not equal to 0
## 95 percent confidence interval:
##  153.7855 155.4087
## sample estimates:
## mean of x 
##  154.5971
```

<br>

**lm()**

<img src="04_ch4_files/figure-html/unnamed-chunk-17-1.png" width="672" />


```r
lm_interc <- lm(d2$height ~ 1)
summary(lm_interc)
```

```
## 
## Call:
## lm(formula = d2$height ~ 1)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -18.0721  -6.0071  -0.2921   6.0579  24.4729 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 154.5971     0.4127   374.6   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 7.742 on 351 degrees of freedom
```

```r
confint(lm_interc)
```

```
##                2.5 %   97.5 %
## (Intercept) 153.7855 155.4087
```

```r
stats <- c(mean = mean(d2$height), sd = sd(d2$height))
stats
```

```
##       mean         sd 
## 154.597093   7.742332
```

<br>

**glm()**


```r
glm_interc <- glm(d2$height ~ 1)
summary(glm_interc)
```

```
## 
## Call:
## glm(formula = d2$height ~ 1)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -18.0721   -6.0071   -0.2921    6.0579   24.4729  
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 154.5971     0.4127   374.6   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for gaussian family taken to be 59.94371)
## 
##     Null deviance: 21040  on 351  degrees of freedom
## Residual deviance: 21040  on 351  degrees of freedom
## AIC: 2442.8
## 
## Number of Fisher Scoring iterations: 2
```

```r
confint(glm_interc)
```

```
## Waiting for profiling to be done...
```

```
##    2.5 %   97.5 % 
## 153.7883 155.4059
```

<br>

**rstanarm::stan_glm()**


```r
stan_glm_interc <- rstanarm::stan_glm(height ~ 1, data = d2, seed = 123, refresh = 0)
stan_glm_interc
```

```
## stan_glm
##  family:       gaussian [identity]
##  formula:      height ~ 1
##  observations: 352
##  predictors:   1
## ------
##             Median MAD_SD
## (Intercept) 154.6    0.4 
## 
## Auxiliary parameter(s):
##       Median MAD_SD
## sigma 7.8    0.3   
## 
## ------
## * For help interpreting the printed output see ?print.stanreg
## * For info on the priors used see ?prior_summary.stanreg
```

<br>

Intervals from rstanarm::stan_glm()


```r
rstanarm::posterior_interval(stan_glm_interc, prob = 0.95)  # This is a percentile interval
```

```
##                   2.5%     97.5%
## (Intercept) 153.820588 155.40722
## sigma         7.199407   8.35898
```

```r
# Below extracting of samples, used to get highest density interval
ss <- as.data.frame(stan_glm_interc)
names(ss) <- c("mu", "sigma")
rethinking::HPDI(ss$mu, prob = 0.95)
```

```
##    |0.95    0.95| 
## 153.8391 155.4235
```

<br>

### By hand, using grid approximation {.unnumbered}


```r
# Define grid
mu <- seq(from = 148, to = 168, length.out = 400)
sigma <- seq(from = 5, to = 10, length.out = 400)
grid <- expand.grid(mu = mu, sigma = sigma)

# Calculate likelihood  (cf. McElreath, R code 4.16)
grid$loglike <- sapply(1:nrow(grid), function(i) sum(dnorm(
                                       d2$height, 
                                       mean = grid$mu[i],
                                       sd = grid$sigma[i],
                                       log = TRUE)))

# Normalize log-likelihood to max = 0, and antilog (not needed, just for illustration)
grid$loglike <- grid$loglike - max(grid$loglike)

# Antilog of likelihood 
grid$likelihood <- exp(grid$loglike) 

# Posterior, with McElreath's priors (Rcode 4.26-27, p. 88).
grid$logpost <- grid$loglike + 
                dnorm(grid$mu, mean = 178, sd = 20, log = TRUE) +
                dunif(grid$sigma, min = 0, max = 50, log = TRUE) 

grid$relpost <- exp(grid$logpost) # Antilog to get relative posterior

par(mfrow = c(1, 2))
# Plot likelihood function (contour)
rethinking::contour_xyz(grid$mu, grid$sigma, grid$likelihood, 
            xlim = c(150, 160), ylim = c(5, 10), xlab = 'mu', ylab = 'sigma',
            drawlabels = FALSE)
mtext(side = 3, text = "Likelihood")

# Plot posterior (contour)
rethinking::contour_xyz(grid$mu, grid$sigma, grid$relpost, 
            xlim = c(150, 160), ylim = c(5, 10), xlab = 'mu', ylab = 'sigma',
            drawlabels = FALSE)
mtext(side = 3, text = "Posterior")
```

<img src="04_ch4_files/figure-html/unnamed-chunk-22-1.png" width="672" />

<br>

Note that the likelihood dominates with these priors.

<br>

Sample from posterior and summarize


```r
# Sample of rows with replacement, from data frame with posterior
srows <- sample(nrow(grid), size = 1e4, prob = grid$relpost, replace = TRUE)
ss <- grid[srows, 1:2]  # Select random rows and put in data frame called ss
plot(ss$mu, ss$sigma, col = rgb(0, 0, 1, 0.1), 
     xlim = c(150, 160), ylim = c(5, 10))  # Plot samples
```

<img src="04_ch4_files/figure-html/unnamed-chunk-23-1.png" width="384" />

```r
# Point estimate and 90% CI mu
postsum <- c(mdn = median(ss$mu), HPDI(ss$mu, prob = 0.9))
round(postsum, 1)
```

```
##   mdn  |0.9  0.9| 
## 154.6 153.9 155.2
```

<br>

### quap() {.unnumbered}

**quap()**, cf. McElreath Rcode 4.26-27, p. 88


```r
model <- alist(height ~ dnorm(mu, sigma),
               mu <- b0, 
               b0 ~ dnorm(178, 20),
               sigma ~ dunif(0, 50))
quap_interc <- quap(model, data = d2, start = list(b0 = 160, sigma = 10))
precis(quap_interc, prob = 0.95)
```

```
##             mean        sd       2.5%      97.5%
## b0    154.607022 0.4119946 153.799527 155.414516
## sigma   7.731332 0.2913859   7.160226   8.302438
```

<br>

Posterior predictive check signals issue with model assumptions.


```r
ss0 <- extract.samples(quap_interc)

# Empty plot
plot(NULL, xlim = c(120, 200), ylim = c(0, 0.08),
     xlab = "Height [cm]", ylab = "Density")

# Add densities for predicted samples
for (j in 1:100) {
  ypred <- rnorm(nrow(d2), ss0$b0[j], ss0$sigma[j])
  lines(density(ypred), col = rgb(1, 0, 0, 0.2))
}

# Add densities for observed sample
lines(density(d2$height), lwd = 2)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-25-1.png" width="672" />

<br>

Let's try adding sex as covariate.

```r
model2 <- alist(height ~ dnorm(mu, sigma),
               mu <- b0 + b1 * male, 
               b0 ~ dnorm(178, 20),
               b1 ~ dnorm(10, 5), 
               sigma ~ dunif(0, 50))
quap_sex <- quap(model2, data = d2, start = list(b0 = 160, sigma = 10))
precis(quap_sex, prob = 0.95)
```

```
##             mean        sd       2.5%      97.5%
## b0    149.530483 0.4023802 148.741832 150.319134
## sigma   5.521336 0.2080943   5.113479   5.929194
## b1     10.821919 0.5856171   9.674131  11.969708
```

<br>

Now the posterior predictive check looks better! 


```r
ss1 <- extract.samples(quap_sex)

# Empty plot
plot(NULL, xlim = c(120, 200), ylim = c(0, 0.08),
     xlab = "Height [cm]", ylab = "Density")

# Add densities for predicted samples
for (j in 1:100) {
  ypred <- rnorm(nrow(d2), ss1$b0[j] + ss1$b1[j] * d2$male, ss1$sigma[j])
  lines(density(ypred), col = rgb(1, 0, 0, 0.2))
}

# Add densities for observed sample
lines(density(d2$height), lwd = 2)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-27-1.png" width="672" />

## Compatibility and prediction interval for one sample {.unnumbered}

Here I will use samples from quap() to derive compatibility and
prediction intervals.

Compatibility interval by sampling from the posterior


```r
ss <- extract.samples(quap_interc)
head(ss)
```

```
##         b0    sigma
## 1 154.9882 7.689402
## 2 154.4054 7.754096
## 3 154.5648 7.654934
## 4 154.3189 7.443391
## 5 154.6327 7.212977
## 6 154.4697 7.864102
```

```r
quantile(ss$b0, prob = c(0.025, 0.975))  # 95 % percentile interval
```

```
##     2.5%    97.5% 
## 153.7876 155.4075
```

```r
# Highest density interval, will be the same as percentile as quap() assume 
# normal posterior
HPDI(ss$b0, prob = 0.95)  
```

```
##    |0.95    0.95| 
## 153.8099 155.4228
```

<br> Prediction interval, using b0 and sigma to simulate (predict)
observations


```r
# Simulate observations for each model defined by rows of the  
# extracted samples data-frame ss <- extract.samples(quap_interc), defined above
my_sim  <- rnorm(nrow(ss), mean = ss$b0, sd = ss$sigma)
mean(my_sim)
```

```
## [1] 154.6196
```

```r
HPDI(my_sim, prob = 0.95)
```

```
##    |0.95    0.95| 
## 139.1341 169.8632
```

<br>

## Bivariate regression {.unnumbered}


```r
plot(d2$height ~ d2$weight, xlab = 'Weight   (NB! uncentered)', ylab = 'Height')

# Draw smoothing curve
lines(lowess(d2$weight,d2$height), col = "blue", lty = 2)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-30-1.png" width="672" />

Model (p. 93):

$height_i \sim Normal(\mu_i, \sigma)$

$\mu_i = \beta_0 + \beta_1weightc_i$

$\beta_0 \sim Normal(160, 100)$

$\beta_1 \sim Normal(0, 10)$

$\sigma \sim Uniform(0, 50)$

Centering: $weightc_i = weight_i - mean(weight)$

<br>

**lm()**


```r
# Center the predictor variable
d2$weight_c <- d2$weight - mean(d2$weight)
lm_out <- lm(d2$height ~ d2$weight_c)
summary(lm_out)
```

```
## 
## Call:
## lm(formula = d2$height ~ d2$weight_c)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -19.7464  -2.8835   0.0222   3.1424  14.7744 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 154.59709    0.27110  570.25   <2e-16 ***
## d2$weight_c   0.90503    0.04205   21.52   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 5.086 on 350 degrees of freedom
## Multiple R-squared:  0.5696,	Adjusted R-squared:  0.5684 
## F-statistic: 463.3 on 1 and 350 DF,  p-value: < 2.2e-16
```

```r
confint(lm_out)
```

```
##                   2.5 %      97.5 %
## (Intercept) 154.0638974 155.1302878
## d2$weight_c   0.8223315   0.9877267
```

<br>

**quap()**, cf. McElreath Rcode 4.42,4.44, p.97-9


```r
model <- alist(height ~ dnorm(mu, sigma),
               mu <- b0 + b1*weight_c,
               b0 ~ dnorm(160, 100),
               b1 ~ dnorm(0, 10),
               sigma ~ dunif(0, 50))
quap_bivariat <- quap(model, data = d2, start = list(b0 = 160, b1 = 1, sigma = 6))
precis(quap_bivariat, prob = 0.95)
```

```
##              mean         sd        2.5%       97.5%
## b0    154.5971355 0.27033054 154.0672973 155.1269736
## b1      0.9050137 0.04192755   0.8228372   0.9871902
## sigma   5.0718691 0.19115340   4.6972153   5.4465228
```

<br>

**plot()**, black solid line: lm(); dashed red line: quap()


```r
plot(d2$height ~ d2$weight_c, xlab = 'Weight (centered)', ylab = 'Height',
     xlim = c(-18, 18))
abline(lm_out, col = 'black')
abline(c(coef(quap_bivariat)['b0'], coef(quap_bivariat)['b1']), lty = 2, col = 'red')
```

<img src="04_ch4_files/figure-html/unnamed-chunk-33-1.png" width="672" />

<br>

## Sampling from the posterior {.unnumbered}

**extract.sample()** E.g., McElreath, p. 90, Rcode 4.34


```r
post_bivariat <- extract.samples(quap_bivariat, 1e4)
head(post_bivariat)
```

```
##         b0        b1    sigma
## 1 154.9736 0.9022337 4.675284
## 2 154.4683 0.8621477 5.061818
## 3 154.4741 0.8565592 5.576837
## 4 154.6705 0.8510702 4.940126
## 5 154.4399 0.9369586 5.130049
## 6 154.3645 0.8729848 5.007056
```

```r
plot(post_bivariat$b0, post_bivariat$b1, pch = 19, col = rgb(0, 0.5, 1, 0.2),
     xlab = 'Posterior intercept, b0', ylab = 'Posterior slope, b1' )
```

<img src="04_ch4_files/figure-html/unnamed-chunk-34-1.png" width="672" />


```r
par(mfrow = c(1, 2))
# Add n lines drawn randomly from the posterior
plot(d2$height ~ d2$weight_c, xlab = 'Weight (centered)', ylab = 'Height',
     xlim = c(-18, 18))
n = 50

add_lines <- function(j) {
  abline(post_bivariat$b0[j], post_bivariat$b1[j], col = rgb(0, 0.5, 0, 0.1))
  
}

invisible(sapply(1:n, add_lines))  # invisible() just to avoid unnecessary printing
```

<img src="04_ch4_files/figure-html/unnamed-chunk-35-1.png" width="672" />

**Posterior distribution of heights at centered weight = 10 (i.e.,
mean(weight) + 10)**


```r
mu_at_10 <- post_bivariat$b0 + post_bivariat$b1 * 10
dens(mu_at_10, col = 'blue', xlab = 'mu|centered-weight=10')
```

<img src="04_ch4_files/figure-html/unnamed-chunk-36-1.png" width="672" />

```r
ci95 <- quantile(mu_at_10, probs = c(0.025, 0.5, 0.975))
round(ci95, 2)
```

```
##   2.5%    50%  97.5% 
## 162.69 163.65 164.60
```

<br>

**link()** and **sim()**

link() provides a posterior distribution of $\mu$ for each x-value
(weight_c) that we ask for (McElreath, p. 105). See also Rcode 4.58 (p.
107) to see under the hood of link().

<br>

sim() gives a collection of outcomes (heights in our example) that
embody the uncertainty in the posterior *as well as* the uncertainty in
the Gaussian likelihood\
(McElreath, p. 108). See also Rcode 4.63 (p. 109-10)0 to see under the
hood of sim(). [It uses rnorm to simulate data]

This code is similar to Rcode 4.54, illustrating **link()**, and Rcode
4.59-61, illustrating **sim()**

But I added two prediction intervals: 50 % (blue) and 95 % (light blue)


```r
weightc_seq <- seq(-20, 20, by = 1)
# Mean-values using link()
mu <- link(quap_bivariat, data = list(weight_c = weightc_seq))

# Individual height values using sim()
sim_height <- sim(quap_bivariat, data = list(weight_c = weightc_seq))

# Plot data 
plot(d2$height ~ d2$weight_c, xlab = 'Weight (centered)', ylab = 'Height',
     xlim = c(-20, 20))

# Add line with ci95 around E(Y|X = x) as shade, using output from link()
mu_mean <- apply(mu, 2, mean)
mu_ci95 <- apply(mu, 2, PI, prob = 0.95)
lines(weightc_seq, mu_mean)
shade(mu_ci95, weightc_seq, col = rgb(0, 1, 0, 0.3))

# Add shaded area for 50 % prediction interval, using output from sim()
height50 <- apply(sim_height, 2, PI, prob = 0.5)
shade(height50, weightc_seq, col = rgb(0, 0, 1, 0.2))

# Add shaded area for 95 % prediction interval, using output from sim()
height95 <- apply(sim_height, 2, PI, prob = 0.95)
shade(height95, weightc_seq, col = rgb(0, 0, 1, 0.05))
```

<img src="04_ch4_files/figure-html/unnamed-chunk-37-1.png" width="672" />

<br>

## Polynomial regression {.unnumbered}

Note: Polynomial regression is needed to solve problem MN4 (part of
individual assignment). Below relevant example with age as predictor of
weight, for men only (MN4 asks for separate models for men and women).

<br>

Here a second-degree polynomial model to capture the non-linear trend of
weight as a function of age (men tend to gain weight with age up to a
certain age after which it declines ...) using the data set `Howell2`
from the rethinking package.

<br>

Get data and select the subset of all men aged 18 or older.


```r
data(Howell2)
d <- Howell2
rm(Howell2)
d <- d[d$age > 18 & d$male == 1, c('age', 'male', 'weight')]  # Select subset of data
d <- d[complete.cases(d), ]  # Remove cases with any NA
plot(d$age, d$weight, pch = 21, bg = "grey", 
     xlab = "Age [y]", ylab = "Weight [kg]")

# Add lowess-line
lines(lowess(d$age, d$weight), col = "red", lty = 2)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-38-1.png" width="672" />

<br>

Model: I will linearly transform age by centering it (set mean = 0) and
change unit to decades, below called $Adc$ .

$weight_i \sim Normal(\mu_i, \sigma)$

$\mu_i = b_0 + b_1Adc+b_2Adc^2$

$b_0 \sim Normal(70, 20)$

$b_1 \sim Normal(0, 2)$

$b_2 \sim Normal(0, 2)$

$sigma \sim Exponential(0.1)$

<br>

### Prior predictive simulation {.unnumbered}


```r
# Function that draws a line, argument: j = index of vector
drawpp <- function(j){
  yy <- b0[j] + b1[j]*xx + b2[j]*xx^2
  lines(xx, yy, col = rgb(1, 0, 0, 0.2))
}

# Random parameter values from priors
b0 <- rnorm(1e4, 70, 20)
b1 <- rnorm(1e4, 0, 2)
b2 <- rnorm(1e4, 0, 2)

# Empty plot
plot(c(-2, 4), c(0, 200), pch = "", xlab = "Age [decade re mean]",
     ylab = "weight [kg]")
abline(h = 20, col = "grey")  # Low weight line as reference
abline(h = 120, col = "grey") # High weight line as reference
mtext("Prior Predictive Simulation", side = 3, cex = 1.2)

# Draw 200 lines from prior
xx <- seq(-2, 4, 0.1)  # x-values for lines
for (j in 1:200) drawpp(j)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-39-1.png" width="672" />




### Fit model using `quap()` {.unnumbered}


```r
# Transform age
d$age10c <- (d$age - mean(d$age))/10

# Make new variable by squaring age
d$age10c2 <- d$age10c^2

#  Model notation
m <- alist(
  weight ~ dnorm(mu, sigma),
  mu <- b0 + b1*age10c + b2*age10c2,
  b0 ~ dnorm(70, 20),
  b1 ~ dnorm(0, 2),
  b2 ~ dnorm(0, 2),
  sigma ~ dexp(0.1)
  )

# Put all data in a list
dat <- list(weight = d$weight, age10c = d$age10c, age10c2 = d$age10c2)

# Fit model using quap()
mfit <- quap(m, data = dat)
precis(mfit)
```

```
##             mean        sd       5.5%      94.5%
## b0    50.3094049 0.5023939 49.5064824 51.1123273
## b1    -0.0608064 0.3105129 -0.5570660  0.4354532
## b2    -0.5542430 0.1338922 -0.7682285 -0.3402574
## sigma  5.4977238 0.2806831  5.0491380  5.9463097
```

<br>

Plot data and model predictions (point-estimate and 50 random lines from
the posterior). Note that I fitted the model using centered decades
(unit [decade re mean age]), but show model predictions on the original
scale (unit [y]).


```r
set.seed(123)

# Empty plot 
plot(d$age10c, d$weight, pch = "", axes = FALSE, 
     xlab = "Age [y]", 
     ylab = "Weight [kg]")

# Generate x-variable (age) and y-variable (mu) for plotting
xx <- seq(-3, 5, by = 0.1)  # age (unit centered decades)
mu <- link(mfit, data = list(age10c = xx, age10c2 = xx^2), n = 1e4) # mu

# Add 50 random lines from the posterior
rrow <- sample(1:1e4, size = 50, replace = FALSE)

for (j in rrow) {
  yy <- mu[j, ]
  lines(xx, yy, col = "grey")
}

# Add point-estimate line ("best" line)
mu_mdn <- apply(mu, 2, median)  # Point estimates per age
lines(xx, mu_mdn, lwd = 2)

# Add data points
points(d$age10c, d$weight, pch = 21, bg = "grey")

# Add axis, note that I transform back the age variable
xlabels <- seq(15, 90, by = 5)  # Ages on the x-axis
xticks <- (xlabels - mean(d$age))/10  # Underlying values in age10c
axis(1, at = xticks, labels = xlabels, pos = 30)
axis(2, at = seq(30, 65, 5))
```

<img src="04_ch4_files/figure-html/unnamed-chunk-41-1.png" width="672" />
### Posterior predictive check {.unnumbered}


```r
# Simulate data according model
ppc <- rethinking::sim(mfit, data = data.frame(age10c = d$age10c, age10c2 = d$age10c2))

## Plot data (black) and simulated data (red)

# Empty plot
plot(density(d$weight), type = "l", lwd = 0, ylim = c(0, 0.1), main = "",
     xlab = "Age")

# Add lines simulated
for (j in 1:100) {
  lines(density(ppc[j, ]), col = rgb(1, 0, 0, 0.1))
}

# Add observed data
lines(density(d$weight), lwd = 2)
```

<img src="04_ch4_files/figure-html/unnamed-chunk-42-1.png" width="672" />

