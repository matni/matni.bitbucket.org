--- 
title: "Stat2 notes"
author: "Mats E. Nilsson"
date: "2025-03-03"
site: bookdown::bookdown_site
documentclass: book
bibliography: ["references.bib"]
link-citations: true
github-repo: 
url: 
description: ""
---

# Preface {-}

Here I have collected my notes to the doctoral course Statistics 2 (Stat2), 
Department of Psychology, Stockholm University.  

The course follows closely the book @mcelreath2020statistical. I sort my notes 
in chapters, with the same chapter number as in McElreath's book.


<img src="figs/thebook.PNG" width="30%" />


## About the course {-}

### Topics and tools {-}

+ **Modelling**, specifically, the General and Generalized Linear Model
+ **Bayesian data analysis**
+ **Data simulation**
+ **Causal inference**
    - Directed Acyclical Graphs (DAGs)
+ **R**

<br>

### Resources {-}

+ The Book, @mcelreath2020statistical
+ [McElreath's lectures 2022](https://www.youtube.com/playlist?list=PLDcUM9US4XdMROZ57-OIRtIK0aOynbgZN){target="blank"}  
+ Two useful papers from Stat1: @wilkinson99apa, @Amrhein2019
+ @rohrer2018thinking, on DAGs
+ @gelman2020regression, Ch. 18, on potential outcome model of causality
+ [DAGitty](http://dagitty.net){target="blank"}


<br>

### Seminars {-}
- About two a week, not obligatory
- Seminar problems, see list of problems in the Syllabus (on Athena). Solve at 
home, discuss solutions at seminars
- Help with R, morning before each seminar


<br>


### Individual assignments {-}

- **Problems**, selected problems not discuss at seminars
- **Paper**, short paper using the book's statistical approach

For detailed instructions, see Athena.

<br>

### Examination {-}

This is a doctoral course, so grades are Pass or Fail. Pass both individual 
assignments and you have passed the course. 

<br>

### Installing rethinking and Stan {-}

Here some code that may be useful for installing rethinking and Stan


```r
## This script gives code for installing rethinking and 
# rstan (installed via rethinking)
# NOTE: If you only want to check that things work, run only the last code block, 
# skip the initial code blocks that install things


## First do this: Install Rtools -----------------------------------------------
# Install Rtools4: 
# Download here: https://cran.r-project.org/bin/windows/Rtools/rtools40.html
## -----------------------------------------------------------------------------


## Then this: Install and check cmdstan ----------------------------------------
# we recommend running this in a fresh R session or restarting your current session

# Install cmdstanr, used to install and check cmdstan
remotes::install_github("stan-dev/cmdstanr", ref = "windows_fixes") 
remotes::install_github("stan-dev/cmdstanr")

# Install cmdstan
library(cmdstanr)
# You may need to do this: check_cmdstan_toolchain(fix = TRUE)
install_cmdstan()
##------------------------------------------------------------------------------


## And then install rethinking -------------------------------------------------
install.packages(c("coda", "mvtnorm", "devtools", "dagitty"))
library(devtools)
devtools::install_github("rmcelreath/rethinking", force = TRUE)
## -----------------------------------------------------------------------------


## Finally: to see if you have both rethinking and Stan working ----------------
set.seed(123)

# simulate some data
y <- rnorm(100, mean = 100, sd = 15)
dat <- list(y = y)

# Check quap()
m1 <- rethinking::quap(flist = alist(
  y ~ dnorm(mu, sigma),
  mu <- b0,
  b0 ~ dnorm(100, 30),
  sigma ~ dexp(0.05)
  
), data = dat, start = list(b0 = 100, sigma = 15))

rethinking::precis(m1)

# Check ulam()
m1ulam <- rethinking::ulam(flist = alist(
  y ~ dnorm(mu, sigma),
  mu <- b0,
  b0 ~ dnorm(100, 30),
  sigma ~ dexp(0.05)
  
), data = dat)

rethinking::precis(m1ulam)

# Check Rstan
model_code  <- "
data{
    vector[100] y;
}
parameters{
    real b0;
    real<lower=0> sigma;
}
model{
    real mu;
    sigma ~ normal(15, 10);
    b0 ~ normal(100, 30);
    mu = b0;
    y ~ normal(mu, sigma);
}
"

m1_comp = rstan::stan_model(model_code = model_code)
m1rstan = rstan::sampling(m1_comp, data = dat,
               iter = 1e4, chains = 1)
print(m1rstan)

## -----------------------------------------------------------------------------
```



<br><br>

